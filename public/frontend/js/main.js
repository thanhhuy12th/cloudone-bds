$(document).ready(function() {


    // Slide Bar 
    $('#mybtnslide').on('click', function() {
        $('.slide-bar').removeClass('none');
    });
    $('#mybtnclose').on('click', function() {
        $('.slide-bar').addClass('none');
    });

    //Back to top
    var windowH = $(window).height() / 2;
    $(window).on('scroll', function() {
        if ($(this).scrollTop() > windowH) {
            $("#myBtn").css('display', 'flex');

        } else {
            $("#myBtn").css('display', 'none');

        }
    });


    $('#myBtn').on("click", function() {
        $('html, body').animate({ scrollTop: 0 }, 300);
    });

    $('#menu_mobile').on("click",function() {
        var i = 0;
        if (true) {}
    });
     //side Menu mobile
     var i =1;
    $ ('#danhmuc_mobile_menu').on("click",function() {      
       if (i%2!= 0) {
            $("#menu_chile_mobile").css("display","block");
           i++;
       } else {
            $("#menu_chile_mobile").css("display","none");
            i++;
        }
      
    });

     var j =1;
    $ ('#user_mobile_menu').on("click",function() {      
       if (j%2!= 0) {
            $("#user_menu_chile_mobile").css("display","block");
           j++;
       } else {
            $("#user_menu_chile_mobile").css("display","none");
            j++;
        }
      
    });
   
  

        //Handle page menu
        if ($('.page_menu_item').length) {
            var items = $('.page_menu_item');
            items.each(function() {
                var item = $(this);
                item.on('click', function(evt) {
                    if (item.hasClass('has-children')) {
                        evt.preventDefault();
                        evt.stopPropagation();
                        var subItem = item.find('> ul');
                        if (subItem.hasClass('active')) {
                            subItem.toggleClass('active');
                            TweenMax.to(subItem, 0.3, { height: 0 });
                        } else {
                            subItem.toggleClass('active');
                            TweenMax.set(subItem, { height: "auto" });
                            TweenMax.from(subItem, 0.3, { height: 0 });
                        }
                    } else {
                        evt.stopPropagation();
                    }
                });
            });
        }
    


    $(window).on('scroll', function() {
        var x = $(this).scrollTop();
        $('.parallax').css('background-position-x', parseInt(-x / 30) + 'px');
    })
        //Popup Image
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function(item) {
                    return item.el.attr('title') + '<small>Tên Công ty</small>';
                }
            }
        });
    //Bxslider for about
    $('.slider').bxSlider({
        auto: true,
        autoControls: true,
        stopAutoOnClick: true,
        pager: true,
        slideWidth: 600
    });

    //Scroll to select
    $('.main-scroll-container a[href^="#"]').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 2000);
        }

    });

    var cr = 1;
    $('#mybtnsrcoll').on('click', function() {
        if (cr == 1) {
            $('.scroll-to-the-selected').removeClass('none');
            $("#mybtnsrcoll").css("transform", "rotate(180deg)");
            cr = 0;
        } else {
            $('.scroll-to-the-selected').addClass('none');
            $("#mybtnsrcoll").css("transform", "rotate(360deg)");
            cr = 1;
        }

    });
    $('.dangbaiviet .form-group').hide();
    $('#1').show();
    $('#option').change(function() {
        $('.dangbaiviet .form-group').hide();
        $('#' + $(this).val()).show();
        if ($(this).val() == 8)
            init();
    });

    //Canhbao
    $('.username .alert').hide();
    $('.username input').keyup(function(e) {
        var count = $('.username input').val().length;
        if (count < 6) {
            $('.username .alert-info').show();
        } else {
            $('.username .alert-info').hide();
        }

        if (count > 24) {
            $('.username .alert-warning').show();
        } else {
            $('.username .alert-warning').hide();
        }

        if (count == 0) {
            $('.username .alert-info').hide();
            $('.username .alert-warning').hide();
        }
    })

    $('.password .alert').hide();
    $('.password input').keyup(function(e) {
        var count = $('.password input').val().length;
        if (count < 6) {
            $('.password .alert-info').show();
        } else {
            $('.password .alert-info').hide();
        }

        if (count > 24) {
            $('.password .alert-warning').show();
        } else {
            $('.password .alert-warning').hide();
        }

        if (count == 0) {
            $('.password .alert-info').hide();
            $('.password .alert-warning').hide();
        }
    })

    $('.telephone .alert').hide();
    $('.telephone input').keyup(function(e) {
        var num = $('.telephone input').val();
        if (!$.isNumeric(num) || num.indexOf('.') > -1) {
            $('.telephone .alert-warning').show();
        } else {
            $('.telephone .alert-warning').hide();
        }
        if (num.length == 0) {
            $('.telephone .alert-warning').hide();
        }
    })

    $('.password_confirmation .alert').hide();
    $('.password_confirmation input').keyup(function(e) {
        var cpass = $('.password_confirmation input').val();
        var pass = $('.password input').val();
        if (pass != cpass) {
            $('.password_confirmation .alert-warning').show();
        } else {
            $('.password_confirmation .alert-warning').hide();
        }

    })

    $('.fullname .alert').hide();
    $('.fullname input').keyup(function(e) {
        var count = $('.fullname input').val().length;
        if (count > 50) {
            $('.fullname .alert-warning').show();
        } else {
            $('.fullname .alert-warning').hide();
        }
        if (count == 0) {
            $('.fullname .alert-warning').hide();
        }
    })

    $('.mess .alert').hide();
    $('.mess textarea').keyup(function(e) {
        var check = $('.mess textarea').val();
        var count = $('.mess textarea').val().length;
        if (count > 200) {
            $('.mess .alert-warning').show();
        } else {
            $('.mess .alert-warning').hide()
        }

        if (check.indexOf('<script>') > -1 || check.indexOf('</script>') > -1) {
            $('.mess .alert-info').show();
        } else {
            $('.mess .alert-info').hide()
        }
        if (count == 0) {
            $('.mess .alert-warning').hide();
            $('.mess .alert-info').hide();
        }
    })


    
    $("#slider-range").slider({
        min: 0,
        max: 400,
        values: [0,400],
        start: function (event, ui) {
            $("#slider-range").slider("option", "step", 5);
        },
        slide: function (event, ui) {
            if (ui.values[0] > ui.values[1] || ui.values[1] > ui.values[2] || ui.values[2] > ui.values[3]) {
                return false;
            }
            $("#dt_from").html(ui.values[0]);
            $("#dt_to").html(ui.values[1]);
            $("#dientich").val(ui.values[0]+"-"+ui.values[1]);
            $("#dientichhidden").val(ui.values[0]+"-"+ui.values[1]);
        }
    });

    $("#slider-range-price").slider({
        min: 500,
        max: 30000,
        values: [500,30000],
        start: function (event, ui) {
            $("#slider-range-price").slider("option", "step", 100);
        },
        slide: function (event, ui) {
            if (ui.values[0] > ui.values[1] || ui.values[1] > ui.values[2] || ui.values[2] > ui.values[3]) {
                return false;
            }

            currencyFrom = "triệu";
            currencyTo = "triệu";
            uiCurrentFrom = ui.values[0];
            uiCurrentTo = ui.values[1];
            if(ui.values[0] >= 1000) {
                currencyFrom = "tỷ";
                uiCurrentFrom = ui.values[0]/1000;
            }
            if(ui.values[1] >= 1000) {
                currencyTo = "tỷ";
                uiCurrentTo = ui.values[1]/1000;
            }
            
            $("#p_from").html((uiCurrentFrom)+" "+currencyFrom);
            $("#p_to").html((uiCurrentTo)+" "+currencyTo);
            $("#gia").val((uiCurrentFrom)+" "+currencyFrom + " - " + (uiCurrentTo)+" "+currencyTo);
            $("#giahidden").val(ui.values[0]+"-"+ui.values[1]);
        }
    });

    



});

function to_vnd(x) {
    x = x.toString();
    var x_arr = x.split("");
    var temp_arr = [];
    var j = 0;
    for(var i=x_arr.length-1; i>=0;i--) {
        j++;
        temp_arr.push(x_arr[i]);
        if(j%3==0 && i>0) {
            temp_arr.push("."); 
        }
    }
    temp_arr = temp_arr.reverse().join("");
    return temp_arr;
}
var j = 0;
var k = 0;
function showModalDienTich(e) {
    if(j == 0) {
        $("#dientich").val("0-400");
        $("#dientichhidden").val("0-400");
    }
    $("#m_dientich").modal();
    j++;
}
function showModalGia(e) {
    if(k == 0) {
        $("#gia").val("500 triệu - 30 tỷ");
        $("#giahidden").val("500-30000");
    }
    $("#m_gia").modal();
    k++;
}