<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/province','ApiController@get_province');
Route::get('/province/{id}','ApiController@get_province');

Route::get('/district','ApiController@get_district');
Route::get('/district/{id}','ApiController@get_district');

Route::post('/tin-dang', 'ApiController@change_status_tindang');
Route::post('/duyet-bai', 'ApiController@duyetbai_tindang');
Route::post('/du-an', 'ApiController@change_status_duan');
Route::post('/video','ApiController@change_status_video');
Route::post('/can-ho', 'ApiController@change_status_canho');
Route::post('/duyet-bai-can-ho', 'ApiController@duyetbai_canho');

Route::get('/change-lien-he', 'ApiController@change_lienhe');