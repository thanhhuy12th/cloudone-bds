<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {

    return view('login');
});*/


Auth::routes();

	Route::get('password/reset', 'Auth\FrontendForgotPasswordController@showLinkRequestForm')->name('password.request');	
	Route::get('password/reset/{token}', 'Auth\FrontendResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'Auth\FrontendResetPasswordController@reset')->name('password.update');
	Route::get('password/confirm/{token}', 'EmailController@confirm')->name('password.confirm');
	Route::post('password/reset/change-password', 'EmailController@change_password')->name('password.change-password');
	Route::post('password/email', 'EmailController@reset_password')->name('password.email');

	// quên mật khẩu admin 
	Route::get('admin-password/reset', 'Auth\ForgotPasswordController@showLinkRequestFormAdmin')->name('admin-password.request');
	Route::post('admin-password/email', 'EmailController@reset_password_admin')->name('admin-password.email');

	Route::get('admin-password/reset/{token}', 'Auth\ResetPasswordController@showResetFormAdmin')->name('admin-password.reset');
	Route::post('admin-password/reset', 'Auth\ResetPasswordController@reset')->name('admin-password.update');

	Route::get('admin-password/confirm/{token}', 'EmailController@confirm_admin')->name('admin-password.confirm');
	Route::post('admin-password/reset/change-password', 'EmailController@change_password_admin')->name('admin-password.change-password');


Route::namespace('Frontend')->group(function() {
	Route::get('','HomeController@index')->name('home');
	
	Route::get('product-category-parent/{alias}','HomeController@get_category_parent')->name('product-category-parent');
	Route::get('product-category/{alias}/{parent}','HomeController@product_category')->name('product-category');
	Route::get('product-detail/{alias}','HomeController@product_detail')->name('product-detail');

	Route::get('can-ho/{alias}','HomeController@getCan_ho')->name('can-ho');
	Route::get('can-ho-detail/{alias}','HomeController@temp_detail')->name('can-ho-detail');
	
	Route::get('gioi-thieu','HomeController@about')->name('gioi-thieu');

	// Route::get('project','HomeController@project_news')->name('project');
	// Route::get('project-detail/{alias}','HomeController@project_detail')->name('project-detail');

	Route::get('tin-tuc','HomeController@getNews')->name('news');
	Route::get('tin-tuc/{alias}','HomeController@news_detail')->name('news-detail');

	Route::get('danh-muc-tin-tuc/{id}/{alias}','HomeController@newsCategory')->name('news-category');

	Route::get('contact','HomeController@ContactCompany')->name('contact');
	Route::get('lien-he','HomeController@contactUs')->name('lien-he');
	Route::post('lien-he','HomeController@storeContact')->name('lien-he.store');

	Route::get('dangnhap','HomeController@getDangnhap')->name('dangnhap');
	Route::post('dangnhap','HomeController@postDangnhap');

	Route::get('register', 'HomeController@RegisterAccount');
	Route::get('registersucces', 'HomeController@Registersucces');
	
	Route::get('dangky','HomeController@getDangky')->name('dangky');
	Route::post('dangky','HomeController@postDangky');

	Route::get('dangxuat','HomeController@getDangxuat')->name('dangxuat');

	Route::get('tin-dang','HomeController@create')->name('quanlytk.create');
	Route::post('tin-dang','HomeController@store')->name('quanlytk.store');
	Route::get('tin-dang/{alias}','HomeController@edit')->name('quanlytd.edit');
	Route::post('tin-dang/{alias}','HomeController@update')->name('quanlytd.update');
	Route::get('tin-dang/{id}/delete','HomeController@destroyQuanlytd')->name('quanlytk.destroy');

	Route::get('dang-tin-can-ho','HomeController@CreateCanho')->name('quanlytk.CreateCanho');
	Route::post('dang-tin-can-ho','HomeController@StoreCanho')->name('quanlytk.StoreCanho');
	Route::get('sua-tin-can-ho/{alias}','HomeController@EditCanHo')->name('quanlycanho.EditCanHo');
	Route::post('sua-tin-can-ho/{alias}','HomeController@UpdateCanHo')->name('quanlycanho.UpdateCanHo');
	Route::get('tin-dang-can-ho/{id}/delete','HomeController@destroyCanho')->name('quanlytk.destroyCanho');


	Route::get('quan-ly-tai-khoan','HomeController@editQuanlytk')->name('quanlytk.edit');
	Route::post('quan-ly-tai-khoan','HomeController@updateQuanlytk')->name('quanlytk.update');
	
	Route::post('change-password','HomeController@updateChangePassword')->name('changepass.update');
	
	Route::post('dangnhap','HomeController@postDangnhap');

	Route::get('tim-kiem','HomeController@get_timkiem')->name('tim-kiem');
	Route::post('tim-kiem','HomeController@timkiem')->name('tim-kiem');
	
	Route::post('search-can-ho','HomeController@postSearch')->name('search-can-ho');

	Route::get('view-more-can-ho','HomeController@getViewCanho')->name('view-more-can-ho');
	Route::get('view-more-nha-dat','HomeController@getViewNhadat')->name('view-more-nha-dat');

});


Route::get('sign-in','LoginController@getLogin')->middleware('loginal')->name('getLogin');
Route::post('sign-in','LoginController@postLogin');
Route::get('logout','LoginController@getLogout')->name('getLogout');


Route::namespace('Backend')->middleware('login')->prefix('admin')->group(function(){
	
	 Route::resource('/','HomeController');
	
	 Route::get('index','AdminController@index')->name('admin.index');
	 Route::get('add','AdminController@create')->name('admin.create');
	 Route::post('add','AdminController@store')->name('admin.store');
	 Route::get('edit/{id}','AdminController@edit')->name('admin.edit');
	 Route::post('edit/{id}','AdminController@update')->name('admin.update');
	 Route::get('delete/{id}','AdminController@destroy')->name('admin.destroy');

	 Route::get('change-password','AdminController@Getchangepass')->name('change-password');
	 Route::post('change-password','AdminController@Postchangepass');

	 Route::resource('dmtd','DMTDController');
	 Route::resource('dmtt','DMTTController');
	 Route::resource('danh-muc-can-ho','DMCanHoController');
	 Route::resource('can-ho','CanHoController');
	 // Route::resource('duan','DuAnController');
	 Route::resource('lienhe','LienHeController');
	 Route::resource('member','MemberController');
	 Route::resource('menu','MenuController');
	 Route::resource('slider','SliderController');
	 Route::resource('tindang','TinDangController');
	 Route::resource('tin-tuc','TinTucController');
	 Route::resource('video','VideoController');
	 Route::resource('gioithieu','IntroduceController');
	 Route::resource('logo','LogoController');
	 Route::resource('mangxahoi','SocialNetworkController');
	 Route::resource('googlemap','GoogleMapController');
	 Route::resource('contactus','ContactUsController');
	 Route::resource('footer','FooterController');
	 Route::resource('cauhinhkhac','ConfigurationController');
});

Route::get('clear-cache','LoginController@clear_cache');