<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Video extends Model
{
    protected $table = 'bds_video';
    public $timestamp = false;
    public function dbVideo()
    {
    	return DB::table('bds_video');
    }
    public function listVideo()
    {
    	return $this->dbVideo()->select('id','name','linkyoutube','status','created_at')->get()->toArray();
    }
    public function addVideo($data)
    {
    	return $this->dbVideo()->insert($data);
    }
    public function editVideo($id)
    {
    	return $this->dbVideo()->find($id);
    }
    public function updateVideo($data,$id)
    {
    	return $this->dbVideo()->whereId($id)->update($data);
    }
    public function delVideo($data)
    {
    	return $this->dbVideo()->delete($data);
    }
}
