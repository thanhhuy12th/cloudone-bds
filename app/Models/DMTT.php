<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class DMTT extends Model
{
    protected $table = "bds_dmtt";
    public $timestamp = false;
    public function dbdmtt()
    {
    	return DB::table('bds_dmtt');
    }
    public function listindex()
    {
    	return $this->dbdmtt()->get()->toArray();
    }
    public function adddmtt($data)
    {
    	return $this->dbdmtt()->insert($data);
    }
    public function showEdit($id)
    {
    	return $this->dbdmtt()->find($id);
    }
    public function updatedmtt($data,$id)
    {
    	return $this->dbdmtt()->whereId($id)->update($data);
    }
    public function delnews($data)
    {
        return $this->dbdmtt()->delete($data);
    }

    public function NewsIdParent($id) 
    {
        return $this->dbdmtt()->where("parent_id",$id)->get()->toArray();
    }
}
