<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class SocialNetwork extends Model
{
    protected $table = 'bds_config';
    public $timestamp = false;
    public function dbSocial()
    {
    	return DB::table('bds_config');
    }
    public function editSocial($id)
    {
    	return $this->dbSocial()->select('content')->find($id);
    }
     public function updateTwitter($data)
    {
        return $this->dbSocial()->whereId(7)->update($data);
    }
    public function updateFacebook($data)
    {
    	return $this->dbSocial()->whereId(8)->update($data);
    }
}
