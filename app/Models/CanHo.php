<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class CanHo extends Model
{
    protected $table = "bds_canho";
    public $timestamp = false;

    public function dbCanHo()
    {
    	return DB::table('bds_canho');
    }
    public function listCanHo()
    {
    	return $this->dbCanHo()->orderBy('id','DESC')->get()->toArray();
    }
    public function addCanHo($data)
    {
    	return $this->dbCanHo()->insert($data);
    }
    public function editCanHo($id)
    {
    	return $this->dbCanHo()->find($id);
    }
    public function updateCanHo($data, $id)
    {
    	return $this->dbCanHo()->whereId($id)->update($data);
    }
    public function delCanHo($data)
    {
    	return $this->dbCanHo()->delete($data);
    }


    public function editCanHoByAlias($alias)
    {
        return $this->dbCanHo()->where('alias',$alias)->first();
    }
    
}
