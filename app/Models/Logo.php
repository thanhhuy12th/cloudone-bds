<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Logo extends Model
{
    protected $table = 'bds_config';
    public $timestamp = false;
    public function dbLogo()
    {
    	return DB::table('bds_config');
    }
    public function addLogo($data)
    {
    	return $this->dbLogo()->insert($data);
    }
    public function editLogo($id)
    {
    	return $this->dbLogo()->find($id);
    }
    public function updateLogo($data,$id)
    {
    	return $this->dbLogo()->whereId($id)->update($data);
    }
}
