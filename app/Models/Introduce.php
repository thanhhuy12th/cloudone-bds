<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Introduce extends Model
{
    protected $table = 'bds_config';
    public $timestamp = false;
    public function dbIntroduce()
    {
    	return DB::table('bds_config');
    }
    public function editIntroduce($id)
    {
    	return $this->dbIntroduce()->select('content')->find($id);
    }
    public function updateIntroduce($data)
    {
    	return $this->dbIntroduce()->whereId(4)->update($data);
    }
}
