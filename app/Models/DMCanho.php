<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class DMCanHo extends Model
{
    protected $table = "bds_dmcanho";
    public $timestamp = false;

    public function dbDMCanHo()
    {
    	return DB::table('bds_dmcanho');
    }
    public function listDMCanHo()
    {
    	return $this->dbDMCanHo()->get()->toArray();
    }
    public function addDMCanHo($data)
    {
    	return $this->dbDMCanHo()->insert($data);
    }
    public function editDMCanHo($id)
    {
    	return $this->dbDMCanHo()->find($id);
    }
    public function updateDMCanHo($data, $id)
    {
    	return $this->dbDMCanHo()->whereId($id)->update($data);
    }
    public function delDMCanHo($data)
    {
    	return $this->dbDMCanHo()->delete($data);
    }

    public function listTempByIdParent($id) 
    {
        return $this->dbDMCanHo()->where("parent_id",$id)->get()->toArray();
    }

}
