<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Menu extends Model
{
    protected $table = 'bds_menu';
    public $timestamp = false;
    public function dbmenu()
    {
    	return DB::table('bds_menu');
    }
    public function listmenu()
    {
    	return $this->dbmenu()->get()->toArray();
    }
    public function addmenu($data)
    {
    	return $this->dbmenu()->insert($data);
    }
    public function editmenu($id)
    {
    	return $this->dbmenu()->find($id);
    }
    public function updatemenu($data,$id)
    {
    	return $this->dbmenu()->whereId($id)->update($data);
    }
    public function delmenu($data)
    {
    	return $this->dbmenu()->delete($data);
    }

    
}
