<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    
    protected $table = "bds_admin";
    public $timestamp = false;

   public function dbadmin()
   {
   		return DB::table('bds_admin');
   }
   public function listadmin()
   {
   		return $this->dbadmin()->get()->toArray();
   }
   public function add($data)
   {
   		return $this->dbadmin()->insert($data);
   }
   public function editAdmin($id)
   {
   		return $this->dbadmin()->find($id);
   }
   public function capnhat($data,$id)
   {
   		return $this->dbadmin()->whereId($id)->update($data);
   }

  public function delAdmin($id)
  {
    $check = $this->editAdmin($id);
      if($check->role != 1){
          $this->dbadmin()->delete($id);
          return true;
      } else {
          return false;
      }
  }

}
