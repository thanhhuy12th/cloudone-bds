<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use DateTime;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $canho = DB::table('bds_canho')->select('id','exp_time')->where('hot',1)->where('status',1)->get()->toArray();
            $tindang = DB::table('bds_tindang')->select('id','exp_time')->where('status',1)->where('duyetbai',1)->get()->toArray();
        
            foreach($canho as $ch){
                $time_now = \Carbon\Carbon::now()->format('Y-m-d');
                if($ch->exp_time === $time_now){
                    DB::table('bds_canho')->where('id',$ch->id)->update(['position' => 10]);
                }
            } 
        
            foreach($tindang as $td){
                $time_now = \Carbon\Carbon::now()->format('Y-m-d');
                if($td->exp_time === $time_now){
                    DB::table('bds_tindang')->where('id',$td->id)->update(['position' => 10]);
                }
            }
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
