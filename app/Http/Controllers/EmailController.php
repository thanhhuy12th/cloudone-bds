<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
use App\Models\Configuration;
use DateTime;
use DB;

class EmailController extends Controller
{

    private $mailer;
    public function __construct(PHPMailer $mailer, Configuration $configuration) {
        $this->configuration = $configuration;

        $this->mailer = $mailer;

        $this->mailer->IsSMTP();
        $this->mailer->Host         = "smtp.gmail.com";
        $this->mailer->SMTPAuth     = true;
        $this->mailer->Host         = "smtp.gmail.com";
        $this->mailer->Port         = 587;
        $this->mailer->SMTPSecure   = 'tls';
        $this->mailer->SMTPOptions  = array(
        'ssl' => array(
            'verify_peer'           => false,
            'verify_peer_name'      => false,
            'allow_self_signed'     => true
            )
        );
        $this->mailer->CharSet      = 'UTF-8';
        $this->mailer->Username     = $this->configuration->editConfig(24)->content;
        $this->mailer->Password     = $this->configuration->editConfig(25)->content;
    }

    public function reset_password(Request $request) {
        $email          = $request->email;

        $checkUserExist = DB::table('bds_member')->where('email',$email)->get();
        if($checkUserExist->count() > 0) {
            $checkExist     = DB::table('password_resets')->where('email',$email)->where('status',0)->get();

            if($checkExist->count() > 0) {
                return redirect()->route('password.request')->with('error',"Không thể xác thực 2 lần, chúng tôi đã gửi link xác thực trước đó đến email này, bạn vui lòng kiểm tra lại trong inbox hoặc spam");
            }
            else {
               $now        = new DateTime();
                $token      = md5($now->format('Y-m-d HH:mm:ss').$email.rand());
                $url_token  = url('/')."/password/confirm/".$token;

                
                $Title      = "Khôi phục mật khẩu từ MUANHAVN.NET";
                $Subject    = "Khôi phục mật khẩu từ MUANHAVN.NET";
                $Content    = "Chào bạn, <p>Bạn cần xác thực email để đổi mật khẩu</p> <p>Bạn vui lòng click vào đường dẫn bên dưới để xác thực tài khoản.</p> <p><a href='".$url_token."'>".$url_token."</a></p> <p>Trân trọng,</p><p>MUANHAVN.NET</p>";

                $data = array(
                    'email' => $email,
                    'token' => $token,
                );

                $insert = DB::table('password_resets')->insert($data);

                if($insert) {
                    $this->mailer->SetFrom($this->mailer->Username, $Title);
                    $this->mailer->Subject      = $Subject;
                    $this->mailer->MsgHTML($Content);
                    $this->mailer->AddAddress($email, $Title);
                    if(!$this->mailer->Send()) {
                        return redirect()->route('password.request')->with('error',"Lỗi gửi mail, vui lòng liên hệ ban quản trị website");
                    }
                    else
                    {

                        return redirect()->route('password.request')->with('success',"Hệ thống đã gửi mã xác nhận đến email của bạn, vui lòng kiểm tra trong inbox hoặc spam");
                    } 
                }
                else {
                    return redirect()->route('password.request')->with('error',"Lỗi hệ thống, vui lòng liên hệ ban quản trị website");
                }
                
            }  
        }
        else {
            return redirect()->route('password.request')->with('error',"Email này không tồn tại trong hệ thống");
        }
        
    }

    
    public function confirm($token) {
        $data = [];
        $check = DB::table('password_resets')->where('token',$token)->first();
        if($check == null) {
            $data['result']         = 0;
            $data['result_message'] = "Link không tồn tại";
        }
        else {
            if($check->status == 1) {
                $data['result']         = 0;
                $data['result_message'] = "Link không còn hiệu lực, vui lòng reset mật khẩu lại";
            }
            else {
                $data['result']         = 1;
            }
        }
        $data['token'] =  $token;
        return view('auth.passwords.confirm',["data"=>$data]);
    }

    public function change_password(Request $request) {
        $password   = $request->password;
        $password2  = $request->password2;
        $token      = $request->token;

        $check = DB::table('password_resets')->where('token',$token)->first();
        if($check == null) {
            $data['result']         = 0;
            $data['result_message'] = "Link không tồn tại";
        }
        else {
            if($check->status == 1) {
                $data['result']         = 0;
                $data['result_message'] = "Link không còn hiệu lực, vui lòng reset mật khẩu lại";
            }
            else {
                if($password != $password2) {
                    $data['result']         = 0;
                    $data['result_message'] = "Mật khẩu xác nhận không trùng khớp";
                }
                else {
                    // Cập nhật lại trạng thái token
                    $data = array(
                        'status' => 1,
                        'created_at' => new DateTime,
                    );
                    $update_token = DB::table('password_resets')->where('token',$token)->update($data);

                    if($update_token) {
                        // Change password
                        $data = array(
                            'password' => bcrypt($password),
                            'updated_at' => new DateTime,
                        );
                        $update_password = DB::table('bds_member')->where('email',$check->email)->update($data);
                        if($update_password) {
                            $data['result']         = 2;
                            $data['result_message'] = "Đổi mật khẩu thành công, bạn có thể đăng nhập lại với mật khẩu mới";
                        }
                        else {
                            $data['result']         = 0;
                            $data['result_message'] = "Lỗi hệ thống, vui lòng liên hệ admin";
                        }
                    }
                    else {
                        $data['result']         = 0;
                        $data['result_message'] = "Lỗi hệ thống, vui lòng liên hệ admin";
                    }
                }
            }
        }
        return view('auth.passwords.confirm',["data"=>$data]);
    }

    public function register_account($Content, $email) {
        $Subject = "Xác thực tài khoản MUANHAVN.NET";
        $Title = "Xác thực tài khoản MUANHAVN.NET";

        $this->mailer->SetFrom($this->mailer->Username, $Title);
        $this->mailer->Subject      = $Subject;
        $this->mailer->MsgHTML($Content);
        $this->mailer->AddAddress($email, $Title);
        return $this->mailer->Send();
        

    }

    // Quên mật khẩu ADMIN
    
    public function reset_password_admin(Request $request) 
    {
        $email          = $request->email;

        $checkUserExist = DB::table('bds_admin')->where('email',$email)->get();

        if($checkUserExist->count() > 0) {
            $checkExist  = DB::table('bds_admin')->where('email',$email)->where('status',0)->get();
         
            if($checkExist->count() > 0) {

               $now        = new DateTime();
                $token      = md5($now->format('Y-m-d HH:mm:ss').$email.rand());
                $url_token  = url('/')."/admin-password/confirm/".$token;

                
                $Title      = "Khôi phục mật khẩu từ MUANHAVN.NET";
                $Subject    = "Khôi phục mật khẩu từ MUANHAVN.NET";
                $Content    = "Chào bạn, <p>Bạn cần xác thực email để đổi mật khẩu</p> <p>Bạn vui lòng click vào đường dẫn bên dưới để xác thực tài khoản.</p> <p><a href='".$url_token."'>".$url_token."</a></p> <p>Trân trọng,</p><p>MUANHAVN.NET</p>";

                $data = array(
                    // 'email' => $email,
                    'token' => $token,
                );
              
                $insert = DB::table('bds_admin')->where('email',$email)->update($data);

                if($insert) {
                    $this->mailer->SetFrom($this->mailer->Username, $Title);
                    $this->mailer->Subject      = $Subject;
                    $this->mailer->MsgHTML($Content);
                    $this->mailer->AddAddress($email, $Title);
                    if(!$this->mailer->Send()) {
                        return redirect()->route('admin-password.request')->with('error',"Lỗi gửi mail, vui lòng liên hệ ban quản trị website");
                    }
                    else
                    {

                        return redirect()->route('admin-password.request')->with('success',"Hệ thống đã gửi mã xác nhận đến email của bạn, vui lòng kiểm tra trong inbox hoặc spam");
                    } 
                }
                else {
                    return redirect()->route('admin-password.request')->with('error',"Lỗi hệ thống, vui lòng liên hệ ban quản trị website");
                }
            }   
              
        }
        else {
            return redirect()->route('admin-password.request')->with('error',"Email này không tồn tại trong hệ thống");
        }
        
    }

    public function confirm_admin($token) 
    {
        $data = [];
        $check = DB::table('bds_admin')->where('token',$token)->first();
        // dd($check);
        if($check == null) {
            $data['result']         = 0;
            $data['result_message'] = "Link không tồn tại";
        }
        else {
            if($check->status == 1) {
                $data['result']         = 0;
                $data['result_message'] = "Link không còn hiệu lực, vui lòng reset mật khẩu lại";
            }
            else {
                $data['result']         = 1;
            }
        }
        $data['token'] =  $token;
        // dd($data['token']);
         
        return view('auth.admin.confirm',["data"=>$data]);
    }
    public function change_password_admin(Request $request) 
    {
       
        $password   = $request->password;
        $password2  = $request->password2;
        $token      = $request->token;

        $check = DB::table('bds_admin')->where('token',$token)->first();
        if($check == null) {
            $data['result']         = 0;
            $data['result_message'] = "Link không tồn tại";
        }
        else {
            if($check->status == 1) {
                $data['result']         = 0;
                $data['result_message'] = "Link không còn hiệu lực, vui lòng reset mật khẩu lại";
            }
            else {
                if($password != $password2) {
                    $data['result']         = 0;
                    $data['result_message'] = "Mật khẩu xác nhận không trùng khớp";
                }
                else {
                    // Cập nhật lại trạng thái token
                    $data = array(
                        'status' => 1,
                        'created_at' => new DateTime,
                    );
                    $update_token = DB::table('bds_admin')->where('token',$token)->update($data);
                    // dd($update_token);
                    if($update_token) {
                        // Change password
                        $data = array(
                            'password' => bcrypt($password),
                            'updated_at' => new DateTime,
                        );
                        $update_password = DB::table('bds_admin')->where('email',$check->email)->update($data);
                       // dd($update_password);
                        if($update_password) {
                            $data['result']         = 2;
                            $data['result_message'] = "Đổi mật khẩu thành công, bạn có thể đăng nhập lại với mật khẩu mới";
                        }
                        else {
                            $data['result']         = 0;
                            $data['result_message'] = "Lỗi hệ thống, vui lòng liên hệ admin";
                        }
                    }
                    else {
                        $data['result']         = 0;
                        $data['result_message'] = "Lỗi hệ thống, vui lòng liên hệ admin";
                    }
                }
            }
        }

            echo "<script>
                alert('Mật khẩu của bạn đã được thay đổi thành công.');
                window.location = '".url('/admin')."'
              </script>";
      // return view('auth.admin.confirm',["data"=>$data]);
    }


}
