<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\LienHe\ContactRequest;
use App\Http\Controllers\Controller;
use App\Models\LienHe;
use DateTime;

class LienHeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $lienhe;
    public function __construct(LienHe $lienhe)
    {
        $this->lienhe = $lienhe;
    }
    public function createContact($request)
    {
        $data = array(
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'content'   => $request->content,
            'created_at'=> new DateTime 
        );

        return $data;
    }
    public function index()
    {
        $data['contact'] = $this->lienhe->listcontact();

        return view('admin.modules.lienhe.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.lienhe.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        $data = $this->createContact($request);

        $this->lienhe->addcontact($data);

        return redirect()->route('lienhe.index',$data)->with("success",'message.store_required');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['contact']   = $this->lienhe->editcontact($id);

        return view('admin.modules.lienhe.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->createContact($request);

        $this->lienhe->updatecontact($data,$id);

        return redirect()->route('lienhe.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data) ; $i++) { 
            $this->lienhe->delcontact($data[$i]);
        }

        return redirect()->route('lienhe.index')->with("success",'message.delete_required');
    }
}
