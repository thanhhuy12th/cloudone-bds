<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\Canho\CanhoStore;
use App\Http\Controllers\Controller;
use App\Models\CanHo;
use App\Models\DMCanHo;
use App\Models\Member;
use DateTime;
use Carbon;

class CanHoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $temp;
    private $dmtemp;
    private $member;
    public function __construct
    (
        CanHo $temp,
        DMCanHo $dmtemp,
        Member $member
    )
    {
        $this->temp = $temp;
        $this->dmtemp = $dmtemp;
        $this->member =$member;
    }
    public function index()
    {
        $data['temp'] = $this->temp->listCanHo();

        return view('admin.modules.temp.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $data['cate_option'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->dmtemp->listTempByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->dmtemp->listTempByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }
        return view('admin.modules.temp.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CanhoStore $request)
    {
       
        $img1="default.jpg";
        $fileNameTotal = "";
        if ($request->hasFile('images')) {

            $rules = array("jpg","jpeg","png","jpeg","gif","svg");
             foreach ($request->file('images') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $arr = [];
                     $fileName = time().'_'.$file->getClientOriginalName();
                     $uploadPath = 'public/uploads/postings';
                     $file->move($uploadPath, $fileName);
                    $fileNameTotal .= $fileName."||";

                 }
            }
            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact;
            $googlemap = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->googlemap );

            $data = array(
                            'title'                 => $request->title,
                            'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                            'dientich'              => $request->dientich,
                            'priceCDT'              => $request->priceCDT,
                            'priceChenh'            => $request->priceChenh,
                            'khuvuc'                => $request->khuvuc,                           
                            'intro'                 => $request->intro,
                            'content'               => $request->content,
                            'googlemap'             => $googlemap,                          
                            'contact'               => $contact,
                            'maCan'                 => $request->maCan,
                            'maToa'                 => $request->maToa,
                            'category_id'           => $request->category_id,
                            'member_id'             => $request->member_id,
                            'images'                => $fileNameTotal,
                            'position'              => $request->position,
                            'link'                  => $request->link,
                            'huong_view'            => $request->huong_view,
                            'huong_bancong'         => $request->huong_bancong,                            
                            'hot'                   => $request->hot,
                            'status'                => $request->status,                           
                            'exp_time'				=> \Carbon\Carbon::parse($request->exp_time)->format('Y-m-d'),
                            'created_at'            => new DateTime
                        );
                
             $this->temp->addCanHo($data);

            return redirect()->route('can-ho.index')->with('success',"message.store_required");
        } 
        else {

            $img1="default.jpg";
            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact;
            $googlemap = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->googlemap );
            $data = array(
                            'title'                 => $request->title,
                            'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                            'dientich'              => $request->dientich,
                            'priceCDT'              => $request->priceCDT,
                            'priceChenh'            => $request->priceChenh,
                            'khuvuc'                => $request->khuvuc,                           
                            'intro'                 => $request->intro,
                            'content'               => $request->content,
                            'googlemap'             => $googlemap,                          
                            'contact'               => $contact,
                            'maCan'                 => $request->maCan,
                            'maToa'                 => $request->maToa,
                            'category_id'           => $request->category_id,
                            'member_id'             => $request->member_id,
                            'images'                => $fileNameTotal,
                            'position'              => $request->position,
                            'link'                  => $request->link,
                            'huong_view'            => $request->huong_view,
                            'huong_bancong'         => $request->huong_bancong,       
                            'hot'                   => $request->hot,
                            'status'                => $request->status,
                             'exp_time'				=> \Carbon\Carbon::parse($request->exp_time)->format('Y-m-d'),
                            'created_at'            => new DateTime
                        );  
             $this->temp->addCanHo($data);
             
           return redirect()->route('can-ho.index')->with('success',"message.store_required");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['temp'] = $this->temp->editCanHo($id);
        $data['cate_option'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->dmtemp->listTempByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->dmtemp->listTempByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }
        
        return view('admin.modules.temp.edit',$data);
    }

    public function Changehinh($id)
    {
        $item = $this->temp->editCanHo($id);
        $splittedstring=explode("||",$item->images);
        foreach ($splittedstring as $key) {
            $filename = 'public/uploads/postings/'.$key;
            if (!is_null($splittedstring)) {
                 if(\File::exists($filename)){
                \File::delete($filename);
                }  
            }
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CanhoStore $request, $id)
    {
        $img1="default.jpg";
        $fileNameTotal = "";

        if ($request->hasFile('images')== true) {
            $rules = array("jpg","jpeg","png","jpeg","gif","svg");
             foreach ($request->file('images') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $arr = [];
                     $fileName = time().'_'.$file->getClientOriginalName();
                     $uploadPath = 'public/uploads/postings';
                     $file->move($uploadPath, $fileName);
                    $this->Changehinh($id);
                    $fileNameTotal .= $fileName."||";
            }
        }
         $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
         $googlemap = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->googlemap ); 
             $data = array(
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'dientich'              => $request->dientich,
                        'priceCDT'              => $request->priceCDT,
                        'priceChenh'            => $request->priceChenh,
                        'khuvuc'                => $request->khuvuc,            
                        'intro'                 => $request->intro,
                        'content'               => $request->content,                      
                        'googlemap'             => $googlemap,    
                        'contact'               => $contact,
                        'maCan'                 => $request->maCan,
                        'maToa'                 => $request->maToa,
                        'category_id'           => $request->category_id,
                        'member_id'             => $request->member_id,
                        'images'                => $fileNameTotal,
                        'position'              => $request->position,
                        'link'                  => $request->link,
                        'huong_view'            => $request->huong_view,
                        'huong_bancong'         => $request->huong_bancong,       
                        'hot'                   => $request->hot,
                        'status'                => $request->status,
                        'exp_time'				=> \Carbon\Carbon::parse($request->exp_time)->format('Y-m-d'),
                        'updated_at'            => new DateTime
                    ); 
            $this->temp->updateCanHo($data,$id);   

            return redirect()->route('can-ho.index')->with('success',"message.update_required");

         } else {
             $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
             $googlemap = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->googlemap );
             $data = array(
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'dientich'              => $request->dientich,
                        'priceCDT'              => $request->priceCDT,
                        'priceChenh'            => $request->priceChenh,
                        'khuvuc'                => $request->khuvuc,            
                        'intro'                 => $request->intro,
                        'content'               => $request->content,                      
                        'googlemap'             => $googlemap,    
                        'contact'               => $contact,
                        'maCan'                 => $request->maCan,
                        'maToa'                 => $request->maToa,
                        'category_id'           => $request->category_id,
                        'member_id'             => $request->member_id,
                        'images'                => $request->imageshidden,
                        'position'              => $request->position,
                        'link'                  => $request->link,
                        'huong_view'            => $request->huong_view,
                        'huong_bancong'         => $request->huong_bancong,       
                        'hot'                   => $request->hot,
                        'status'                => $request->status,
                        'exp_time'				=> \Carbon\Carbon::parse($request->exp_time)->format('Y-m-d'),
                        'updated_at'            => new DateTime
                    );
            $this->temp->updateCanHo($data,$id);   

            return redirect()->route('can-ho.index')->with('success',"message.update_required");  
         }
          return redirect()->back()->with('error', 'Cập nhật không thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->checked;
        for ($i=0; $i < count($data); $i++) { 
            $this->temp->delCanHo($data[$i]);
        }

        return redirect()->route('can-ho.index')->with("success",'message.delete_required');
    }
    
}
