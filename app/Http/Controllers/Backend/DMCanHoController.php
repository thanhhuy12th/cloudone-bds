<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DMCanHo;
use DateTime;

class DMCanHoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $dmtemp;
    public function __construct(DMCanHo $dmtemp)
    {
        $this->dmtemp = $dmtemp;
    }
    public function index()
    {
        $data['dmtemp'] = $this->dmtemp->listDMCanHo();

        return view('admin.modules.dmtemp.index',$data);
    }

    public function DMTemp(Request $request)
    {
        $data = array(
            'name' => $request->name,
            'alias' => (empty($request->alias))? str_slug($request->name,"-"):$request->alias,
            'hot'   => $request->hot,
            'position'  => $request->position,
            'parent_id' => $request->parent_id,
            'created_at' => new DateTime
        );
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.dmtemp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->DMTemp($request);

          $insert = $this->dmtemp->addDMCanHo($data);

          if ($insert) {
             return redirect()->route('danh-muc-can-ho.index')->with("success",'message.store_required');
          } else {
             return redirect()->back()->with("error",'Thêm dữ liệu không thành công!!! ');
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['dmt'] = $this->dmtemp->editDMCanHo($id);

        return view('admin.modules.dmtemp.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $data = $this->DMTemp($request);

          $update = $this->dmtemp->updateDMCanHo($data,$id);

           if ($update) {
             return redirect()->route('danh-muc-can-ho.index')->with("success",'message.update_required');
          } else {
             return redirect()->back()->with("error",'Cập nhật dữ liệu không thành công!!! ');
          }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
         $data = $request->chk;
       for ($i = 0; $i < count($data); $i++){

                 $this->dmtemp->delDMCanHo($data[$i]);
            }
            
       return redirect()->route('danh-muc-can-ho.index')->with('success',"message.delete_required"); 
    }
}
