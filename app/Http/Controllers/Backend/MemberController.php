<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\Member\MemberRequest;
use App\Http\Controllers\Controller;
use App\Models\Member;
use DateTime;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $member;
    public function __construct(Member $member)
    {
        $this->member =$member;
    }

    public function DelImgMember($id)
    {
        $item = $this->member->editmember($id);
        $filename = 'public/uploads/avatar'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    }
    public function index()
    {
        $data['member'] = $this->member->listmember();

        return view('admin.modules.member.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberRequest $request)
    {
       /* $data = array(
            'fullname'      => $request->fullname,
            'username'      => $request->username,
            'email'         => $request->email,
            'image'         => $request->image,
            'phone'         => $request->phone,
            'password'      => bcrypt($request->password),
            'address'       => $request->address,
            'created_at'    => new DateTime,
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/avatar';
                $image->move($des,$image_name);
                $data['image']=$image_name;
            }


        $this->member->addMember($data);

        return redirect()->route('member.index',$data)->with("success",'message.store_required');*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['member'] = $this->member->editmember($id);

        return view('admin.modules.member.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $data = array(
            'fullname'      => $request->fullname,
            'username'      => $request->username,
            'email'         => $request->email,
            'image'         => $request->image,
            'phone'         => $request->phone,
            'password'      => bcrypt($request->password),
            'address'       => $request->address,
            'reg_status'    => $request->reg_status,
            'created_at'    => new DateTime,
        );
       if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/avatar';
                $image->move($des,$image_name);
                $data['image']=$image_name;

                
                $this->DelImgMember($id);
            } 

        $this->member->updatemember($data,$id);

        return redirect()->route('member.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
        for ($i=0; $i < count($data); $i++) { 
            $this->member->delmember($data[$i]);
        }

        return redirect()->route('member.index')->with("success",'message.delete_required');
    }
}
