<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\TinDang\PostingsRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\TinDang;
use App\Models\Member;
use App\Models\DMTD;
use DB,DateTime;
use Carbon;


class TinDangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $tindang;
    private $danhMucTinDang;
   
    public function __construct(
        TinDang $tindang,
        Member $member, 
        DMTD $danhMucTinDang
    )
    {
        $this->postings = $tindang;
        $this->member = $member;
        $this->danhMucTinDang = $danhMucTinDang;
    }
   
    public function index()
    {
        $data['postings'] = $this->postings->listPostings();

        return view('admin.modules.tindang.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                        
        $data['parent'] = DB::table('bds_dmtd')->select('id','name','parent_id')->get()->toArray();
        $data['cate_option'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->danhMucTinDang->listCateByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->danhMucTinDang->listCateByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }
        //dd($data['cate_option']);

        return view('admin.modules.tindang.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    public function store(PostingsRequest $request)
    {
        
        $img1="default.jpg";
        $fileNameTotal = "";
       
        if ($request->hasFile('images')) {

            $rules = array("jpg","jpeg","png","jpeg","gif","svg");
             foreach ($request->file('images') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $arr = [];
                     $fileName = time().'_'.$file->getClientOriginalName();
                     $uploadPath = 'public/uploads/postings';
                     $file->move($uploadPath, $fileName);
                    $fileNameTotal .= $fileName."||";

                 }
            }
            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
            $vitrinew = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->vitridacdia );
            $data = array(
                            'title'                 => $request->title,
                            'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                            'dientichmatbang'       => $request->dientichmatbang,
                            'dientichsudung'        => $request->dientichsudung,
                            'price'                 => $request->price,
                            'typegia'               => $request->typegia,
                            'vitri'                 => $request->vitri,
                            'province'              => $request->province,
                            'district'              => $request->district,
                            'intro'                 => $request->intro,
                            'tongquan'              => $request->tongquan,
                            'vitridacdia'           => $vitrinew,
                            'typegia'               => $request->typegia,
                            'contact'               => $contact,
                            'category_id'           => $request->category_id,
                            'member_id'             => $request->member_id,
                            'position'              => $request->position,
                            'images'                => $fileNameTotal,
                            'status'                => $request->status,
                            'duyetbai'              => $request->duyetbai,
                            'exp_time'              => \Carbon\Carbon::parse($request->exp_time)->format('Y-m-d'),                           
                            'created_at'            => new DateTime
                        );  
            $this->postings->addPostings($data);
            return redirect()->route('tindang.index')->with('success',"message.store_required");
        } else{
            $img1="default.jpg";
            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
            $vitrinew = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->vitridacdia );
            $data = array(
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'dientichmatbang'       => $request->dientichmatbang,
                        'dientichsudung'        => $request->dientichsudung,
                        'price'                 => $request->price,
                        'typegia'               => $request->typegia,
                        'vitri'                 => $request->vitri,
                        'province'              => $request->province,
                        'district'              => $request->district,
                        'intro'                 => $request->intro,
                        'tongquan'              => $request->tongquan,
                        'vitridacdia'           => $vitrinew,
                        'typegia'               => $request->typegia,
                        'contact'               => $contact,
                        'category_id'           => $request->category_id,
                        'member_id'             => $request->member_id,
                        'position'              => $request->position,
                        'images'                => $img1,
                        'status'                => $request->status,
                        'duyetbai'              => $request->duyetbai,
                        'exp_time'              => \Carbon\Carbon::parse($request->exp_time)->format('Y-m-d'),                      
                        'created_at'            => new DateTime
                    );   
            $this->postings->addPostings($data);
            return redirect()->route('tindang.index')->with('success',"message.store_required");
        } 

    }

   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['post'] = $this->postings->editPostings($id);
       
        $data['cate_option'] = [];
        // Xử lý option danh mục tin đăng
        $option_parent = $this->danhMucTinDang->listCateByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->danhMucTinDang->listCateByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }

        return view('admin.modules.tindang.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Changehinh($id)
    {
        $item = $this->postings->editPostings($id);
        $splittedstring=explode("||",$item->images);
        foreach ($splittedstring as $key) {
            $filename = 'public/uploads/postings/'.$key;
            if (!is_null($splittedstring)) {
                 if(\File::exists($filename)){
                \File::delete($filename);
                }  
            }
        }
       
    }
     public function update(PostingsRequest $request, $id)
    {

        $img1="default.jpg";
        $fileNameTotal = "";
       
        if ($request->hasFile('images')== true) {
            $rules = array("jpg","jpeg","png","jpeg","gif","svg");
             foreach ($request->file('images') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $arr = [];
                     $fileName = time().'_'.$file->getClientOriginalName();
                     $uploadPath = 'public/uploads/postings';
                     $file->move($uploadPath, $fileName);
                    $this->Changehinh($id);
                    $fileNameTotal .= $fileName."||";
            }
        }

         $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
             $data = array(
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'dientichmatbang'       => $request->dientichmatbang,
                        'dientichsudung'        => $request->dientichsudung,
                        'price'                 => $request->price,
                        'typegia'               => $request->typegia,
                        'vitri'                 => $request->vitri,
                        'province'              => $request->province,
                        'district'              => $request->district,
                        'intro'                 => $request->intro,
                        'tongquan'              => $request->tongquan,                       
                        'vitridacdia'           => $request->vitridacdia,
                        'contact'               => $contact,
                        'category_id'           => $request->category_id,
                        'member_id'             => $request->member_id,
                        'position'              => $request->position,
                        'images'                => $fileNameTotal,
                        'status'                => $request->status,
                        'duyetbai'              => $request->duyetbai,
                        'exp_time'              => \Carbon\Carbon::parse($request->exp_time)->format('Y-m-d'),                      
                        'created_at'            => new DateTime
                    ); 
          $this->postings->updatePostings($data,$id);   

          return redirect()->route('tindang.index')->with('success',"message.update_required");

         } else {
            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
            $data = array(
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'dientichmatbang'       => $request->dientichmatbang,
                        'dientichsudung'        => $request->dientichsudung,
                        'price'                 => $request->price,
                        'typegia'               => $request->typegia,
                        'vitri'                 => $request->vitri,
                        'province'              => $request->province,
                        'district'              => $request->district,
                        'intro'                 => $request->intro,
                        'tongquan'              => $request->tongquan,
                        'vitridacdia'           => $request->vitridacdia,
                        'contact'               => $contact,
                        'category_id'           => $request->category_id,
                        'member_id'             => $request->member_id,
                        'position'              => $request->position,
                        'images'                => $request->hinhhidden,
                        'status'                => $request->status,
                        'duyetbai'              => $request->duyetbai,
                        'exp_time'              => \Carbon\Carbon::parse($request->exp_time)->format('Y-m-d'),                       
                        'created_at'            => new DateTime
                    );       
            //dd($data);
        $this->postings->updatePostings($data,$id);

        return redirect()->route('tindang.index')->with('success',"message.update_required");
            
         } 
        
         return redirect()->back()->with('error', 'Cập nhật không thành công!');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->checked;
        for ($i=0; $i < count($data); $i++) { 
            $this->postings->delPostings($data[$i]);
        }

        return redirect()->route('tindang.index')->with("success",'message.delete_required');
    }
}
