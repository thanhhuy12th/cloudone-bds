<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\Menu\MenuRequest;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use DB,DateTime;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $menu;
    public function __construct(Menu $menu)
    {
        $this->menu = $menu;
    }
    public function createMenu($request)
    {
        $data = array(
            'name'      => $request->name,
            'slug'      => (empty($request->slug))? str_slug($request->name,"-"):$request->slug,
            'parent_id' => $request->parent_id,
            'link'      => $request->link,
            'created_at'=> new DateTime
        );
        return $data;
    }
    public function index()
    {
        $data['menu'] = $this->menu->listmenu();

        return view('admin.modules.menu.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$data['menus'] = DB::table('bds_menu')->select('id','name','parent_id')->get()->toArray();  

        return view('admin.modules.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        $data = $this->createMenu($request);

        $this->menu->addmenu($data);

        return redirect()->route('menu.index',$data)->with("success",'message.store_required');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // $data['menus'] = DB::table('bds_menu')->select('id','name','parent_id')->get()->toArray();

        $data['menu'] = $this->menu->editmenu($id);

        return view('admin.modules.menu.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->createMenu($request);

        $this->menu->updatemenu($data,$id);

        return redirect()->route('menu.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
        for ($i=0; $i < count($data); $i++) { 
            $this->menu->delmenu($data[$i]);
        }

        return redirect()->route('menu.index')->with("success",'message.delete_required');
    }
}
