<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\Slider\SliderRequest;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\DMCanHo;
use DateTime;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $slider;
    private $dmtemp;
    public function __construct(Slider $slider, DMCanHo $dmtemp)
    { 
        $this->slider =$slider;
        $this->dmtemp = $dmtemp;
    }
    public function createSlider($request)
    {
        $data =array(
            'name'      => $request->name,
            'intro'     => $request->intro,
            'dmcanho_id'=> $request->dmcanho_id,
            'created_at'=> new DateTime
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/slider';
                $image->move($des,$image_name);
                $data['image']=$image_name;
            }

        return $data; 
    }
     public function UpdateImage($id)
    {
        $item = $this->slider->editSlider($id);
        $filename = 'public/uploads/slider'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    }
    public function index()
    {
        $data['slider'] = $this->slider->listSlider();

        return view('admin.modules.slider.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $data['cate_option'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->dmtemp->listTempByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->dmtemp->listTempByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }
        return view('admin.modules.slider.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request)
    {
        $data = $this->createSlider($request);

        $this->slider->addSlider($data);

        return redirect()->route('slider.index',$data)->with("success",'message.store_required');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['slider'] = $this->slider->editSlider($id);

         $data['cate_option'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->dmtemp->listTempByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->dmtemp->listTempByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }

        return view('admin.modules.slider.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $data =array(
            'name'      => $request->name,
            'intro'     => $request->intro,
            'dmcanho_id'=> $request->dmcanho_id,
            'created_at'=> new DateTime
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/slider';
                $image->move($des,$image_name);
                $data['image']=$image_name;

                $this->UpdateImage($id);
            }

        $this->slider->updateSlider($data,$id);

        return redirect()->route('slider.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data); $i++) { 
            $this->slider->delSlider($data[$i]);
        }

        return redirect()->route('slider.index')->with("success",'message.delete_required');
    }
}
