<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\DuAn\DuAnResquest;
use App\Http\Controllers\Controller;
use App\Models\DuAn;
use DateTime;

class DuAnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $duan;
    public function __construct(DuAn $duan)
    {
        $this->duan = $duan;
    }
    public function createDuAn($request)
    {
       $data = array(
            'name'      => $request->name,
            'title'     => $request->title,
            'alias'     => (empty($request->alias))? str_slug($request->name,"-"):$request->alias,
            'dt'        => $request->dt,
            'giaban'    => $request->giaban,
            'vitridacdia'=> $request->vitridacdia,
            'intro'     => $request->intro,
            'tongquan'  => $request->tongquan,
            'matbang'   => $request->matbang,
            'hinh'      => $request->hinh,
            'contact'   => $request->contact,    
            'status'    => $request->status,
            'created_at'=> new DateTime
        );
        if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/postings';
                $image->move($des,$image_name);
                $data['image']=$image_name;
        }

        return $data;
    }
    public function ImageUpdate($id)
    {
        $item = $this->duan->editduan($id);
        $filename = 'public/uploads/postings'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    }
    public function index()
    {
        $data['duan'] = $this->duan->listduan();

        return view('admin.modules.duan.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.duan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DuAnResquest $request)
    {
        $data = $this->createDuAn($request);

        $this->duan->addduan($data);

        return redirect()->route('duan.index',$data)->with("success",'message.store_required');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['duan'] = $this->duan->editduan($id);

        return view('admin.modules.duan.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'name'      => $request->name,
            'title'     => $request->title,
            'alias'     => (empty($request->alias))? str_slug($request->name,"-"):$request->alias,
            'dt'        => $request->dt,
            'giaban'    => $request->giaban,
            'vitridacdia'=> $request->vitridacdia,
            'intro'     => $request->intro,
            'tongquan'  => $request->tongquan,
            'matbang'   => $request->matbang,
            'hinh'      => $request->hinh,
            'contact'   => $request->contact,    
            'status'    => $request->status,
            'created_at'=> new DateTime
        );
        if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/postings';
                $image->move($des,$image_name);
                $data['image']=$image_name;

                $this->ImageUpdate($id);
        }


        $this->duan->updateduan($data,$id);

        return redirect()->route('duan.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data); $i++) { 
            $this->duan->delduan($data[$i]);
        }

        return redirect()->route('duan.index')->with("success",'message.delete_required');
    }
}
