<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Logo;
use DateTime;

class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $logo;
    public function __construct(Logo $logo)
    {
        $this->logo = $logo;
    }
    public function createLogo($request)
    {
        $data = array(
            'name'      => $request->name,
            'created_at'=> new DateTime 
        );
        if($request->hasFile('content')){
                $content = $request->file('content');
                $content_name = time(). '-'.$content->getClientOriginalName();
                $des = 'public/uploads/logo';
                $content->move($des,$content_name);
                $data['content']=$content_name;
            }
        return $data;
    }
    public function UpdateLogo($id)
    {
        $item = $this->logo->editLogo($id);
        $filename = 'public/uploads/logo'.$item->content;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    }
    public function index()
    {
        $data['logo'] = $this->logo->editLogo(5);
          
        return view('admin.modules.logo.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.logo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->createLogo($request);

        $this->logo->addLogo($data);

        return redirect()->route('logo.index',$data)->with("success",'message.store_required');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        return view('admin.modules.logo.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'name'      => $request->name,
            'created_at'=> new DateTime 
        );
        if($request->hasFile('content')){
                $content = $request->file('content');
                $content_name = time(). '-'.$content->getClientOriginalName();
                $des = 'public/uploads/logo';
                $content->move($des,$content_name);
                $data['content']=$content_name;

                $this->UpdateLogo($id);
            }

            $this->logo->updateLogo($data,$id);

            return redirect()->route('logo.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }
}
