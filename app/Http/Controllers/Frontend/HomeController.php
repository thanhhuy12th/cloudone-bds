<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Login\LoginStore;
use App\Http\Requests\SignUp\SignupStore;
use App\Http\Requests\TinDang\PostingsRequest;
use App\Http\Requests\Canho\CanhoStore;
use App\Models\TinDang;
use App\Models\CanHo;
use App\Models\DMTD;
use App\Models\DMCanHo;
use App\Models\Member;
use App\Http\Controllers\EmailController;
use Auth;
use Hash;
use DB,Mail,DateTime;


class HomeController extends Controller
{
    private $tindang;
    private $danhMucTinDang;
    private $member;
    private $reg;
    private $dmcanho;
    private $canho;
    public function __construct(
        TinDang $tindang, 
        DMTD $danhMucTinDang,
        Member $member,
        EmailController $reg,
        DMCanHo $dmcanho,
        CanHo $canho
    )
    {
        $this->postings = $tindang;
        $this->danhMucTinDang = $danhMucTinDang;
        $this->member = $member;
        $this->reg = $reg;
        $this->dmcanho = $dmcanho;
        $this->canho = $canho;
    }
    public function delImages($id)
    {
        $item = $this->member->editmember($id);
        $filename = 'public/uploads/avatar'.$item->image;
        if (!is_null($item->image)) {
         if(\File::exists($filename)){
            \File::delete($filename);
        }  
    }

}
public function DelImg($id)
{
    $item = $this->postings->editPostings($id);
    $filename = 'public/uploads/postings'.$item->image;
    if (!is_null($item->image)) {
     if(\File::exists($filename)){
        \File::delete($filename);
    }  
}
}


public function index()
{
    $data['get_canho'] = DB::table('bds_canho')->where('hot',1)->where('status',1)->orderBy('position','ASC')->orderBy('id','DESC')->take(20)->get();
    $data['nhadatnoibat'] = DB::table('bds_tindang')->where('duyetbai',1)->where('status',1)->orderBy('position','ASC')->orderBy( 'id','DESC')->take(8)->get()->toArray();
    $data['slider'] = DB::table('bds_slider')->where('dmcanho_id',0)->orderBy('image', 'DESC')->take(4)->get();
    return view('frontend.pages.home',$data);
}

public function getViewCanho()
{
 $data['get_canho'] = DB::table('bds_canho')->where('hot',1)->where('status',1)->orderBy('position','ASC')->orderBy('id','DESC')->limit(16)->paginate(16);
 $data['slider'] = DB::table('bds_slider')->orderBy('image', 'DESC')->take(4)->get();
 return view('frontend.pages.viewmore_canho',$data);
}

public function getViewNhadat()
{
 $data['nhadatnoibat'] = DB::table('bds_tindang')->where('duyetbai',1)->orderBy('position','ASC')->orderBy('id','DESC')->limit(16)->paginate(16);
 $data['slider'] = DB::table('bds_slider')->orderBy('image', 'DESC')->take(4)->get();
 return view('frontend.pages.viewmore_nhadat',$data);
}
    // tin dang
public function product_category($alias,$parent)
{
    // dd(123);
    $getID = DB::table('bds_dmtd')->select('id')->where('parent_id',$parent)->where('alias',$alias)->get();

    $data['product_category'] = DB::table('bds_tindang')->where('duyetbai',1)->where('category_id', $getID[0]->id)->orderBy('position','ASC')->orderBy('id','DESC')->limit(8)->paginate(6);
    $data['nhadat'] = DB::table('bds_tindang')->where('duyetbai',1)->orderBy('id','DESC')->take(5)->get()->toArray();

    return view('frontend.pages.product_category',$data);
}
public function get_category_parent($alias)
{
    $data['nhadat'] = DB::table('bds_tindang')->where('duyetbai',1)->where('status',1)->orderBy('position','ASC')->orderBy('id','DESC')->take(5)->get()->toArray();
    // dd($data['nhadat']);
    $data['alias'] = $alias;
    return view('frontend.pages.get-category-parent',$data);
}
public function product_detail($alias)
{


    $data['cates_detail'] = DB::table('bds_tindang')->where('alias',$alias)->first();
    $data['canho_hot']    = DB::table('bds_canho')->where('hot',1)->where('status',1)->orderBy('id','DESC')->take(5)->get()->toArray();
    $data['nhadat'] = DB::table('bds_tindang')->where('duyetbai',1)->where('status',1)->orderBy('id','DESC')->take(5)->get()->toArray();

    $id = $data['cates_detail']->id;
    $data['nhadatnoibat'] = DB::table('bds_tindang')->where('status',1)->where('id','!=', $id)->orderBy('position','ASC')->orderBy('id','DESC')->take(4)->get()->toArray();
    $data['canhonoibat']  = DB::table('bds_canho')->where('hot',1)->orderBy('position','ASC')->orderBy('id','DESC')->take(4)->get()->toArray(); 

        // $id_cate = $data['cates_detail']->category_id;

        // $data['nhadatlienquan'] = DB::table('bds_tindang')->where('duyetbai',1)->where('category_id',$id_cate)->where('id','!=', $id)->orderBy('id','DESC')->take(4)->get()->toArray();


    return view('frontend.pages.product_detail',$data);

}

public function getCan_ho($alias)
{
    $getID = DB::table('bds_dmcanho')->select('id')->where('alias',$alias)->get();

    $data['slider'] = DB::table('bds_slider')
    ->select('bds_slider.*')
    ->join('bds_dmcanho', 'bds_dmcanho.id', '=', 'bds_slider.dmcanho_id')
    ->where('bds_dmcanho.id',$getID[0]->id)
    ->orderBy('id', 'DESC')->take(4)->get();

    $data['alias'] = $alias;


    return view('frontend.pages.canho',$data);

}


public function temp_detail($alias)
{
    $data['canho_detail'] = DB::table('bds_canho')->where('alias',$alias)->first();
    $data['canho_hot']    = DB::table('bds_canho')->where('hot',1)->where('status',1)->orderBy('id','DESC')->take(5)->get()->toArray();
    $data['nhadat'] = DB::table('bds_tindang')->where('duyetbai',1)->where('status',1)->orderBy('id','DESC')->take(5)->get()->toArray();

    $id_cate_canho = $data['canho_detail']->category_id;

    $id = $data['canho_detail']->id;
    $data['canhonoibat']  = DB::table('bds_canho')->where('status',1)->where('hot',1)->where('category_id',$id_cate_canho)->where('id','!=', $id)->orderBy('position','ASC')->orderBy('id','DESC')->take(4)->get()->toArray();

    $data['nhadatnoibat'] = DB::table('bds_tindang')->where('status',1)->where('id','!=', $id)->orderBy('position','ASC')->orderBy('id','DESC')->take(4)->get()->toArray();

    return view('frontend.pages.temp_detail',$data);
}
    // gioi thieu
public function about()
{
 $data['gioithieu'] = DB::table('bds_config')->whereId(4)->first();
 $data['title'] = DB::table('bds_config')->whereId(20)->first();
 $data['video'] = DB::table('bds_video')->orderBy('id', 'DESC')->first();
 $data['hotline1'] = DB::table('bds_config')->whereId(9)->first();
 $data['hotline2'] = DB::table('bds_config')->whereId(10)->first();
 $data['hotline3'] = DB::table('bds_config')->whereId(11)->first();
 $data['address'] = DB::table('bds_config')->whereId(22)->first();
 $data['email'] = DB::table('bds_config')->whereId(17)->first();
 $data['link'] = DB::table('bds_config')->whereId(21)->first();
 $data['ggmap'] = DB::table('bds_config')->whereId(14)->first();

 return view('frontend.pages.gioithieu',$data);
}



    // tin tuc
public function getNews()
{

    $data['news'] = DB::table('bds_tintuc')->orderBy('id', 'DESC')->limit(10)->paginate(6);
    $data['canho'] = DB::table('bds_canho')->where('hot',1)->orderBy('id', 'DESC')->take(10)->get()->toArray();
    $data['tinmoi'] = DB::table('bds_tintuc')->orderBy('id', 'DESC')->take(10)->get()->toArray();

    return view('frontend.pages.tintuc',$data);
}
public function news_detail($alias)
{
    $data['chitiettin'] = DB::table('bds_tintuc')->where('alias',$alias)->first();
    $data['canho'] = DB::table('bds_canho')->where('hot',1)->orderBy('id', 'DESC')->take(10)->get()->toArray();
    $data['tinmoi'] = DB::table('bds_tintuc')->orderBy('id','DESC')->take(10)->get()->toArray();

    return view('frontend.pages.news_detail',$data);
}


    // Liên hệ
public function ContactCompany()
{
    return view('frontend.pages.lienhecongty');
}
    // lien he với chúng tôi
public function contactUs()
{
    return view('frontend.pages.contactUs');
}
public function storeContact(Request $request)
{
    $captcha = null;
    if(isset($_POST['g-recaptcha-response'])){
        $captcha=$_POST['g-recaptcha-response'];
    }
    if(!$captcha){
      echo "<script>
      alert('Chưa nhập mã xác thực!');
      window.location = '".url('/contact')."'
      </script>";
  }
  else {
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LewA7oUAAAAANF1uxgFWakpahNP3RdOOdsIdyuR&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
    $obj =json_decode($response); 
    if($obj->{'success'}==true)            
    {
        $data = array(
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'content'   => $request->content,
            'created_at'=> new DateTime 
        );
        DB::table('bds_lienhe')->insert($data);

        echo "<script>
        alert('Cảm ơn bạn đã liên hệ. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất');
        window.location = '".url('/')."'
        </script>";
    }
    else {
        echo "<script>
        alert('Sai mã xác thực, vui lòng nhập lại');
        window.location = '".url('/contact')."'
        </script>";

    }
}      

}

    // dang nhap
public function getDangnhap()
{
    return view('frontend.login-user');
}
public function postDangnhap(LoginStore $request)
{
     // $credentials = [
     //        'email'     => $request['email'],
     //        'password'  => $request['password'],
     //        'reg_status'  => 1,
     //    ];
        
     //    if (Auth::attempt($credentials) ){

     //               echo "<script>
     //                alert('Đăng nhập thành công !');
     //                window.location = '".url('/quan-ly-tai-khoan')."'
     //                </script>";
     //    } else {

     //        return redirect()->route('dangnhap')->with('error',"message.dangnhap_required");
     //    }


     // $credentials = [
     //        'email'     => $request['email'],
     //        'password'  => $request['password'],
     //        'reg_status'  => 1,
     //    ];
     //    if (Auth::attempt($credentials) ){

     //       echo "<script>
     //       alert('Đăng nhập thành công !');
     //       window.location = '".url('/quan-ly-tai-khoan')."'
     //       </script>";
     //   }



    $captcha = null;
    if(isset($_POST['g-recaptcha-response'])){
        $captcha=$_POST['g-recaptcha-response'];
    }
    if(!$captcha){
      echo "<script>
      alert('Chưa nhập mã xác thực!');
      window.location = '".url('/dangnhap')."'
      </script>";
  }
  else {
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LewA7oUAAAAANF1uxgFWakpahNP3RdOOdsIdyuR&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
    $obj =json_decode($response); 
    if($obj->{'success'}==true)            
    {
        $credentials = [
            'email'     => $request['email'],
            'password'  => $request['password'],
            'reg_status'  => 1,
        ];
        if (Auth::attempt($credentials) ){

           echo "<script>
           alert('Đăng nhập thành công !');
           window.location = '".url('/quan-ly-tai-khoan')."'
           </script>";

       } else {

           echo "<script>
           alert('Mật khẩu không đúng, vui lòng nhập lại');
           window.location = '".url('/dangnhap')."'
           </script>";


       }
   }
   else {

       echo "<script>
       alert('Sai mã xác thực, vui lòng nhập lại');
       window.location = '".url('/dangnhap')."'
       </script>";


   }
}      

}

    // dang xuat
public function getDangxuat()
{
    Auth::logout();

    return redirect()->route('home');
}
     // dang ky tai khoan
public function getDangky()
{
    return view('frontend.sign-up');
}
public function postDangky(Request $request)
{

    $captcha = null;
    if(isset($_POST['g-recaptcha-response'])){
        $captcha=$_POST['g-recaptcha-response'];
    }
    if(!$captcha){
      echo "<script>
      alert('Chưa nhập mã xác thực!');
      window.location = '".url('/dangky')."'
      </script>";
  }
  else {
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LewA7oUAAAAANF1uxgFWakpahNP3RdOOdsIdyuR&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
    $obj =json_decode($response); 
    if($obj->{'success'}==true)            
    {
        $now        = new DateTime();
        $reg_token      = md5($now->format('Y-m-d HH:mm:ss').$request->email.rand());
        $urlBase = 'https://muanhavn.net/'; 
        $data = array(
            'phone'         => $request->phone,
            'email'         => $request->email,
            'password'      => bcrypt($request->password),
            'reg_status'    => 0,
            'reg_token'     => $reg_token,
            'created_at'    => new DateTime
        );
        $Content = 'Chào bạn, <p>Bạn cần xác thực email để đăng nhập</p> <p>Bạn vui lòng click vào đường dẫn bên dưới để xác thực tài khoản.</p>'.$urlBase.'register?regtoken='.$reg_token.' <p>Trân trọng,</p><p>MUANHAVN.NET</p>';

        $this->reg->register_account($Content, $request->email);

        $register =  DB::table('bds_member')->insert($data);

        if ($register) {

            echo "<script>
            alert('Chào bạn, Bạn vui lòng kích hoạt email để đăng nhập vào tài khoản.');
            window.location = '".url('/dangnhap')."'
            </script>";
        } 
        else {

         echo "<script>
         alert('Sai mã xác thực, vui lòng nhập lại');
         window.location = '".url('/dangky')."'
         </script>";

     }   
    }
  }   
}
public function Registersucces()
{

    return view('auth.email.register');
}
public function RegisterAccount()
{
    $reg_token = \Illuminate\Support\Facades\Request::post('regtoken');
    $data = array(
        'reg_status'    => 1,
    );
    $register =  $this->member->updateToken($data,$reg_token);

    return view('auth.email.register');
}

    // quan ly tai khoan

public function create()
{

    $data['cate_option'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->danhMucTinDang->listCateByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->danhMucTinDang->listCateByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }        

        return view('frontend.quanlytindang',$data);
    }


    public function store(Request $request)
    {
        // dd($request->all());
       $img1="default.jpg";
        $fileNameTotal = "";
       
        if ($request->hasFile('images')) {

            $rules = array("jpg","jpeg","png","jpeg","gif","svg");
             foreach ($request->file('images') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $arr = [];
                     $fileName = time().'_'.$file->getClientOriginalName();
                     $uploadPath = 'public/uploads/postings';
                     $file->move($uploadPath, $fileName);
                    $fileNameTotal .= $fileName."||";

                 }
            }

            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
            $vitrinew = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->vitridacdia );
            $data = array(
                            'title'                 => $request->title,
                            'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                            'dientichmatbang'       => $request->dientichmatbang,
                            'dientichsudung'        => $request->dientichsudung,
                            'price'                 => $request->price,
                            'typegia'               => $request->typegia,
                            'vitri'                 => $request->vitri,
                            'province'              => $request->province,
                            'district'              => $request->district,
                            'intro'                 => $request->intro,
                            'tongquan'              => $request->tongquan,
                            'vitridacdia'           => $vitrinew,                           
                            'contact'               => $contact,
                            'category_id'           => $request->category_id,
                            'member_id'             => Auth::user()->id,                       
                            'images'                => $fileNameTotal,                                                     
                            'created_at'            => new DateTime
                        );  
            $this->postings->addPostings($data);
            return redirect()->back()->with('success',"message.store_required");
        }else{
            $img1="default.jpg";
            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
            $vitrinew = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->vitridacdia );
            $data = array(
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'dientichmatbang'       => $request->dientichmatbang,
                        'dientichsudung'        => $request->dientichsudung,
                        'price'                 => $request->price,
                        'typegia'               => $request->typegia,
                        'vitri'                 => $request->vitri,
                        'province'              => $request->province,
                        'district'              => $request->district,
                        'intro'                 => $request->intro,
                        'tongquan'              => $request->tongquan,
                        'vitridacdia'           => $vitrinew,                        
                        'contact'               => $contact,
                        'category_id'           => $request->category_id,
                        'member_id'             => Auth::user()->id,                      
                        'images'                => $img1,                                  
                        'created_at'            => new DateTime
                    );   
            $this->postings->addPostings($data);
            return redirect()->back()->with('success',"message.store_required");
        } 

 }
 public function edit($alias)
 {
    $data['post'] = $this->postings->editTinDangByAlias($alias);

    $data['cate_option'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->danhMucTinDang->listCateByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->danhMucTinDang->listCateByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }

        return view('frontend.editquanlytd',$data);
    }

    public function DoiNhieuImages($alias)
    {
        $item = $this->postings->editTinDangByAlias($alias);
        $splittedstring=explode("||",$item->images);       
        foreach ($splittedstring as $key) {
            $filename = 'public/uploads/postings/'.$key;
            if (!is_null($splittedstring)) {
             if(\File::exists($filename)){
                \File::delete($filename);
            }  
        }
    }

}

public function update(PostingsRequest $request, $alias)
{
    $item = $this->postings->editTinDangByAlias($alias);
    $img1="default.jpg";
    $fileNameTotal = "";
    
    if ($request->hasFile('images')) {
        $rules = array("jpg","jpeg","png","jpeg","gif","svg");
        foreach ($request->file('images') as $file) {
            $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            if (in_array($fileExtension,$rules)) {
                $arr = [];
                $fileName = time().'_'.$file->getClientOriginalName();

                $uploadPath = 'public/uploads/postings';
                $file->move($uploadPath, $fileName);
                $this->DoiNhieuImages($alias);
                $fileNameTotal .= $fileName."||";

            }
        }
        $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact;
        $data = array(
            'title'                 => $request->title,
            'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
            'dientichmatbang'       => $request->dientichmatbang,
            'dientichsudung'        => $request->dientichsudung,
            'price'                 => $request->price,
            'typegia'               => $request->typegia,
            'vitri'                 => $request->vitri,
            'province'              => $request->province,
            'district'              => $request->district,
            'intro'                 => $request->intro,
            'tongquan'              => $request->tongquan,                       
            'vitridacdia'           => $request->vitridacdia,
            'contact'               => $contact,
            'category_id'           => $request->category_id,
            'member_id'             => Auth::user()->id,            
            'images'                => $fileNameTotal,                         
            'updated_at'            => new DateTime
        ); 
        
        $this->postings->updatePostings($data,$item->id);   

        return redirect()->back()->with('success',"message.update_required");

    } else {
        $img1="default.jpg";
        $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact;
        $data = array(
            'title'                 => $request->title,
            'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
            'dientichmatbang'       => $request->dientichmatbang,
            'dientichsudung'        => $request->dientichsudung,
            'price'                 => $request->price,
            'typegia'               => $request->typegia,
            'vitri'                 => $request->vitri,
            'province'              => $request->province,
            'district'              => $request->district,
            'intro'                 => $request->intro,
            'tongquan'              => $request->tongquan,
            'vitridacdia'           => $request->vitridacdia,
            'contact'               => $contact,
            'category_id'           => $request->category_id,
            'member_id'             => Auth::user()->id,           
            'images'                => $request->imageshidden,                        
            'updated_at'            => new DateTime
        );       

        $this->postings->updatePostings($data,$item->id);

        return redirect()->back()->with('success',"message.update_required");

    }
    return redirect()->back()->with('error', 'Cập nhật không thành công!');

}


public function editQuanlytk()
{

    $data['postnews'] = DB::table('bds_tindang')
            ->select('bds_tindang.*')
            ->join('bds_dmtd', 'bds_tindang.category_id', '=', 'bds_dmtd.id')
            ->join('bds_member', 'bds_tindang.member_id', '=', 'bds_member.id')
            ->where('bds_tindang.member_id', '=',Auth::user()->id)
            ->where('bds_tindang.user_type', 2)
            ->paginate(10);
    $data['canho'] = DB::table('bds_canho')
            ->select('bds_canho.*')
            ->join('bds_dmcanho', 'bds_canho.category_id', '=', 'bds_dmcanho.id')
            ->join('bds_member', 'bds_canho.member_id', '=', 'bds_member.id')
            ->where('bds_canho.member_id', '=',Auth::user()->id)
            ->where('bds_canho.user_type', 2)
            ->paginate(10);
     
    return view('frontend.quanlytk',$data);
}

public function updateQuanlytk(Request $request)
{

    $data = array(
        'fullname'      => $request->fullname,
        'username'      => $request->username,
        'email'         => $request->email,
        'phone'         => $request->phone,
        'address'       => $request->address,
        'created_at'    => new DateTime
    );

    if($request->hasFile('image')){
        $image = $request->file('image');
        $image_name = time(). '-'.$image->getClientOriginalName();
        $des = 'public/uploads/avatar';
        $image->move($des,$image_name);
        $data['image']=$image_name;
        $this->delImages(Auth::user()->id);
    }

           // dd($data);
    DB::table('bds_member')->whereId(Auth::user()->id)->update($data);

    echo "<script>
    alert('Thông tin của bạn đã được thay đổi thành công.');
    window.location = '".url('/')."'
    </script>";
}

public function destroyQuanlytd($id)
{
   DB::table('bds_tindang')->whereId($id)->delete();

   return back()->with("success", "message.delete_required");

}

     // doi mat khau

public function updateChangePassword(Request $request)
{
 if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // Mật khẩu trùng khớp

    return redirect()->back()->with("error","Mật khẩu hiện tại của bạn không khớp với mật khẩu bạn cung cấp. Vui lòng thử lại.");
}
if(Hash::check($request->get('new-password'), Auth::user()->password)) {
            //Mật khẩu hiện tại và mật khẩu mới giống nhau

    return redirect()->back()->with("error","Mật khẩu mới không thể giống như mật khẩu hiện tại của bạn. Vui lòng chọn một mật khẩu khác nhau.");
}

$validatedData = $request->validate([
    'current-password' => 'required',
    'new-password' => 'required|min:6|confirmed',
]);
        // Đổi mật khẩu

$data['password'] = bcrypt($request->get('new-password'));


DB::table('bds_member')->whereId(Auth::user()->id)->update($data);

echo "<script>
alert('Mật khẩu của bạn đã được thay đổi thành công.');
window.location = '".url('/')."'
</script>";


}

    // tim kiem
public function get_timkiem()
{
    return view('frontend.pages.timkiem_category');
}
public function timkiem(Request $request) {
    $danhmuc    = $request->danhmuc;
    $province   = (int)$request->province;
    $district   = (int)$request->district;
    $dientich   = $request->dientichmatbang;
    $price      = $request->price;
    $param      = array();
    $where      = "";

    $data_dm    = DB::table('bds_dmtd')->where('parent_id', $danhmuc)->get();
    if($data_dm->count() > 0) {
        $condition  = "OR ";
        $counter    = 0;
        foreach($data_dm as $r_dm) {
            if (++$counter == count($data_dm)) {
                $condition .= "category_id=".$r_dm->id;
            } else {
                $condition .= "category_id=".$r_dm->id." OR ";
            }
        }
        $where  .= "WHERE category_id = $danhmuc $condition";
    }
    else {
        $where  .= "WHERE category_id = $danhmuc";
    }



    if($province != -1) {
        $where  .= " AND province = $province ";
        $param = array('danhmuc' => $danhmuc, 'province' => $province);
    }
    if($district != -1) {
        $where  .= " AND district = $district ";
        $param = array('danhmuc' => $danhmuc, 'province' => $province,'district' => $district);
    }

    switch ($dientich) {
        case 'less-15':
        $where  .= " AND dientichmatbang < 15 ";

        break;
        case '15-25':
        $where  .= " AND dientichmatbang >= 15 AND dientichmatbang < 25 ";

        break;
        case '25-50':
        $where  .= " AND dientichmatbang >= 25 AND dientichmatbang < 50 ";

        break;
        case '50-100':
        $where  .= " AND dientichmatbang >= 50 AND dientichmatbang < 100 ";

        break;
        case 'more-100':
        $where  .= " AND dientichmatbang > 100 ";

        break;

        default:
        $where  .= "";
        break;
    }

    switch ($price) {
        case 'less-1m':
        $where  .= " AND price < 1000000";

        break;
        case '1m-3m':
        $where  .= " AND price >= 1000000 AND price < 3000000";

        break;
        case '3m-5m':
        $where  .= " AND price >= 3000000 AND price < 5000000";

        break;
        case '5m-10m':
        $where  .= " AND price >= 5000000 AND price < 10000000";

        break;
        case '10m-100m':
        $where  .= " AND price >= 10000000 AND price < 100000000";

        break;
        case '100m-1b':
        $where  .= " AND price >= 100000000 AND price < 1000000000";

        break;
        case '1b-10b':
        $where  .= " AND price >= 1000000000 AND price < 10000000000";

        break;
        case 'more-10b':
        $where  .= " AND price >= 1000000000";

        break;

        default:
        $where  .= "";
        break;
    }
        //dd($param);

    $data['search_category'] = DB::select(DB::raw("SELECT * FROM bds_tindang $where"),$param);

        //dd($data);
        // Đổ data ra 1 route mới tên timkiem giống page danh mục tin đăng
    return view('frontend.pages.timkiem_category',$data); 
}
    // public function GetCanho()
    // {
    //     return view('frontend.quanlycanho');
    // }
public function CreateCanho()
{
 $data['dm_cate'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->dmcanho->listTempByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['dm_cate'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->dmcanho->listTempByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['dm_cate'], $option_child_item);
            }
        }
        return view('frontend.create_canho',$data);
}

public function StoreCanho(Request $request)
{ 
         // dd($request->all());   
        $img1="default.jpg";
        $fileNameTotal = "";
        if ($request->hasFile('images')) {
            $rules = array("jpg","jpeg","png","jpeg","gif","svg");
            foreach ($request->file('images') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $arr = [];
                    $fileName = time().'_'.$file->getClientOriginalName();
                    $uploadPath = 'public/uploads/postings';
                    $file->move($uploadPath, $fileName);
                    $fileNameTotal .= $fileName."||";
                }
            }
            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact;
            $googlemap = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->googlemap );
            $data = array(
                'title'                 => $request->title,
                'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                'dientich'              => $request->dientich,
                'priceCDT'              => $request->priceCDT,
                'priceChenh'            => $request->priceChenh,
                'khuvuc'                => $request->khuvuc,                           
                'intro'                 => $request->intro,
                'content'               => $request->content,
                'googlemap'             => $googlemap,                          
                'contact'               => $contact,
                'maCan'                 => $request->maCan,
                'maToa'                 => $request->maToa,
                'category_id'           => $request->category_id,
                'member_id'             => Auth::user()->id,
                'images'                => $fileNameTotal,                
                'link'                  => $request->link,
                'huong_view'            => $request->huong_view,
                'huong_bancong'         => $request->huong_bancong,                            
                'hot'                   => $request->hot,
                'created_at'            => new DateTime
            );

            $this->canho->addCanHo($data);

            return redirect()->back()->with('success',"message.store_required");
        } 
        else {

            $img1="default.jpg";
            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact;
            $googlemap = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->googlemap );
            $data = array(
                'title'                 => $request->title,
                'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                'dientich'              => $request->dientich,
                'priceCDT'              => $request->priceCDT,
                'priceChenh'            => $request->priceChenh,
                'khuvuc'                => $request->khuvuc,                           
                'intro'                 => $request->intro,
                'content'               => $request->content,
                'googlemap'             => $googlemap,                          
                'contact'               => $contact,
                'maCan'                 => $request->maCan,
                'maToa'                 => $request->maToa,
                'category_id'           => $request->category_id,
                'member_id'             => Auth::user()->id,               
                'images'                => $fileNameTotal,                           
                'link'                  => $request->link,
                'huong_view'            => $request->huong_view,
                'huong_bancong'         => $request->huong_bancong,       
                'hot'                   => $request->hot,
                'created_at'            => new DateTime
            );  
            $this->canho->addCanHo($data);

            return redirect()->back()->with('success',"message.store_required");
        }

}
    public function EditCanHo($alias)
    {
        $data['canho'] = $this->canho->editCanHoByAlias($alias);
        // dd($data['canho']);

        $data['dm_cate'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->dmcanho->listTempByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['dm_cate'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->dmcanho->listTempByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['dm_cate'], $option_child_item);
            }
        }
        return view('frontend.edit_canho',$data);
    }

    public function ChangeImages($alias)
    {
        $item = $this->canho->editCanHoByAlias($alias);
        $splittedstring=explode("||",$item->images);
        foreach ($splittedstring as $key) {
            $filename = 'public/uploads/postings/'.$key;
            if (!is_null($splittedstring)) {
             if(\File::exists($filename)){
                \File::delete($filename);
            }  
        }
    }

}
public function UpdateCanHo(CanhoStore $request,$alias)
{
    $item = $this->canho->editCanHoByAlias($alias);
    $img1="default.jpg";
    $fileNameTotal = "";

    if ($request->hasFile('images')== true) {
        $rules = array("jpg","jpeg","png","jpeg","gif","svg");
        foreach ($request->file('images') as $file) {
            $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            if (in_array($fileExtension,$rules)) {
                $arr = [];
                $fileName = time().'_'.$file->getClientOriginalName();
                $uploadPath = 'public/uploads/postings';
                $file->move($uploadPath, $fileName);
                $this->ChangeImages($alias);
                $fileNameTotal .= $fileName."||";
            }
        }
        $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
        $googlemap = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->googlemap );
        $data = array(
            'title'                 => $request->title,
            'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
            'dientich'              => $request->dientich,
            'priceCDT'              => $request->priceCDT,
            'priceChenh'            => $request->priceChenh,
            'khuvuc'                => $request->khuvuc,                           
            'intro'                 => $request->intro,
            'content'               => $request->content,
            'googlemap'             => $googlemap,                          
            'contact'               => $contact,
            'maCan'                 => $request->maCan,
            'maToa'                 => $request->maToa,
            'category_id'           => $request->category_id,
            'member_id'             => Auth::user()->id,
            'images'                => $fileNameTotal,
            'link'                  => $request->link,
            'huong_view'            => $request->huong_view,
            'huong_bancong'         => $request->huong_bancong,                            
            'hot'                   => $request->hot,
            'created_at'            => new DateTime
        );
        $this->canho->updateCanHo($data,$item->id);
        return redirect()->back()->with('success',"message.update_required"); 
    } else {
     $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact ;
     $googlemap = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->googlemap );
     $data = array(
        'title'                 => $request->title,
        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
        'dientich'              => $request->dientich,
        'priceCDT'              => $request->priceCDT,
        'priceChenh'            => $request->priceChenh,
        'khuvuc'                => $request->khuvuc,                           
        'intro'                 => $request->intro,
        'content'               => $request->content,
        'googlemap'             => $googlemap,                          
        'contact'               => $contact,
        'maCan'                 => $request->maCan,
        'maToa'                 => $request->maToa,
        'category_id'           => $request->category_id,
        'member_id'             => Auth::user()->id,
        'images'                => $request->imageshidden,
        'link'                  => $request->link,
        'huong_view'            => $request->huong_view,
        'huong_bancong'         => $request->huong_bancong,                            
        'hot'                   => $request->hot,
        'created_at'            => new DateTime
    );
     $this->canho->updateCanHo($data,$item->id);
     return redirect()->back()->with('success',"message.update_required");
 }
 return redirect()->back()->with('error', 'Cập nhật không thành công!');
}

public function destroyCanho($id)
{  
    $del = DB::table('bds_canho')->whereId($id)->delete();
    if ($del) {
       return back()->with("success", "message.delete_required");
   }
   return back()->with("error", "Xóa dữ liệu không thành công");
}


public function postSearch(Request $request)
{
    $project = $request->dmcanho;
    $area = $request->khuvuc;
    $dientich = $request->dientichhidden;
    $price = $request->priceCDThidden;
    $view = $request->huong_view;

    $where = "";
    $param = [];

    if($project != 0) {
        $where  .= " category_id = $project AND";
        $param["project"] = $project;
    }

    if(strlen($area) > 0) {
        $where  .= " khuvuc = '$area' AND";
        $param["area"] = $area;
    }

    if(strlen($dientich) > 0) {
        $arr_tmp = explode("-",$dientich);
        $dtFrom = $arr_tmp[0];
        $dtTo = $arr_tmp[1];
        $where  .= " (dientich BETWEEN $dtFrom AND $dtTo) AND";
        $param["dtFrom"] = $dtFrom;
        $param["dtTo"] = $dtTo;
    }

    if(strlen($price) > 0) {
        $arr_tmp = explode("-",$price);
        $priceFrom = $arr_tmp[0] * 1000000;
        $priceTo = $arr_tmp[1] * 1000000;
        $where  .= " (priceCDT BETWEEN $priceFrom AND $priceTo) AND";
        $param["priceFrom"] = $priceFrom;
        $param["priceTo"] = $priceTo;
    }

    if($view != 0) {
        $where  .= " huong_view = $view ";
        $param["view"] = $view;
    }


    $arr_where = explode(" ",$where);
        ///dd($arr_where);
    if(count($arr_where) - 1 > 0) {
        $where = "WHERE ";
    }

    if($arr_where[count($arr_where) - 1] == "AND" ) {
        $arr_where[count($arr_where) - 1] = "";
    }

    $where .= implode(" ", $arr_where);

    $data['search_can_ho'] = DB::select(DB::raw("SELECT * FROM bds_canho $where"),$param);

        //$data['can_ho_tuong_tu'] = DB::select(DB::raw("SELECT category_id, khuvuc FROM bds_canho $where ORDER BY position DESC "),$param);


    
    $data['canhotuongtu'] = DB::select(DB::raw("SELECT * FROM bds_canho $where ORDER BY position DESC, id DESC LIMIT 0, 4 "));

    //dd($data['canhotuongtu']);
    return view('frontend.pages.search_canho',$data); 

}



}
