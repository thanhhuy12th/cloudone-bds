<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TinDang;
use DB;

class ApiController extends Controller
{
    public function get_province($id = null) {
        $data = array();

        if($id == null) {
            $data = DB::table('province')->orderByRaw('position ASC, name ASC')->get()->toArray();
        }
        else {
            $data = DB::table('province')->where('provinceid', '=', $id)->orderByRaw('position ASC, name ASC')->get()->toArray();
        }

        echo json_encode($data);
    }

    public function get_district($id = null) {
        $data = array();

        if($id == null) {
            $data = DB::table('district')->orderBy('name', 'ASC')->get()->toArray();
        }
        else {
            $data = DB::table('district')->where('provinceid', '=', (int)$id)->orderBy('name', 'ASC')->get()->toArray();
        }

        echo json_encode($data);
    }
    // Status tin đăng
    public function change_status_tindang(Request $request)
    {
        $id = $request->id;
        $status = $request->status =='true' ? 1 : 0;
        //$msg = $request->status;
        $data = DB::table('bds_tindang')->where('id',$id)->limit(1);
        if(is_null($data)) $msg = 'ID không hợp lệ hoặc không tồn tại trong cơ sở dữ liệu';
        else {
            DB::table('bds_tindang')->where('id',$id)->update(['status'=>$status]);
            $msg = 'Cập nhật trạng thái thành công!!!';
        }
        return response()->json(array('msg'=>$msg), 200);
    }
    // Duyệt bài tin đăng
    public function duyetbai_tindang(Request $request)
    {
        $id = $request->id;
        $duyetbai = $request->duyetbai =='true' ? 1 : 0;
        //$msg = $request->id;
        $data = DB::table('bds_tindang')->where('id',$id)->limit(1);
        if(is_null($data)) $msg = 'ID không hợp lệ hoặc không tồn tại trong cơ sở dữ liệu';
        else {
            DB::table('bds_tindang')->where('id',$id)->update(['duyetbai'=>$duyetbai]);
            $msg = 'Cập nhật trạng thái thành công!!!';
        }
        return response()->json(array('msg'=>$msg), 200);
    }
    // Status dự án
    public function change_status_duan(Request $request)
    {
        $id = $request->id;
        $status = $request->status =='true' ? 1 : 0;
        //$msg = $request->status;
        $data = DB::table('bds_duan')->where('id',$id)->limit(1);
        if(is_null($data)) $msg = 'ID không hợp lệ hoặc không tồn tại trong cơ sở dữ liệu';
        else {
            DB::table('bds_duan')->where('id',$id)->update(['status'=>$status]);
            $msg = 'Cập nhật trạng thái thành công!!!';
        }
        return response()->json(array('msg'=>$msg), 200);
    }
    // Status Video
    public function change_status_video(Request $request)
    {
        $id = $request->id;
        $status = $request->status =='true' ? 1 : 0;
        //$msg = $request->status;
        $data = DB::table('bds_video')->where('id',$id)->limit(1);
        if(is_null($data)) $msg = 'ID không hợp lệ hoặc không tồn tại trong cơ sở dữ liệu';
        else {
            DB::table('bds_video')->where('id',$id)->update(['status'=>$status]);
            $msg = 'Cập nhật trạng thái thành công!!!';
        }
        return response()->json(array('msg'=>$msg), 200);
    }

     // Duyệt bài căn hộ
    public function duyetbai_canho(Request $request)
    {
        $id = $request->id;
        $status = $request->status =='true' ? 1 : 0;
        //$msg = $request->id;
        $data = DB::table('bds_canho')->where('id',$id)->limit(1);
        if(is_null($data)) $msg = 'ID không hợp lệ hoặc không tồn tại trong cơ sở dữ liệu';
        else {
            DB::table('bds_canho')->where('id',$id)->update(['status'=>$status]);
            $msg = 'Cập nhật trạng thái thành công!!!';
        }
        return response()->json(array('msg'=>$msg), 200);
    }

     // căn hộ HOT 
    public function change_status_canho(Request $request)
    {
        $id = $request->id;
        $hot = $request->hot =='true' ? 1 : 0;
      
        $data = DB::table('bds_canho')->where('id',$id)->limit(1);
        if(is_null($data)) $msg = 'ID không hợp lệ hoặc không tồn tại trong cơ sở dữ liệu';
        else {
            DB::table('bds_canho')->where('id',$id)->update(['hot'=>$hot]);
            $msg = 'Cập nhật trạng thái thành công!!!';
        }
        return response()->json(array('msg'=>$msg), 200);
    }

   public function change_lienhe()
   {
        $data = DB::table('bds_lienhe')->where('seen',1)->update(['seen'=>0]);
   }








}
