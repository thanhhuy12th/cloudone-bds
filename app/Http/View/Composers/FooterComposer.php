<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use DB;

class FooterComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
     public function compose(View $view)
    {
        $hotline = DB::table('bds_config')->whereId(16)->first();
        $view->with('hotline', $hotline);

        $address = DB::table('bds_config')->whereId(22)->first();
        $view->with('address', $address);

        $email = DB::table('bds_config')->whereId(17)->first();
        $view->with('email', $email);

        $title = DB::table('bds_config')->whereId(20)->first();
        $view->with('title', $title);

        $baseUrl = DB::table('bds_config')->whereId(21)->first();
        $view->with('baseUrl', $baseUrl);

        $ggmap = DB::table('bds_config')->whereId(14)->first();
        $view->with('ggmap', $ggmap);

        $seo = DB::table('bds_config')->whereId(18)->first();
        $view->with('seo',$seo);

        $description = DB::table('bds_config')->whereId(19)->first();
        $view->with('description', $description);


        // liên hệ công ty
       $hotline1 = DB::table('bds_config')->whereId(9)->first();
       $view->with('hotline1',$hotline1);

       $phonename1 = DB::table('bds_config')->whereId(9)->first();
       $view->with('phonename1',$phonename1);

       $hotline2 = DB::table('bds_config')->whereId(10)->first();
       $view->with('hotline2',$hotline2);

       $phonename2 = DB::table('bds_config')->whereId(10)->first();
       $view->with('phonename2',$phonename2);


      $hotnews = DB::table('bds_tintuc')->orderBy('id', 'DESC')->limit(5)->get()->toArray();
       $view->with('hotnews',$hotnews);
        
    }

}