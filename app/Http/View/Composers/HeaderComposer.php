<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use DB;

class HeaderComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
     public function compose(View $view)
    {
        $count_lienhe = DB::table('bds_lienhe')->where('seen',1)->count();
        $view->with('count_lienhe', $count_lienhe);

        $get_count_lienhe = DB::table('bds_lienhe')->where('seen',0)->orderBy('id','DESC')->take(5)->get()->toArray();
        $view->with('get_count_lienhe',$get_count_lienhe);

        $logo = DB::table('bds_config')->whereId(5)->first();
        $view->with('logo',$logo);
    }
}