<?php

    function getCan_ho($alias)
    {
       
        $dataTemp = DB::table('bds_dmcanho')->where('alias',$alias)->take(20)->get()->toArray();
        
        $condition  = "";
        $counter    = 0;
        foreach ($dataTemp as $key) {
             if (++$counter == count($dataTemp)) {
                $condition .= "category_id=".$key->id;
             }else{
                $condition .= "category_id=".$key->id. " OR " ;
             }
         }
         $select = "select * from bds_canho where $condition ORDER BY position ASC, created_at DESC";

         $can_ho = DB::select($select);
        
           return $can_ho;
       
    }


    function getCan_ho_limit($alias,$start,$row_per_page)
    {
        $dataTemp = DB::table('bds_dmcanho')->where('alias',$alias)->take(24)->get()->toArray();
        
        $condition  = "";
        $counter    = 0;
        foreach ($dataTemp as $key) {
             if (++$counter == count($dataTemp)) {
                $condition .= "category_id=".$key->id;
             }else{
                $condition .= "category_id=".$key->id. " OR " ;
             }
         }
         $select = "select * from bds_canho where $condition ORDER BY position ASC, created_at DESC limit $start,$row_per_page";
         
         $can_ho = DB::select($select);
         
        return $can_ho;

    }

    function get_category_parent($alias)
    {
        $getID = DB::table('bds_dmtd')->where('alias',$alias)->first();
        
        $dataParent = DB::table('bds_dmtd')->where('id',$getID->id)->orWhere('parent_id',$getID->id)->get()->toArray();

         $condition  = "";
         $counter    = 0;

         foreach ($dataParent as $key) {
           
             if (++$counter == count($dataParent)) {
                $condition .= "category_id=".$key->id;
             }else{
                $condition .= "category_id=".$key->id. " OR " ;
             }
         }
        
         $select = "select * from bds_tindang where $condition ORDER BY position ASC , created_at DESC";
         $product_category_parent = DB::select($select);
        
        return $product_category_parent;
    }

    function get_category_parent_limit($alias,$start,$row_per_page)
    {
         $getID = DB::table('bds_dmtd')->where('alias',$alias)->first();
         $dataParent = DB::table('bds_dmtd')->where('id',$getID->id)->orWhere('parent_id',$getID->id)->get()->toArray();

         $condition  = "";
         $counter    = 0;

         foreach ($dataParent as $key) {
             if (++$counter == count($dataParent)) {
                $condition .= "category_id=".$key->id;
             }else{
                $condition .= "category_id=".$key->id. " OR " ;
             }
         }
         $select = "select * from bds_tindang where $condition ORDER BY position ASC, created_at DESC limit $start,$row_per_page";
         $product_category_parent = DB::select($select);
        
        return $product_category_parent;
    }



    function limit_time_canho()
    {
         
        $ngayhientai = \Carbon\Carbon::now();
       
        $data = DB::table('bds_canho')->get()->toArray();
        foreach ($data as $key) {
            $key = \Carbon\Carbon::parse($key->exp_time);
            $limit_time = ($ngayhientai->day - $key->day) + ($ngayhientai->month - $key->month) + ($ngayhientai->year - $key->year);
            if ($limit_time ==0) {
                DB::table('bds_canho')->where('id',$key->id)->update('hot',0);
            }


            // if ($limit_time <= $key) {
            //     DB::table('bds_canho')->where('id',$key->id)->update('position',1);
            // }
            // else {
            //     DB::table('bds_canho')->where('id',$key->id)->update('position',10);
            // }

             
        }

        return 1;
    }

  function limit_time_tindang()
    {

        $ngayhientai = \Carbon\Carbon::now();
       
        $data = DB::table('bds_tindang')->get()->toArray();
        foreach ($data as $key) {
            $key = \Carbon\Carbon::parse($key->exp_time);
            $limit_time = ($ngayhientai->day - $key->day) + ($ngayhientai->month - $key->month) + ($ngayhientai->year - $key->year);
            if ($limit_time ==0) {
                DB::table('bds_tindang')->where('id',$key->id)->update('status',0);
            }


            // if ($limit_time <= $key) {
            //     DB::table('bds_tindang')->where('id',$key->id)->update('position',1);
            // }
            // else {
            //     DB::table('bds_tindang')->where('id',$key->id)->update('position',10);
            // }
           
        }

        return 1;
    }
