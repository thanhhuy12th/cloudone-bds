<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use DB;

class DashboardComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
     public function compose(View $view)
    {
        $canho = DB::table('bds_canho')->where('hot',1)->where('status',1)->orderBy('id', 'DESC')->limit(5)->get()->toArray();
        $view->with('canho', $canho);

        $news = DB::table('bds_tintuc')->orderBy('id', 'DESC')->limit(5)->get()->toArray();
        $view->with('news', $news);

        $member = DB::table('bds_member')->orderBy('id', 'DESC')->limit(5)->get()->toArray();
        $view->with('member', $member);

        $postings = DB::table('bds_tindang')->where('status',1)->orderBy('id', 'DESC')->limit(5)->get()->toArray();
        $view->with('postings', $postings);


        $count_member = DB::table('bds_member')->count();
        $view->with('count_member', $count_member);


        $count_canho = DB::table('bds_canho')->count();
        $view->with('count_canho', $count_canho);

        $count_tindang = DB::table('bds_tindang')->count();
        $view->with('count_tindang', $count_tindang);

    }
}