<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\TinDang;
use App\Models\DMTD;
use DB;

class HomeComposer
{
    private $tindang;
    private $danhMucTinDang;
    public function __construct(TinDang $tindang, DMTD $danhMucTinDang)
    {
        $this->postings = $tindang;
        $this->danhMucTinDang = $danhMucTinDang;
    }
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
     public function compose(View $view)
    {
        // du an noi bat
        $duan = DB::table('bds_duan')->orderBy('id', 'DESC')->limit(5)->get()->toArray();
        $view->with('duan', $duan);


        $duannoibat = DB::table('bds_duan')->orderBy('id', 'DESC')->take(4)->get();
        $view->with('duannoibat', $duannoibat);

        // tin tuc
        $news = DB::table('bds_tintuc')->orderBy('id', 'DESC')->take(4)->get();
        $view->with('news', $news);


        $danhmuc = [];
        // Xử lý option danh mục tin đăng
        $option_parent = $this->danhMucTinDang->listCateByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($danhmuc, $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->danhMucTinDang->listCateByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($danhmuc, $option_child_item);
            }
        }
        $view->with('danhmuc',$danhmuc);



        // nhà đất nổi bật
        $postings = DB::table('bds_tindang')->orderBy('id', 'DESC')->take(6)->get();
        $view->with('postings', $postings);

        // $dt_gia = DB::table('bds_tindang')->select('id','dientich','price')->get();
        // $view->with('dt_gia', $dt_gia);

         //$province = DB::table('province')->orderByRaw('position ASC, name ASC')->get();
         //$view->with('province', $province);

         //$district = DB::table('district')->select('districtid','name','provinceid')->get();
         //$view->with('district', $district);

        
        

    }
}