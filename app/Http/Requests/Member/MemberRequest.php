<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'   => 'required|unique:bds_member,fullname',
            'username'   => 'required|unique:bds_member,username',
            'email'      => 'required|unique:bds_member,email',
            'phone'      => 'required|min:11|numeric',
            'address'    => 'required',
           
        ];
    }
    public function messages()
    {
        return [
            'fullname.required'         => trans('message.fullname_required'),
            'fullname.unique'           => trans('message.fullname_unique'),
            'username.required'         => trans('message.username_required'),
            'username.unique'           => trans('message.username_unique'),
            'email.required'            => trans('message.email_required'),
            'email.unique'              => trans('message.email_unique'),
            'phone.required'            => trans('message.phone_required'),
            'phone.min'                 => trans('message.phone_min'),
            'phone.numeric'             => trans('message.phone_numeric'),
            'address.required'          => trans('message.address_required'),
        ];
    }
}
