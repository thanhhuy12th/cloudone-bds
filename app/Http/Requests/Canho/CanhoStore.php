<?php

namespace App\Http\Requests\Canho;

use Illuminate\Foundation\Http\FormRequest;

class CanhoStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required',
            'khuvuc'    => 'required',
            'priceCDT'  => 'numeric:bds_canho,priceCDT',
            'priceChenh'  => 'numeric:bds_canho,priceChenh',
            'intro'     => 'required',
            'content'   => 'required',
            'position'   => 'required',


        ];
    }
    public function messages()
    {
        return [
            'title.required'        => trans('message.title_required'),
            'khuvuc.required'       => trans('message.khuvuc_required'),
            'priceCDT.numeric'      => trans('message.priceCDT_numeric'),
            'priceChenh.numeric'      => trans('message.priceChenh_numeric'),
            'intro.required'        => trans('message.intro_required'),        
            'content.required'      => trans('message.content_required'),
            'position.required'      => trans('message.position_required'),
        ];
    }
}
