<?php

namespace App\Http\Requests\TinTuc;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|unique:bds_tintuc,title',
            'intro'     => 'required',
            'content'   => 'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required'        => trans('message.title_required'),
            'title.unique'          => trans('message.title_unique'),
            'intro.required'        => trans('message.intro_required'),
            'content.required'      => trans('message.content_required'),
        ];
    }
}
