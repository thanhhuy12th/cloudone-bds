<?php

namespace App\Http\Requests\DuAn;

use Illuminate\Foundation\Http\FormRequest;

class DuAnResquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:255|unique:bds_duan,name',
            'title'     => 'required|unique:bds_duan,title',
            'intro'     => 'required',
            'status'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => trans('message.name_required'),
            'name.max'              => trans('message.name_max'),
            'name.unique'           => trans('message.name_unique'),
            'title.required'        => trans('message.title_required'),
            'title.unique'          => trans('message.title_unique'),
            'intro.required'        => trans('message.intro_required'),
            'status.required'       => trans('message.status_required'),
        ];
    }
}
