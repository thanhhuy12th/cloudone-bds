<?php

namespace App\Http\Requests\ChangePass;

use Illuminate\Foundation\Http\FormRequest;

class ChangePassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current-password' => 'required',
            'new-password' => 'required|min:8|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'current-password.required'     => trans('message.current-password_required'),
            'new-password.required'         => trans('message.new-password_required'),
            'new-password.min'              => trans('message.new-password_min'),
            'new-password.confirmed'        => trans('message.new-password_confirmed'),
        ];
    }
}
