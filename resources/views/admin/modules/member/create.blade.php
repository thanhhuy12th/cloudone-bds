@extends('admin.master')
@section('title','Thêm Member')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('member.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.AddUser Member')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Fullname Member')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="fullname" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Username Member')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="username" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Email field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Email Member')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="email" name="email" class="form-control" id="email" />
                        </div>
                    </div>
                    <!-- /email field -->
 <!-- Password field -->
                    <div class="form-group row" >
                        <label class="col-form-label col-lg-3">{{trans('template.Password')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="password" name="password" id="password" class="form-control" />
                        </div>
                    </div>
                    <!-- /password field -->

                    <!-- Repeat password -->
                    <div class="form-group row" >
                       <label class="col-form-label col-lg-3">{{trans('template.Repassword')}}<span class="text-danger">*</span></label>
                       <div class="col-lg-9">
                           <input type="password" name="password_confirmation" class="form-control" />
                       </div>
                   </div>
                    <!-- /repeat password -->

                    <!-- phone field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Phone Member')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="phone" class="form-control"  />
                        </div>
                    </div>
                    <!-- /phone field -->

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Address')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="address" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                   
                      <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Image Member')}}</label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-input-styled" />
                        </div>
                    </div>
                    <!-- /styled file uploader -->



                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection