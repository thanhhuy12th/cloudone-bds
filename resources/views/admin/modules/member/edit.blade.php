@extends('admin.master')
@section('title','Edit Member')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('member.update',['id' => $member->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Edit Member')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Fullname Member')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="fullname" class="form-control" value="{{$member->fullname}}" readonly="readonly" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Username Member')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="username" class="form-control" value="{{$member
                                ->username}}" readonly="readonly" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Email field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Email Member')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="email" name="email" class="form-control" id="email" value="{{$member->email}}" readonly="readonly" />
                        </div>
                    </div>
                    <!-- /email field -->

                   
                    <!-- Repeat password -->
                   {{--  <div class="form-group row" hidden>
                       <label class="col-form-label col-lg-3">{{trans('template.Repassword')}}<span class="text-danger">*</span></label>
                       <div class="col-lg-9">
                           <input type="password" name="password_confirmation" class="form-control" />
                       </div>
                   </div> --}}
                    <!-- /repeat password -->

                    <!-- phone field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Phone Member')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="phone" class="form-control" value="{{$member->phone}}" />
                        </div>
                    </div>
                    <!-- /phone field -->

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Address')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="address" class="form-control" value="{{$member->address}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                      <!-- Password field -->
                    <div class="form-group row" >
                        <label class="col-form-label col-lg-3">{{trans('template.Password')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="password" name="password" id="password" class="form-control" />
                        </div>
                    </div>
                    <!-- /password field -->

                    
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Status</label>
                        <div class="col-lg-3">
                            <select name="reg_status" class="form-control col-lg-9">
                                <option value="0" {{$member->reg_status == 0 ? 'selected' : ''}}>InActive</option>
                                <option value="1" {{$member->reg_status == 1 ? 'selected' : ''}}>Active</option>
                            </select>
                        </div>
                    </div>

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3"></label>
                        <div class="col-lg-9">
                            <img src="{{ asset('uploads/avatar/'.$member ->image)}}" width="100px">
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                      <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Image Member')}}</label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-input-styled" />
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection