@extends('admin.master')
@section('title','Chỉnh Sửa Dự Án')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('tin-tuc.update',['id' => $duan->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Edit Project')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Name Project')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="name" class="form-control" value="{{old('name',$duan->name)}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row" hidden="">
                        <label class="col-form-label col-lg-3">{{trans('template.Alias Project')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="alias" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Title Project')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="title" class="form-control" value="{{old('title',$duan->title)}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3"></label>
                        <div class="col-lg-9">
                           <img src="{{asset('uploads/postings/'.$duan->image)}}" width="50%">
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Image Project')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-input-styled" >
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                     <!-- Basic status input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Status')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                             <select name="status" class="form-control" />
                                <option value="1" {{($duan->status ==1)? 'selected':''}}>Nổi bật</option>
                                <option value="0" {{($duan->status ==0)? 'selected':''}}>Không nổi bật</option>
                           </select>
                            {{-- <input type="text" name="status" class="form-control" value="{{old('status',$post->status)}}" /> --}}
                        </div>
                    </div>
                    <!-- /basic status input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12">{{trans('template.Content')}}</label>
                    </div>
                    <!-- /basic text input -->
                    <hr>
                    
                     <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.Intro Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.Acreage Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab3" class="nav-link" data-toggle="tab">{{trans('template.Price Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab4" class="nav-link" data-toggle="tab">{{trans('template.Overview')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab5" class="nav-link" data-toggle="tab">{{trans('template.Location Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab6" class="nav-link" data-toggle="tab">{{trans('template.Flat')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab7" class="nav-link" data-toggle="tab">{{trans('template.Image Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab8" class="nav-link" data-toggle="tab">{{trans('template.Contact')}}</a></li>
                        </ul>

                  <div class="tab-content">
                        <div class="tab-pane fade active show" id="basic-tab1">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Intro')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" name="intro" class="form-control col-md-12 col-xs-12" >{{old('intro',$duan->intro)}}</textarea>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab2">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Acreage Content')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <textarea rows="6" id="dt" name="dt" class="form-control col-md-12 col-xs-12">{{old('dt',$duan->dt)}}</textarea>
                            </div>
                        </div>

                         <div class="tab-pane fade" id="basic-tab3">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Price Content')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="giaban" name="giaban" class="form-control col-md-12 col-xs-12">{{old('giaban',$duan->giaban)}}</textarea>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab4">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Overview Content')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="editor1" name="tongquan" class="form-control col-md-12 col-xs-12">{{old('tongquan',$duan->tongquan)}}</textarea>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab5">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Location Post')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <textarea rows="6" id="vitridacdia" name="vitridacdia" class="form-control col-md-12 col-xs-12">{{old('vitridacdia',$duan->vitridacdia)}}</textarea>
                            </div>
                        </div>

                         <div class="tab-pane fade" id="basic-tab6">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Flat')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="matbang" name="matbang" class="form-control col-md-12 col-xs-12">{{old('matbang',$duan->matbang)}}</textarea>
                            </div>
                        </div>

                         <div class="tab-pane fade" id="basic-tab7">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Image Post')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <textarea rows="6" id="hinh" name="hinh" class="form-control col-md-12 col-xs-12">{{old('hinh',$duan->hinh)}}</textarea>
                            </div>
                        </div>
            
                        <div class="tab-pane fade" id="basic-tab8">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Contact')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="lienhe" name="contact" class="form-control col-md-12 col-xs-12">{{old('contact',$duan->contact)}}</textarea>
                            </div>
                        </div>

                    </div>
                </div>
                   
                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection