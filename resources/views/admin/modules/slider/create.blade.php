@extends('admin.master')
@section('title','Thêm Slider')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('slider.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Add Slider')}}</legend>


                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Chọn danh mục<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                           <select name="dmcanho_id" class="form-control">   
                                <option value="0">Trang Chủ</option>                                 
                               @for($i = 0; $i < count($cate_option); $i++)
                                <option value="{!!$cate_option[$i]['id']!!}" {{ (old('dmcanho_id') == $cate_option[$i]['name'])? 'selected': ''}}>{!!$cate_option[$i]['name']!!}</option>
                               @endfor
                           </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Name Slider')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="name" value="{{old('name')}}" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Intro Slider')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" name="intro" class="form-control col-md-12 col-xs-12">{{old('intro')}}</textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Image Slider')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-input-styled" >
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection