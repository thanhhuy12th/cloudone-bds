@extends('admin.master') 
@section('title','Danh Sách Slider') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.List Slider')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('slider.create')}}">
                <button type="button" class="btn btn-primary">Thêm Slider</button>
            </a>
        </div>
        <form action="{{route('slider.destroy',['action' => 'delete'])}}" method="POST">
            @method('DELETE')
            @csrf

        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
                    </th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Mô Tả</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($slider as $item)
                <tr>
                    <td style="width: 10%">
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td style="width: 20%">{{$item->name}}</td>
                    <td  style="width: 20%">
                        <img src="{{asset('uploads/slider/'.$item ->image)}}" width="100%">
                    </td>
                    <td style="width: 20%">{{$item->intro}}</td>
                    <td style="width: 20%">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center" style="width: 10%">
                        <a href="{{ route('slider.edit',['slider' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa Slider"><i class="icon-pencil7"></i></a> &nbsp;
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.Delete')}}</button><br/><br>              
        </form>  
    </div>
    <!-- /page length options -->
</div>
@endsection