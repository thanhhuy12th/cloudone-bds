@extends('admin.master')
@section('title','Chỉnh Sửa Admin')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('admin.update',['id' => $admin->id])}}" method="POST" enctype="multipart/form-data">
               
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Edit Admin')}}</legend>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Username Admin')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="username" class="form-control form-control-maxlength" value="{{old('username',$admin->username)}}" maxlength="25" readonly="readonly" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Fullname Admin')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="fullname" class="form-control form-control-maxlength" value="{{old('fullname',$admin->fullname)}}" maxlength="30" readonly="readonly" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Email field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Email Admin')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="email" name="email" class="form-control" id="email" value="{{old('email',$admin->email)}}" readonly="readonly" />
                        </div>
                    </div>
                    <!-- /email field -->

					<!-- Number -->
                    <div class="form-group row">
						<label class="col-form-label col-lg-3">{{trans('template.Phone Admin')}}<span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="text" name="phone" class="form-control" value="{{old('phone',$admin->phone)}}" />
						</div>
					</div>
					<!-- /number -->

                     <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3"></label>
                        <div class="col-lg-9">
                            <img src="{{ asset('uploads/avatar/'.$admin->image)}}" width="100px">
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Image Admin')}}</label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-input-styled" />
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Basic radio group -->
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">{{trans('template.Level Admin')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
			               <select name="role" class="form-control" />
			                	<option value="1" {{($admin->role ==1)? 'selected':''}}>Admin</option>
			                	<option value="2" {{($admin->role ==2)? 'selected':''}}>Nhân viên</option>
			                	
			               </select>
			             </div>
                    </div>
                    <!-- /basic radio group -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection