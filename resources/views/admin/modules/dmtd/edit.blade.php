@extends('admin.master')
@section('title','Sửa Danh Mục Tin Đăng')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-validate-jquery" action="{{route('dmtd.update',['id'=>$category->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Edit Category')}}</legend>

                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Parent Category')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                           <select name="parent_id" class="form-control">
                               <option value="0">ROOT</option>
                               @foreach($menus as $menu)
                               <option value="{{$menu->id}}" {{ (old('parent_id',$menu->parent_id) == $menu->id)? 'selected': ''}}>{{$menu->name}}</option>
                               @endforeach
                           </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Name Category')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="name" class="form-control" value="{{old('name',$category->name)}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row" hidden="">
                        <label class="col-form-label col-lg-3">{{trans('template.Alias Category')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="alias" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Location Category')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="location" class="form-control" value="{{old('location',$category->location)}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection