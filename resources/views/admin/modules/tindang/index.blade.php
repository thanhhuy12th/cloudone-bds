@extends('admin.master') 
@section('title','Danh Sách Tin Đăng') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.List Postings')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('tindang.create')}}">
                <button type="button" class="btn btn-primary">Thêm Tin Đăng</button>
            </a>
        </div>
        <form id="listForm" action="{{route('tindang.destroy',['action' => 'delete'])}}" method="POST">
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                    </th>
                    <th>ID</th>
                    <th>Tiêu đề</th>
                    <th>Diện tích(m²)</th>
                    <th>Giá(VND)</th>
                    <th>Nổi bật</th>
                    <th>Duyệt bài</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($postings as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="checked[]" value="{{$item->id}}">
                    </td>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->dientichmatbang .'m²' }}</td>
                    <td>{{ number_format( $item->price,0,",",".")  }}</td>
                    <td>
                        <div class="form-check form-check form-check-switchery form-check-switchery-sm">
                            <label class="form-check-label">
                                <input type="checkbox" data-id="{{ $item->id }}" name="status" class="form-input-switchery" {{ $item->status ? 'checked' : '' }}  >
                            </label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-switchery">
                            <label class="form-check-label">
                                <input type="checkbox" data-id="{{ $item->id }}" name="duyetbai" class="form-check-input-switchery" {{ $item->duyetbai ? 'checked' : '' }} >
                            </label>
                        </div>
                    </td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center"> 
                        <a href="{{ route('tindang.edit',['tindang' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa tin đăng"><i class="icon-pencil7"></i></a> &nbsp;                      
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.Delete')}}</button><br/><br>              
        </form>
    </div>
    <!-- /page length options -->
</div>
<script type="text/javascript">
    function checkallClick(o) {
            var form = document.listForm;
            for (var i = 0; i < form.elements.length; i++) {
                if (form.elements[i].type == "checkbox" && form.elements[i].name!="checkAll") {
                    form.elements[i].checked = document.listForm.checkAll.checked;
                }
            }
        }


$(document).ready(function () {
    $('.form-check-input-switchery').on('click',function () {
        var Id = $(this).data('id');
        var duyetbai = this.checked;
        $.ajax({
            type: "POST",
            url: "../api/duyet-bai",
            data: {
                id: Id,
                duyetbai: duyetbai
            },
            success: function (result) {  
                    console.log(result.msg);
            },
            error: function () {
                console.log('Lỗi rồi,debug đi nhé.');
            }
        })
    });
});
$(document).ready(function () {
    $('.form-input-switchery').on('click',function () {
        var Id = $(this).data('id');
        var status = this.checked;
        $.ajax({
            type: "POST",
            url: "../api/tin-dang",
            data: {
                id: Id,
                status: status
            },
            success: function (result) {  
                    console.log(result.msg);
            },
            error: function () {
                console.log('Bị hack rồi nha bạn :D');
            }
        })
    });
});

</script>
@endsection