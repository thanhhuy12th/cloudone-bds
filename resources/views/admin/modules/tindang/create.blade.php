@extends('admin.master')
@section('title','Thêm Tin Đăng')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger" id="flash-message">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-validate-jquery" action="{{route('tindang.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Postings')}}</legend>

                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.OptionPostings')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                           <select name="category_id" class="form-control">
    
                               @for($i = 0; $i < count($cate_option); $i++)
                                <option value="{!!$cate_option[$i]['id']!!}" {{ (old('category_id') == $cate_option[$i]['name'])? 'selected': ''}}>{!!$cate_option[$i]['name']!!}</option>
                               @endfor
                           </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic title input -->
                
                     <div class="form-group row">
                         <label class="col-form-label col-lg-2">Tiêu đề<span class="text-danger">*</span></label>
                         <div class="col-lg-9">
                            <input type="text" name="title" class="form-control form-control-maxlength" value="{{old('title')}}" maxlength="150" required="">
                        </div>
                    </div>
                    <!-- /basic title input -->

                    <!-- Basic alias input -->
                    <div class="form-group row" hidden="">
                        <label class="col-form-label col-lg-2">{{trans('template.Alias Post')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="alias" class="form-control"  />
                        </div>
                    </div>
                    <!-- /basic alias input -->

                     <!-- Basic alias input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.Location')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="vitri" class="form-control form-control-maxlength" value="{{old('vitri')}}" maxlength="50" />
                        </div>
                    </div>
                    <!-- /basic alias input -->
                     <div class="form-group row">
                        <label class="col-form-label col-lg-2">Tỉnh thành</label>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-md-6">                                    
                                   <div class="form-group">
                                        <select id="province" name="province" class="form-control">
                                             <option value="-1">-- Chọn Tỉnh Thành --</option>                                     
                                        </select>
                                    </div>                                   
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select id="district" name="district" class="form-control">
                                            <option value="-1">-- Chọn Quận Huyện --</option>
                                        </select>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                     <div class="form-group row">
                        <label class="col-form-label col-lg-2">Ưu tiên</label>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-md-6">                                    
                                   <div class="form-group">
                                        <select name="status" class="form-control">
                                            <option value="0">Không</option>                                           
                                            <option value="1">Có</option>                                            
                                        </select>
                                    </div>                                   
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select name="duyetbai" class="form-control">
                                            <option value="0">Chưa duyệt bài</option>
                                            <option value="1">Cho duyệt bài</option> 
                                        </select>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                   <div class="form-group row">
                        <label class="col-form-label col-lg-2">Thứ tự đăng</label>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-md-6">                                    
                                   <div class="form-group">
                                        <select name="position" class="form-control" required="">
                                            <option value="">-- Chọn vị trí --</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </div>                                   
                                </div>
                                <div class="col-md-6">                                    
                                  <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">Ngày hết hạn</span>
                                        </span>
                                        <input type="text" name="exp_time" class="form-control daterange-single" value="09/11/2019" >
                                    </div>                              
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Basic content input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12">{{trans('template.Content')}}</label>
                    </div>
                    
                    <hr>
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.Intro Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.Acreage Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab3" class="nav-link" data-toggle="tab">{{trans('template.Price Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab4" class="nav-link" data-toggle="tab">{{trans('template.Overview')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab5" class="nav-link" data-toggle="tab">{{trans('template.Contact')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab6" class="nav-link" data-toggle="tab">{{trans('template.Location Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab7" class="nav-link" data-toggle="tab">{{trans('template.Image Post')}}</a></li>
                            
                        </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="basic-tab1">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" name="intro" class="form-control col-md-12 col-xs-12" required="">{{old('intro')}}</textarea>
                            </div>
                        </div>

                       <div class="tab-pane fade" id="basic-tab2">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                             <div class="row">
                                <div class="col-lg-6">
                                    
                                    <div class="input-group">
                                        <input type="text" name="dientichmatbang" class="form-control" value="{{old('dientichmatbang')}}" placeholder="Diện tích mặt bằng" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">m²</span>
                                        </span>
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                    
                                    <div class="input-group">
                                        <input type="text" name="dientichsudung" class="form-control" value="{{old('dientichsudung')}}" placeholder="Diện tích sử dụng" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">m²</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="tab-pane fade" id="basic-tab3">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </span>
                                        <input type="number" name="price" class="form-control" value="{{old('price')}}" placeholder="Giá bán" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">.00</span>
                                        </span>
                                    </div>
                                </div>
                               
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control form-control" name="typegia">
                                            <option value="1">VND</option>
                                            <option value="2">USD</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab4">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="10" id="tongquanbreak" name="tongquan" class="form-control col-md-12 col-xs-12">{{old('tongquan')}}</textarea>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab5">
                             <label class="control-label col-md-12 col-sm-12 col-xs-12 mt-1" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Tên liên hệ:</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="name_contact" class="form-control" value="{{old('name_contact')}}" placeholder="Nhập tên liên hệ" />
                                    </div>
                                </div>
                                  <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Email:</label>
                                    <div class="col-lg-8">
                                        <input type="email" name="email_contact" class="form-control" value="{{old('email_contact')}}" id="email" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Số điện thoại:</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="phone_contact" class="form-control" value="{{old('phone_contact')}}" placeholder="Số điện thoại" />
                                    </div>
                                </div>
                               
                               
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab6">
                            <div class="container mt-3">
                                 
                                  <!-- Trigger the modal with a button -->
                                  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="padding: 0px 20px; font-size: 14px; border-radius: 2rem; z-index: 9999">Hướng dẫn nhúng iframe Google Maps</button>
                                  <!-- Modal -->
                                  <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog" style="top:110px">
                                    
                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                         
                                          <h4 class="modal-title">Hướng dẫn nhúng iframe google map</h4>
                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <h6>Bước 1: Đăng nhập và truy cập vào website Google Maps tại địa chỉ: <a href="https://www.google.com/maps/preview" target="_blank" />https://www.google.com/maps/preview</a></h6>
                                                <p> - Nhập địa chỉ của bạn tại ô tìm kiếm bản đồ.</p>
                                            <h6>Bước 2: Click vào menu để lấy mã code</h6>
                                                <p> - Click vào menu bản đồ Google Maps</p>
                                                <p> - Click vào mục Chia sẻ & Nhúng bản đồ – Share & Embed map để lấy mã code</p>
                                            <h6>Bước 3: Lấy mã code nhúng vào website</h6>
                                                <p> - Click vào mục Nhúng bản đồ – Tab Embel maps</p>
                                                <p> - Copy mã code Google Maps nhúng code vào website</p>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>                                      
                                    </div>
                                  </div>                                  
                                </div>
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <textarea rows="6" name="vitridacdia" class="form-control col-md-12 col-xs-12">{{old('vitridacdia')}}</textarea>
                            </div>
                        </div>

                         <div class="tab-pane fade" id="basic-tab7">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                             <p style="color: red">Ghi chú: Chỉ cho phép định dạng jpg, png, jpeg, gif, svg và kích thước tối đa tệp tin là 2MB.</p>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="images[]" class="form-input" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="images[]" class="form-input" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="images[]" class="form-input" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="images[]" class="form-input" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="images[]" class="form-input" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="images[]" class="form-input" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="images[]" class="form-input" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="images[]" class="form-input" >
                            </div>                          
                        </div>
                    </div>
                </div>
                <!-- /basic content input -->
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var url = "../../api/province";
        $.getJSON(url,function(result){
            $.each(result, function(i, field){
                var id      = field.provinceid;
                var name    = field.name;
                var type    = field.type;
                $("#province").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });

        //Khi chọn tỉnh thành
        $("#province").change(function() {
            $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
            var idprovince = $("#province").val();
            var url = "../../api/district/"+idprovince;
            $.getJSON(url,function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var id      = field.districtid;
                    var name    = field.name;
                    var type    = field.type;
                    $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
                });
            });
        });
    })


  let textareaText = $('#tongquanbreak').val();
     textareaText = textareaText.replace(/\r?\n/g, '<br />');
</script>
@endsection
