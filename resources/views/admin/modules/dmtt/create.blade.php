@extends('admin.master')
@section('title','Thêm Danh Mục Tin Tức')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('dmtt.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Add CateNews')}}</legend>

                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Parent CateNews')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                           <select name="parent_id" class="form-control">
                               <option value="0">ROOT</option>
                                @foreach($menus  as $item)
                                    <option value="{{$item->id}}" {{ (old('parent_id') == $item->id)? 'selected': ''}}>{{$item->name}}</option>
                                @endforeach
                           </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Name CateNews')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="name" value="{{old('name')}}" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Alias CateNews')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="alias" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                   
                   
                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection