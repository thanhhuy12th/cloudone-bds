@extends('admin.master')
@section('title','Video')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('video.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-lg font-weight-bold">{{trans('template.Video')}}</legend>

                       <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Name Video')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="name" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Link Video')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" id="editor1" name="linkyoutube" class="form-control col-md-12 col-xs-12"></textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                     <!-- Basic status input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Status Video')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                             <select name="status" class="form-control" />
                                <option value="1">Nổi bật</option>
                                <option value="0">Không nổi bật</option>
                           </select>
                            {{-- <input type="text" name="status" class="form-control" value="{{old('status')}}" /> --}}
                        </div>
                    </div>
                    <!-- /basic status input -->

                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection