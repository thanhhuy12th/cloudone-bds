@extends('admin.master') 
@section('title','Danh Sách Video') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.List Video')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('video.create')}}">
                <button type="button" class="btn btn-primary">Thêm Video</button>
            </a>
        </div>
        <form action="{{route('video.destroy',['action' => 'delete'])}}" method="POST">
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
                    </th>
                    <th>#</th>
                    <th>Tên Video</th>
                    <th>Link Video</th>
                    <th>Status</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Chọn</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($video as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->linkyoutube}}</td>
                    <td>
                        <div class="form-check form-check form-check-switchery form-check-switchery-sm">
                            <label class="form-check-label">
                                <input type="checkbox" data-id="{{ $item->id }}" name="status" class="form-input-switchery" {{ $item->status ? 'checked' : '' }}  >
                            </label>
                        </div>
                    </td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">
                        <a href="{{ route('video.edit',['video' => $item->id]) }}"><i class="icon-pencil7">{{trans('template.Edit')}}</i></a> &nbsp;
                      
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.Delete')}}</button><br/><br>              
        </form>
    </div>
    <!-- /page length options -->
</div>
<script type="text/javascript">

$(document).ready(function () {
    $('.form-input-switchery').on('click',function () {
        var Id = $(this).data('id');
        var status = this.checked;

        $.ajax({
            type: "POST",
            url: "../api/video",
            data: {
                id: Id,
                status: status
            },
            success: function (result) {  
                    console.log(result.msg);
            },
            error: function () {
                console.log('Bị hack rồi nha bạn :D');
            }
        })
    });
});

</script>
@endsection