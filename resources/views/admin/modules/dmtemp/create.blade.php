@extends('admin.master')
@section('title','Thêm Danh Mục Căn Hộ')
@section('content')
<div class="content">
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-validate-jquery" action="{{route('danh-muc-can-ho.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thêm Danh Mục</legend>
                    <div class="col-md-12 row">
                        <div class="col-md-6">
                             <!-- Basic select -->
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Chọn danh mục<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                   <select name="parent_id" class="form-control">
                                       <option value="0">ROOT</option>
                                   </select>
                                </div>
                            </div>
                            <!-- /basic select -->

                            <!-- Basic text input -->
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Tên danh mục<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text"  name="name" class="form-control form-control-maxlength" value="{{old('name')}}" maxlength="50" />
                                </div>
                            </div>
                            <!-- /basic text input -->

                            <!-- Basic text input -->
                            <div class="form-group row" hidden="">
                                <label class="col-form-label col-lg-3">{{trans('template.Alias Category')}}</label>
                                <div class="col-lg-9">
                                    <input type="text" name="alias" class="form-control" />
                                </div>
                            </div>
                            <!-- /basic text input -->
             
                        </div>
                        <div class="col-md-6">
                            
                            <!-- Basic text input -->
                            <div class="form-group row" >
                                <label class="col-form-label col-md-3">Vị trí</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="number" name="position" placeholder="1-9999" >
                                </div>
                            </div>
                            <!-- /basic text input -->            
                              <div class="form-group row">
                                <label class="col-form-label col-lg-3">Hot</label>
                                <div class="col-lg-9">
                                    <select name="hot" class="form-control">
                                       <option value="1">Active</option>
                                       <option value="0">InActive</option>
                                   </select>
                                </div>
                            </div>
                                <!-- end -->                      
                        </div>                           
                    </div> 
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit"  class="btn btn-primary ml-3">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection