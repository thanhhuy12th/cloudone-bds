@extends('admin.master') 
@section('title','Danh Sách Danh Mục Tin Đăng') 
@section('content')
@include('admin.blocks.alert')
<div class="content">
	<!-- Individual column searching (text inputs) -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Danh sách căn hộ</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>
					<div class="card-body">
			            <a href="{{route('danh-muc-can-ho.create')}}">
			                <button type="button" class="btn btn-primary">Thêm danh mục</button>
			            </a>
			        </div>
			        <form action="{{route('danh-muc-can-ho.destroy',['action'=>'delete'])}}" method="POST">
			            @method('DELETE')
			            @csrf
			        <table class="table datatable-column-search-inputs">
						<thead>
							<tr>
				               <th>
			                        <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
			                    </th>
				                <th>Name</th>
				                <th>Alias</th>
				                <th>Location</th>
				                <th>Start date</th>
				                <th class="text-center">Action</th>
				            </tr>
						</thead>
						<tbody>
							@foreach($dmtemp as $item)
							<tr>
								<td>
			                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
			                    </td>
				                <td>{{ $item->name }}</td>				              
				                <td>{{ $item->alias }}</td>
				                <td>
				                	@if ($item->hot == 1)
			    						<span class="badge badge-danger">Hot</span>
									@elseif ($item->hot == 0)
			    						<span class="badge badge-primary">Status</span>
									@endif				                	
				                </td>
				                <td><a href="#">{{ date("d/m/Y",strtotime($item ->created_at))}}</a></td>
								<td class="text-center">
									<a href="{{ route('danh-muc-can-ho.edit',['danh-muc-can-ho' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa danh mục"><i class="icon-pencil7"></i></a> &nbsp;
								</td>
			            </tr>
				            @endforeach				        
						</tbody>
						<tfoot>
							<tr>
				                <td>Name</td>
				                <td>Alias</td>
				                <td>Location</td>
				                <td>Start date</td>
				                <td></td>
				                <td></td>
				            </tr>
						</tfoot>
					</table>
					 <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.Delete')}}</button><br/><br>              
        		</form>					
			</div>
				<!-- /individual column searching (text inputs) -->
</div>

@endsection