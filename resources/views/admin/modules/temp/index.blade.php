@extends('admin.master') 
@section('title','Danh Sách Căn Hộ') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Danh sách căn hộ</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('can-ho.create')}}">
                <button type="button" class="btn btn-primary">Thêm căn hộ</button>
            </a>
        </div>
        <form id="listForm" action="{{route('can-ho.destroy',['action' => 'delete'])}}" method="POST">
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                    </th>
                    <th>ID</th>
                    <th>Tiêu đề</th>
                    <th>Diện tích(m²)</th>
                    <th>Giá(VND)</th>
                    <th>Ưu tiên</th>
                    <th>Duyệt bài</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($temp as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="checked[]" value="{{$item->id}}">
                    </td>
                    <td>{{$item->id}}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->dientich .'m²' }}</td>
                    <td>{{ number_format($item->priceCDT,0,",",".") }}</td>
                      <td>
                        <div class="form-check form-check-switchery">
                            <label class="form-check-label">
                                <input type="checkbox" data-id="{{ $item->id }}" name="hot" class="form-check-input-switchery" {{ $item->hot ? 'checked' : '' }} >
                            </label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check form-check-switchery form-check-switchery-sm">
                            <label class="form-check-label">
                                <input type="checkbox" data-id="{{ $item->id }}" name="status" class="form-input-switchery" {{ $item->status ? 'checked' : '' }}  >
                            </label>
                        </div>
                    </td>                  
                    <td>{{ date("d/m/Y",strtotime($item ->created_at))}}</td>
                    <td class="text-center">                       
                        <a href="{{ route('can-ho.edit',['can-ho' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa tin căn hộ"><i class="icon-pencil7"></i></a> &nbsp;
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.Delete')}}</button><br/><br>              
        </form>
    </div>
    <!-- /page length options -->
</div>
<script type="text/javascript">
    function checkallClick(o) {
            var form = document.listForm;
            for (var i = 0; i < form.elements.length; i++) {
                if (form.elements[i].type == "checkbox" && form.elements[i].name!="checkAll") {
                    form.elements[i].checked = document.listForm.checkAll.checked;
                }
            }
        }


$(document).ready(function () {
    $('.form-check-input-switchery').on('click',function () {
        var Id = $(this).data('id');
        var hot = this.checked;
        $.ajax({
            type: "POST",
            url: "../api/can-ho",
            data: {
                id: Id,
                hot: hot
            },
            success: function (result) {  
                    console.log(result.msg);
            },
            error: function () {
                console.log('Lỗi rồi,debug đi nhé.');
            }
        })
    });
});
$(document).ready(function () {
    $('.form-input-switchery').on('click',function () {
        var Id = $(this).data('id');
        var status = this.checked;
        $.ajax({
            type: "POST",
            url: "../api/duyet-bai-can-ho",
            data: {
                id: Id,
                status: status
            },
            success: function (result) {  
                    console.log(result.msg);
            },
            error: function () {
                console.log('Bị hack rồi nha bạn :D');
            }
        })
    });
});

</script>
@endsection
