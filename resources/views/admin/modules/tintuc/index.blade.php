@extends('admin.master') 
@section('title','Danh Sách Tin Tức') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.List News')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('tin-tuc.create')}}">
                <button type="button" class="btn btn-primary">Thêm Tin Tức</button>
            </a>
        </div>
        <form action="{{route('tin-tuc.destroy',['action' => 'delete'])}}" method="POST">
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
                    </th>
                    <th>ID</th>
                    <th>Tên căn hộ</th>
                    <th>Mô tả</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($news as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->intro }}</td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">
                        <a href="{{ route('tin-tuc.edit',['tin-tuc' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa tin dự án"><i class="icon-pencil7"></i></a> &nbsp;
                      
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.Delete')}}</button><br/><br>              
        </form>
    </div>
    <!-- /page length options -->
</div>
@endsection