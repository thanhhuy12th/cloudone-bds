@extends('admin.master')
@section('title','Chỉnh Sửa Tin Tức')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('tin-tuc.update',['id' => $news->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.News')}}</legend>
                     <!-- Basic title input -->
                    <div class="form-group row">
                         <label class="col-form-label col-lg-2">Tên tin tức<span class="text-danger">*</span></label>
                         <div class="col-lg-9">
                            <input type="text" name="name" class="form-control form-control-maxlength" value="{{old('name',$news->name) }}" maxlength="50">
                        </div>
                    </div>
                    <!-- /basic title input -->

                    <!-- Basic title input -->

                    <div class="form-group row">
                         <label class="col-form-label col-lg-2">Tiêu đề<span class="text-danger">*</span></label>
                         <div class="col-lg-9">
                            <input type="text" name="title" class="form-control form-control-maxlength" value="{{old('title',$news->title)}}" maxlength="150">
                        </div>
                    </div>
                    
                    <!-- /basic title input -->

                    <!-- Basic alias input -->
                    <div class="form-group row" hidden="">
                        <label class="col-form-label col-lg-2">{{trans('template.Alias News')}}</label>
                        <div class="col-lg-9">
                            <input type="text" name="alias" class="form-control" />
                        </div>
                    </div>
                    <!-- /basic alias input -->

                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2"></label>
                        <div class="col-lg-9">
                            <img src="{{asset('uploads/postings/'.$news->image)}}" width="50%">
                        </div>
                    </div>
                    <!-- /styled file uploader -->


                    <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{trans('template.Image News')}}</label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-input-styled" >
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Basic content input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12">{{trans('template.Content')}}</label>
                    </div>
                    
                    <hr>
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.Intro News')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.Content News')}}</a></li>
                        </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="basic-tab1">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Intro')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" name="intro" class="form-control col-md-12 col-xs-12" >{{old('intro',$news->intro)}}</textarea>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab2">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Content News')}}<span class="text-danger">*</span>
                            </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="editor1" name="content" class="form-control col-md-12 col-xs-12">{{old('content',$news->content)}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic content input -->
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection