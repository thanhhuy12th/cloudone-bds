@extends('admin.master') 
@section('title','Danh Sách Liên Hệ') 
@section('content')
@include('admin.blocks.alert')
<div class="content">

    <!-- Basic initialization -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('template.List Contact')}}</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            <form action="{{route('lienhe.destroy',['action' => 'delete'])}}" method="POST">
                @method('DELETE')
                @csrf
            <table class="table datatable-button-html5-basic">
                <thead>
                    <tr>
                        <th>
                            <input type="checkbox" name="chkall"  onClick="chkallClick(this);">
                        </th>                   
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Nội dung</th>
                        <th>Hoạt động</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contact as $item)
                        <tr>
                            <td>
                                <input type="checkbox" name="chk[]" value="{{$item->id}}">
                            </td>                 
                            <td>{{$item->email}}</td>
                            <td>{{$item->phone}}</td>
                            <td>{{$item->content}}</td>
                            <td>{{ date("d/m/Y",strtotime($item ->created_at))}}</td>
                            <td class="text-center">
                                <a href="{{ route('lienhe.edit',['lienhe' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa liên hệ"><i class="icon-pencil7"></i></a> &nbsp;
                            </td>
                        </tr>
                        @endforeach                            
                </tbody>

            </table>
               <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px">{{trans('template.Delete')}}</button> <br/><br>
         </form>

        </div>
        <!-- /basic initialization -->   
</div>
@endsection