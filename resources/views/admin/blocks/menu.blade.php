<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->

    <!-- Sidebar content -->
    <div class="sidebar-content">
        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="{{ asset('uploads/avatar/'.Auth::guard('admin')->user()->image)}}" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">{{Auth::guard('admin')->user()->fullname}}</div>
                        <div class="font-size-xs opacity-50">
                            <i style="color: green" class="icon-primitive-dot"></i> &nbsp;Online
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->
       
        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            @if(Auth::guard('admin')->user()->role ==1 )
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">ADMIN</div> <i class="icon-menu" title="Admin"></i></li>
                    <li class="nav-item">
                        <a href="./" class="nav-link active">
                            <i class="icon-home4"></i>
                            <span>
                                        Trang Chủ
                                    </span>
                        </a>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-people"></i> <span>Quản lý tài khoản</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Tài khoản">
                            <li class="nav-item"><a href="{{route('admin.index')}}" class="nav-link">User Admin</a></li>
                            <li class="nav-item"><a href="{{route('member.index')}}" class="nav-link">Khách hàng</a></li>
                        </ul>
                    </li>
                     <li class="nav-item ">
                        <a href="{{route('menu.index')}}" class="nav-link"><i class="icon-menu3"></i> <span>Menu</span></a>
                    </li>
                    <li class="nav-item ">
                        <a href="{{route('slider.index')}}" class="nav-link"><i class="icon-images3"></i> <span>Slider</span></a>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-city"></i> <span>Quản lý Căn hộ</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Dự án">
                            <li class="nav-item"><a href="{{route('danh-muc-can-ho.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('can-ho.index')}}" class="nav-link">Danh sách căn hộ</a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack-text"></i> <span>Quản lý tin đăng</span></a>
                        
                        <ul class="nav nav-group-sub" data-submenu-title="Tin đăng">
                            <li class="nav-item"><a href="{{route('dmtd.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('tindang.index')}}" class="nav-link">Danh sách tin đăng</a></li>
                        </ul>
                    </li>                 
                    <li class="nav-item ">
                        <a href="{{route('tin-tuc.index')}}" class="nav-link"><i class="icon-stack"></i> <span>Tin tức</span></a>
                    </li>                 
                    <li class="nav-item ">
                        <a id="menu_seen" href="{{route('lienhe.index')}}" class="nav-link"><i class="icon-phone"></i> <span>Liên hệ</span></a>
                    </li>
                  
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-cog3"></i> <span>Cấu hình website</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Cấu hình">
                            <li class="nav-item"><a href="{{route('gioithieu.index')}}" class="nav-link">Giới thiệu</a></li>
                            <li class="nav-item"><a href="{{route('logo.index')}}" class="nav-link">Logo - Banner</a></li>
                            <li class="nav-item"><a href="{{route('mangxahoi.index')}}" class="nav-link">Mạng xã hội</a></li>
                            <li class="nav-item"><a href="{{route('googlemap.index')}}" class="nav-link">Google Map</a></li>
                            <li class="nav-item"><a href="{{route('contactus.index')}}" class="nav-link">Liên hệ</a></li>
                            <li class="nav-item"><a href="{{route('footer.index')}}" class="nav-link">Footer</a></li>
                            <li class="nav-item"><a href="{{route('cauhinhkhac.index')}}" class="nav-link">Cấu hình khác</a></li>
                        </ul>
                    </li> 
                </ul>
            @else
                 <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">NHÂN VIÊN</div> <i class="icon-menu" title="Nhân viên"></i></li>
                <li class="nav-item">
                    <a href="./" class="nav-link active">
                        <i class="icon-home4"></i><span>Trang Chủ</span>
                    </a>
                </li>             
                <li class="nav-item ">
                    <a href="{{route('slider.index')}}" class="nav-link"><i class="icon-images3"></i> <span>Slider</span></a>
                </li>
                 <li class="nav-item ">
                    <a href="{{route('can-ho.index')}}" class="nav-link"><i class="icon-city"></i> <span>Đăng tin căn hộ</span></a>
                </li>
                 <li class="nav-item ">
                    <a href="{{route('tindang.index')}}" class="nav-link"><i class="icon-stack-text"></i> <span>Tin Đăng</span></a>
                </li>              
              <li class="nav-item ">
                    <a href="{{route('tin-tuc.index')}}" class="nav-link"><i class="icon-stack"></i> <span>Tin tức</span></a>
                </li>   
                <li class="nav-item ">
                    <a id="menu_seen" href="{{route('lienhe.index')}}" class="nav-link"><i class="icon-phone"></i> <span>Liên hệ</span></a>
                </li>
            </ul>
            @endif  
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<script type="text/javascript">
    $(document).ready(function () {
    $('#menu_seen').on('click',function () {       
        $.ajax({
            type: "GET",
            url: "../api/change-lien-he",
            success: function (result) {
                $('#count_seen').empty();
                   $('#count_seen').text(0);
                    console.log(result);
            },
            error: function () {
                console.log('Lỗi rồi,debug đi nhé.');
            }
        })
    });
});
</script>