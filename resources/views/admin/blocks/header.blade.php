@if(Auth::guard('admin')->user()->role ==1 )
    <div class="navbar navbar-expand-md navbar-dark">
        <style>
            .height-logo-icon{
                height: 50px !important;
            }
            .height-logo-icon img{
                height: 30px !important;
                width: 100px !important;
            }
        </style>
        <div class="navbar-brand height-logo-icon">
            <a href="./" class="d-inline-block">
                {{-- <img src="{{asset('backend/global_assets/images/logo_light.png')}}" alt=""> --}}
                <img src="{{ asset('uploads/logo/'.$logo ->content)}}" width="100%">
               
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>     
            </ul>
            <span class="badge bg-success ml-md-3 mr-md-auto">Online</span>

            <ul class="navbar-nav">
                <li class="nav-item dropdown" >
                    <a id="seen" href="#"  class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false">
                        <i class="icon-bubbles4"></i>
                        <span class="d-md-none ml-2">Messages</span>

                        <span id="count_seen" class="badge badge-pill bg-warning-400 ml-auto ml-md-0">{{$count_lienhe}}</span>
                    </a>
                    
                    <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                        <div class="dropdown-content-header">
                            <span class="font-weight-semibold">Messages</span>                            
                        </div>
                    @foreach($get_count_lienhe as $key)
                        <div  class="dropdown-content-body dropdown-scrollable" style="padding: 0px 20px 10px 10px;">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="mr-3 position-relative">
                                        <img src="{{asset('backend/global_assets/images/placeholders/placeholder.jpg')}}" width="36" height="36" class="rounded-circle" alt="">
                                    </div>                               
                                    <div class="media-body">
                                        <div class="media-title">
                                            <a href="#">
                                                <span class="font-weight-semibold">{{ $key->name }}</span>
                                                <span class="text-muted float-right font-size-sm">{{ $key->created_at }}</span>
                                            </a>
                                        </div>

                                        <span class="text-muted" style="overflow: hidden; text-overflow: ellipsis; -webkit-line-clamp: 2; display: -webkit-box; -webkit-box-orient: vertical; height: 40px;">{{ $key->content }}</span>
                                    </div>
                                </li>                           
                            </ul>
                        </div>

                    @endforeach
                        <div class="dropdown-content-footer justify-content-center p-0">
                            <a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="" data-original-title="Load more"><i class="icon-menu7 d-block top-0"></i></a>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('uploads/avatar/'.Auth::guard('admin')->user()->image)}}" class="rounded-circle mr-2" height="34" alt="">
                        <span>{{Auth::guard('admin')->user()->fullname}}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                       
                        <a href="{{ route('admin.edit',[Auth::guard('admin')->user()->id]) }}" class="dropdown-item"><i class="icon-cog5"></i> Thông tin cá nhân</a>
                        <a href="{{ route('change-password')}}" class="dropdown-item"><i class="icon-key"></i>Đổi mật khẩu</a>
                        <a href="{{route('getLogout')}}" class="dropdown-item"><i class="icon-switch2"></i>Đăng xuất</a>
                    </div>
                </li>               
            </ul> 
        </div>
    </div>
@else 
    <div class="navbar navbar-expand-md navbar-dark">
    <style>
        .height-logo-icon{
            height: 50px !important;
        }
        .height-logo-icon img{
            height: 30px !important;
            width: 100px !important;
        }
    </style>
    <div class="navbar-brand height-logo-icon">
        <a href="./admin" class="d-inline-block">
          
            <img src="{{ asset('uploads/logo/'.$logo ->content)}}" width="100%">
           
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>     
        </ul>
        <span class="badge bg-success ml-md-3 mr-md-auto">Online</span>
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false">
                        <i class="icon-bubbles4"></i>
                        <span class="d-md-none ml-2">Messages</span>
                        <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0">{{$count_lienhe}}</span>
                    </a>
                    
                    <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                        <div class="dropdown-content-header">
                            <span class="font-weight-semibold">Messages</span>                            
                        </div>
                    @foreach($get_count_lienhe as $key)
                        <div class="dropdown-content-body dropdown-scrollable" style="padding: 0px 20px 10px 10px;">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="mr-3 position-relative">
                                        <img src="{{asset('backend/global_assets/images/placeholders/placeholder.jpg')}}" width="36" height="36" class="rounded-circle" alt="">
                                    </div>                               
                                    <div class="media-body">
                                        <div class="media-title">
                                            <a href="#">
                                                <span class="font-weight-semibold">{{ $key->name }}</span>
                                                <span class="text-muted float-right font-size-sm">{{ $key->created_at }}</span>
                                            </a>
                                        </div>
                                        <span class="text-muted">{{ $key->content }}</span>
                                    </div>
                                </li>                           
                            </ul>
                        </div>
                    @endforeach
                        <div class="dropdown-content-footer justify-content-center p-0">
                            <a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="" data-original-title="Load more"><i class="icon-menu7 d-block top-0"></i></a>
                        </div>
                    </div>
                </li>
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('uploads/avatar/'.Auth::guard('admin')->user()->image)}}" class="rounded-circle mr-2" height="34" alt="">
                    <span>{{Auth::guard('admin')->user()->fullname}}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">                   
                    {{-- <a href="{{ route('admin.edit',[Auth::guard('admin')->user()->id]) }}" class="dropdown-item"><i class="icon-cog5"></i> Thông tin cá nhân</a> --}}
                    <a href="{{ route('change-password')}}" class="dropdown-item"><i class="icon-key"></i>Đổi mật khẩu</a>
                    <a href="{{route('getLogout')}}" class="dropdown-item"><i class="icon-switch2"></i>Đăng xuất</a>
                </div>
            </li>
        </ul>
    </div>
</div>


@endif
<script type="text/javascript">
    $(document).ready(function () {
    $('#seen').on('click',function () {
       // var Id = $(this).data('id');
        $.ajax({
            type: "GET",
            url: "../api/change-lien-he",
            success: function (result) {
                $('#count_seen').empty();
                   $('#count_seen').text(0);
                    console.log(result);
            },
            error: function () {
                console.log('Lỗi rồi,debug đi nhé.');
            }
        })
    });
});
</script>