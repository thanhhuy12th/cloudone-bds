<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body>
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
    <div class="wrap-login ">
        <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Khôi phục mật khẩu') }}</div>
                @if (\Session::has('success'))
                            <span class="alert-success"  id="flash-message" style="padding: 5px">
                                {!! __(\Session::get('success')) !!}
                            </span>
                        @endif
                        @if (\Session::has('error'))
                            <div class="alert-danger"  id="flash-message" style="padding: 5px" />
                                {!! __(\Session::get('error')) !!}
                            </div>
                        @endif
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                               
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Gửi Xác Nhận') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




        <!-- <div class="container">
            <div class="login-backgroud" style="min-height: 500px">
                <div class="login-form">                   
                    <form id="quenmatkhau" action="" method="POST" >
                
                        <div class="form-group">
                            <i class="fas fa-envelope-open"></i>
                            <label>Địa chỉ email</label>
                            <input type="email" class="form-control" name="email" value="" placeholder="Nhập địa chỉ email" />
                        </div>
                        <button type="submit" class="btn btn-primary">Gửi xác nhận</button>
                    </form>
                </div>
            </div>
        </div> -->
    </div>
    <!-- Footer -->
    <footer class="footer">
        @include('frontend.blocks.footer')
    </footer>

</body>

</html>