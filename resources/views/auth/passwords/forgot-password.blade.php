<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body>
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
    <div class="wrap-login ">
        <div class="container">
            <div class="login-backgroud" style="min-height: 500px">
                <div class="login-form">                   
                    <form id="quenmatkhau" action="{{ route('password.email') }}" method="POST" >
                    @csrf 
                        <div class="form-group">
                            <i class="fas fa-envelope-open"></i>
                            <label>Địa chỉ email</label>
                            <input type="email" class="form-control" name="email" value="" placeholder="Nhập địa chỉ email" />
                        </div>
                        <button type="submit" class="btn btn-primary">Gửi xác nhận</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="footer">
        @include('frontend.blocks.footer')
    </footer>

</body>

</html>