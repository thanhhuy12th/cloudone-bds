<!DOCTYPE html>
<html lang="en">
<head>
<title>Khôi phục mật khẩu</title>

<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Space Login Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->

<!-- css files -->
<link href="{{asset('login/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
<!-- css files -->

<!-- Online-fonts -->
<link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese" rel="stylesheet">
<!-- //Online-fonts -->

</head>
<body>
  <!-- main -->
  <div class="main">
    <div class="main-w3l">
           
      <h1 class="logo-w3">{{ __('Khôi phục mật khẩu') }}</h1>
       @if (\Session::has('success'))
            <span class="alert-success"  id="flash-message" style="padding: 5px; color: #f7f7f7">
                {!! __(\Session::get('success')) !!}
            </span>
        @endif
        @if (\Session::has('error'))
            <div class="alert-danger"  id="flash-message" style="padding: 5px; color: #f7f7f7" />
                {!! __(\Session::get('error')) !!}
            </div>
        @endif
      <div class="w3layouts-main">
        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
          <form action="{{ route('admin-password.update') }}" method="POST">
             @csrf

              <input type="hidden" name="token" value="{{ $token }}">

            <input placeholder="E-mail" name="email" type="email" value="{{ $email ?? old('email') }}" required autofocus>
               @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif

               <input placeholder="Password" name="password" type="password" required="">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

              <input placeholder="Password" name="password_confirmation" type="password" required="">

             <input type="submit" value="Reset Password" name="Reset Password">
          </form>
      </div>
      <!-- //main -->
      
      
    </div>
   
  </div>

</body>
</html>