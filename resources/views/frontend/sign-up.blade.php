<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
</head>

<body>
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
    <div class="wrap-login">
        <div class="container">
            <div class="login-backgroud">

                <div class="login-form">
                    <!-- dăng nhập -->

                    <!-- Đăng ký -->
                    <form id="dangky" action="{{route('dangky')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                       
                        <div class="form-group">
                            <i class="fas fa-envelope-open"></i>
                            <label>Địa chỉ email</label>
                            <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Nhập địa chỉ email" required="">
                        </div>
                         <div class="form-group telephone">
                            <i class="fas fa-mobile-alt"></i>
                            <label>Số điện thoại</label>
                            <input type="tel" class="form-control" name="phone" value="{{old('phone')}}" placeholder="Nhập số điện thoại" required="">
                            <div class="alert alert-warning">
                                <strong>Số điện thoại phải là số</strong> 
                            </div>
                        </div>
                       
                        <div class="form-group password">
                            <i class="fas fa-key"></i>
                            <label>Mật khẩu</label>
                            <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" required="">
                            <div class="alert alert-info">
                                <strong>Mật khẩu của bạn phải trên 6 ký tự</strong> 
                            </div>
                            <div class="alert alert-warning">
                                <strong>Mật khẩu của bạn đã vượt quá 25 ký tự</strong> 
                            </div>
                        </div>
                        <div class="form-group password_confirmation">
                            <i class="fas fa-key"></i>
                            <label>Nhập lại mật khẩu</label>
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Nhập lại mật khẩu"  />
                            <div class="alert alert-warning">
                                <strong>Mật khẩu của bạn không trùng khớp</strong> 
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6LewA7oUAAAAANLI8MKhRUWB8kagUy4w8UC_lnNq"></div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-3">Đăng ký</button>

                    </form>

                </div>
            </div>
        </div>
    </div>


    <!-- Footer -->
    <footer class="footer">
        @include('frontend.blocks.footer')
    </footer>
 @include('frontend.blocks.lienhe')
</body>

</html>