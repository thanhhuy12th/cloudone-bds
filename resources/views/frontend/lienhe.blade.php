<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')  
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
</head>


<body>
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
    
    @include('frontend.blocks.leftslide')

    @yield('content')

    <!-- Footer -->
  
    @include('frontend.blocks.footer')
    

    <!-- Hotline -->
    @include('frontend.blocks.lienhe')
    
    <!-- Slide Bar Thông tin khuyến mãi -->
    <div class="btn-slide-bar" id="mybtnslide">
        <span class="symbol-btn-slide-bar">
            <i class="fas fa-arrow-circle-right"></i>
        </span>
    </div>



    <!-- Back to top -->
    <div class="btn-back-to-top" id="myBtn" style="display:none;">
        <span class="symbol-btn-back-to-top">
            <i class="fa fa-angle-double-up" aria-hidden="true"></i>
        </span>
    </div>

</body>

</html>