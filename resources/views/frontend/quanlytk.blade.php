<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body>
    @include('frontend.blocks.menu')
    <section class="info pt-5">
        <div class="container">
            <div class="info-header">
                <div class="avatar">
                    <img src="{{ asset('uploads/avatar/'.$user ->image) }}">
                </div>
                <div class="avatar-name">
                    <p>{{ $user->fullname }}</p>
                </div>
            </div>

            <div class="info-body">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-news" role="tab"><i class="fas fa-newspaper"></i>Quản lý tin đăng</a>
                    <a class="nav-item nav-link" id="nav-canho-tab" data-toggle="tab" href="#nav-canho" role="tab"><i class="far fa-building pr-1"></i>Quản lý tin căn hộ</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"><i class="fas fa-info-circle"></i>Thông tin cá nhân</a>
                    <a class="nav-item nav-link" id="nav-changepass-tab" data-toggle="tab" href="#nav-changepass" role="tab"><i class="fas fa-key"></i>Đổi mật khẩu</a>
                </div>
            </nav>

            <div class="tab-content pt-3 pb-3" id="nav-tabContent">

                @include('frontend.quanlytd')

                @include('frontend.quanlycanho')

                <div class="tab-pane fade" id="nav-profile" role="tabpanel">
                    <form id="thongtincanhan" action="{{route('quanlytk.update')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <i class="fas fa-user"></i>
                            <label>Họ và tên</label>
                            <input type="text" class="form-control" name="fullname" value="{{$user->fullname}}" placeholder="Nhập họ tên" >
                        </div>
                        <div class="form-group">
                            <i class="fas fa-signature"></i>
                            <label>Tên đăng ký</label>
                            <input type="text" class="form-control" name="username" value="{{$user->username}}" placeholder="Nhập tên đăng ký" >
                        </div>
                        <div class="form-group">
                            <i class="fas fa-envelope-open"></i>
                            <label>Địa chỉ email</label>
                            <input type="email" class="form-control" name="email" value="{{$user->email}}" placeholder="Nhập địa chỉ email" readonly="">
                        </div>
                        <div class="form-group">
                            <i class="fas fa-mobile-alt"></i>
                            <label>Số điện thoại</label>
                            <input type="tel" class="form-control" name="phone" value="{{$user->phone}}" placeholder="Nhập số điện thoại">
                        </div>
                        <div class="form-group">
                            <i class="fas fa-map-marker-alt"></i>
                            <label>Địa chỉ</label>
                            <input type="text" class="form-control" name="address" value="{{$user->address}}" placeholder="Nhập địa chỉ">
                        </div>
                        <div class="form-group" hidden="">
                            <i class="fas fa-key"></i>
                            <label>Mật khẩu</label>
                            <input type="password" class="form-control" placeholder="Nhập mật khẩu">
                        </div>
                        <div class="form-group">
                            <i class="fas fa-image"></i>
                            <label>Hình đại diện</label>
                            <div class="form-group">
                                <img src="{{ asset('uploads/avatar/'.$user ->image)}}" width="100px">
                            </div>
                            <input type="file" name="image" class="form-control-file">
                        </div>
                        <button type="submit" class="btn btn-primary mt-3">Cập nhật</button>
                    </form>
                </div>
               @include('frontend.change-password')

            </div>
        </div>
        </div>
    </section>

    <footer class="footer">
        @include('frontend.blocks.footer')
    </footer>
</body>

</html>