@extends('frontend.index')
@section('content')

<!-- Nhà đất nổi bật -->
 <section class="project-nhadat">
        <div class="container">
        <div class="sec-title">
            <div class="m-title">
                <p>@yield('title','View More Nhà đất')</p>
            </div>
        </div>        
            <div class="row">
                @foreach($nhadatnoibat as $item)
                    @if($item->position ==1 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp
                                    <span class="badge-notify"><img src="{{asset('frontend/img/vip.gif')}}" alt=""></span>
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>

                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>   
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @elseif($item->position ==2)
                    <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp
                                   <span class="badge-notify-hot"><img src="{{asset('frontend/img/hot.gif')}}" alt=""></span>
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>    
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @elseif($item->position ==3)
                    <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp                                    
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>    
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @elseif($item->position ==4)
                    <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp                                   
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>     
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @else
                    <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp                                  
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p>{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>     
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="row" >
            <div class="col-md-4"></div>
            <div class="col-md-4 pt-3" style="margin-left: 45%;">
                 <ul class="pagination" >
                     @if( $nhadatnoibat->currentPage() != 1)
                        <li class="page-item" >
                          <a class="page-link" href="{!! $nhadatnoibat->url($nhadatnoibat->currentPage() - 1) !!}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                          </a>
                        </li>
                     @endif
                     @for($i=1; $i <= $nhadatnoibat->lastPage(); $i= $i+1)
                        <li class="page-item {!! ($nhadatnoibat->currentPage() == $i )? 'active': '' !!}">
                            <a class="page-link" href="{!! $nhadatnoibat->url($i) !!}">{!! $i !!}</a></li>      
                    @endfor
                    @if($nhadatnoibat->currentPage() != $nhadatnoibat->lastPage())
                        <li class="page-item">
                          <a class="page-link" href="{!! $nhadatnoibat->url($nhadatnoibat->currentPage() + 1) !!}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                          </a>
                        </li>
                     @endif
                  </ul>
            </div>
            <div class="col-md-4"></div>
        </div>               
        </div>
        
    </section>


<!-- MODAL FORM -->
<div id="m_dientich" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top: 10%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Lựa chọn diện tích</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>Từ: <span class="red"><span id="dt_from">0</span> m2</span> đến: <span class="red"><span id="dt_to">400</span> m2</span></p>
                <div id="slider-range"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div id="m_gia" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top: 10%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Lựa chọn giá</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>Từ: <span class="red"><span id="p_from">500 triệu</span> </span> đến: <span class="red"><span id="p_to">30 tỷ</span></span></p>
                <div id="slider-range-price"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL FORM -->

<!-- Script -->
<script type="text/javascript">
    $(document).ready(function() {
        var url = "./api/province";
        $.getJSON(url, function(result) {
            $.each(result, function(i, field) {
                var id = field.provinceid;
                var name = field.name;
                var type = field.type;
                $("#province").append('<option value="' + id + '">' + " " + name + '</option>');
            });
        });

        //Khi chọn tỉnh thành
        $("#province").change(function() {
            $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
            var idprovince = $("#province").val();
            var url = "./api/district/" + idprovince;
            $.getJSON(url, function(result) {
                console.log(result);
                $.each(result, function(i, field) {
                    var id = field.districtid;
                    var name = field.name;
                    var type = field.type;
                    $("#district").append('<option value="' + id + '">' + " " + name + '</option>');
                });
            });
        });
    })


</script>

@endsection