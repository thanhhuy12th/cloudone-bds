@extends('frontend.timkiem')
@section('content')
<!-- Product -->


<div class="container-fluid">
        <div class="row">
            <div class="col-md-9">          
                <section class="product-detail">
                    <div class="container-fluid">
                       
                        <div class="alter-contanier">
                            @foreach($search_category as $item)
                                <div class="row pb-4">
                                <div class="col-lg-12 col-md-12">
                                <div class="row">
                                     @php 
                                            $splittedstring=explode("||",$item->images);   
                                    @endphp
                                        @for($i =0; $i<1; $i++)
                                    <div class="col-lg-4 col-md-4">                                
                                        <div class="alter-grid"  id="thumbnail">                                            
                                            <img class="portrait" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                        </div>
                                    </div>
                                     @endfor
                                    <div class="col-lg-8 col-md-8">
                                        <div class="grid">
                                            <div class="grid-title">
                                                <a href="{{route('product-detail',[$item->alias])}}">
                                                    <p>{!! $item->title !!}</p>
                                                </a>
                                            </div>
                                            <div class="grid-content">
                                                <p>{!! $item->intro !!}</p>
                                            </div>
                                        </div>
                                        <div class="all">
                                            <div class="meta-item gia">
                                                @if($item->typegia == 1)
                                                     <b style="color: red">Giá:</b> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                @else
                                                     <b style="color: red">Giá:</b> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                @endif          
                                               
                                            </div>

                                            <div class="meta-item dien_tich">
                                                <b style="color: red"> Diện tích:</b> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                            </div>
                                            <div class="meta-item vi_tri">
                                                <b style="color: red">Vị trí: </b> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </section>
            
            </div>

            <div class="col-md-3 pt-20"style="margin-left: -55px">
                <div class="control-bar mt-20">
                    <section class="product-detail">                       
                         <div class="right-category">
                            <h5 style="text-align: center;">Tìm kiếm</h5>
                            <div class="search-product">
                            <div class="container">
                              <form id="search" action="{{route('tim-kiem')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Danh mục--</p>
                                            </div>
                                            <div class="select-content">
                                               <select name="danhmuc">
                                                   @for($i = 0; $i < count($danhmuc); $i++)
                                                    <option value="{!!$danhmuc[$i]['id']!!}" {{ (old('category_id') == $danhmuc[$i]['name'])? 'selected': ''}}>{!!$danhmuc[$i]['name']!!}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Tỉnh Thành--</p>
                                            </div>
                                            <div class="select-content">
                                                <select  id="province"  name="province">
                                                    <option value="-1">--Chọn Tỉnh Thành--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Quận/Huyện--</p>
                                            </div>
                                            <div class="select-content">
                                                <select  id="district"  name="district">
                                                    <option value="-1">--Chọn Quận Huyện--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Khoảng diện tích--</p>
                                            </div>
                                            <div class="select-content">
                                                <select name="dientichmatbang">
                                                    <option value="-1">--Chọn diện tích--</option>
                                                    <option value="less-15">< 15 <sup>m²</sup></option>
                                                    <option value="15-25">15 <sup>m²</sup> - 25<sup>m²</sup></option>
                                                    <option value="25-50">25 <sup>m²</sup> - 50<sup>m²</sup></option>
                                                    <option value="50-100">50 <sup>m²</sup> - 100<sup>m²</sup></option>
                                                    <option value="more-100">> 100 <sup>m²</sup></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Khoảng giá--</p>
                                            </div>
                                            <div class="select-content">
                                                <select name="price">
                                                    <option value="-1">--Chọn giá--</option>
                                                    <option value="less-1m">< 1 triệu</option>
                                                    <option value="1m-3m"> 1 triệu - 3 triệu</option>
                                                    <option value="3m-5m"> 3 triệu - 5 triệu</option>
                                                    <option value="5m-10m"> 5 triệu - 10 triệu</option>
                                                    <option value="10m-100m"> 10 triệu - 100 triệu</option>
                                                    <option value="100m-1b"> 100 triệu - 1 tỷ</option>
                                                    <option value="1b-10b"> 1 tỷ - 10 tỷ</option>
                                                    <option value="more-10b"> > 10 tỷ</option>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="search_button_container" >
                                        <input type="submit" name="sumbit" value="Tìm kiếm">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        </div>
                    </section>
                </div>        
               
                <div class="control-bar mt-20" >
                     <section class="product-detail">                       
                         <div class="right-category">
                            <h5 style="text-align: center;">Danh mục</h5>
                            <ul style="padding-left: 3px">
                                <?php
                                $menu_lv_1 = DB::table('bds_dmtd')->where('parent_id', 0)->get();
                                ?>
                                @foreach($menu_lv_1 as $item_lv_1)                               
                                    <a href="{{url('product-category-parent',[$item_lv_1->alias])}}">{{$item_lv_1->name}}</a>
                                    <ul>
                                        <?php
                                        $menu_lv_2 = DB::table('bds_dmtd')->where('parent_id', $item_lv_1->id)->get();
                                        ?>
                                        @foreach($menu_lv_2 as $item_lv_2)
                                        <li class="left-sub">
                                            <a href="{{url('product-category',[$item_lv_2->alias,$item_lv_2->parent_id])}}">-- {{$item_lv_2->name}}</a>
                                        </li>
                                        @endforeach
                                    </ul>                              
                                @endforeach
                            </ul>
                        </div>
                    </section>               
                </div>
               <div class="control-bar mt-20">
                    <section class="product-detail">                       
                         <div class="right-category">
                            <h5 style="text-align: center;">Thông tin dự án</h5>
                            <ul class="list-group list-unstyled" style="margin-bottom: 15px;">
                                @foreach($duan as $item)
                                    <li><a href="{{url('tin-tuc',[$item->alias])}}">{{ $item->name }}</a></li>  
                                 @endforeach                      
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
        </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var url = "./api/province";
        $.getJSON(url,function(result){
            $.each(result, function(i, field){
                var id      = field.provinceid;
                var name    = field.name;
                var type    = field.type;
                $("#province").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });

        //Khi chọn tỉnh thành
        $("#province").change(function() {
            $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
            var idprovince = $("#province").val();
            var url = "./api/district/"+idprovince;
            $.getJSON(url,function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var id      = field.districtid;
                    var name    = field.name;
                    var type    = field.type;
                    $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
                });
            });
        });
    })
</script>
@endsection