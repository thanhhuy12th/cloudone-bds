 @extends('frontend.project')
 @section('content')
 <!-- Scroll to the selected  -->
 <div class="btn-srcoll-to" id="mybtnsrcoll">
     <span>
         <i class="fas fa-arrow-circle-left"></i>
     </span>
 </div>
 <section class="scroll-to-the-selected none">
     <div class="scroll-container">
         <ul class="main-scroll-container">
             <li class="sub-scroll-container"><i class="fas fa-globe "></i><a href="#tongquan">Tổng quan</a>
             </li>
             <li class="sub-scroll-container"><i class="fas fa-map-marker-alt "></i><a href="#vitridacdia">Vị trí</a>
             </li>
             <li class="sub-scroll-container"><i class="fas fa-map "></i><a href="#dt">Diện tích</a></li>
             <li class="sub-scroll-container"><i class="fas fa-dollar-sign "></i><a href="#giaban">Giá bán</a></li>
             <li class="sub-scroll-container"><i class="fas fa-layer-group "></i><a href="#matbang">Mặt bằng</a>
             </li>
             <li class="sub-scroll-container"><i class="fas fa-images "></i><a href="#hinh">Hình ảnh</a></li>
             <li class="sub-scroll-container"><i class="fas fa-phone-volume "></i><a href="#contact">Liên hệ</a>
             </li>
         </ul>
     </div>
 </section>

 <!-- Product Detail-->
 <section class="product-detail">
     <div class="contanier">

         <div class="alter-contanier">
             <div class="pro-content">

                 <h3>{!! $project_detail->title !!}</h3>

                 <div id="tongquan">
                     <p>{!! $project_detail->tongquan !!}</p>
                 </div>
                 <div id="vitridacdia">
                     <p>{!! $project_detail->vitridacdia !!}</p>
                 </div>
                 <div id="dt">
                     <p>{!! $project_detail->dt !!}</p>
                 </div>
                 <div id="giaban">
                     <p>{!! $project_detail->giaban !!}</p>
                 </div>
                 <div id="matbang">
                     <p>{!! $project_detail->matbang !!}</p>
                 </div>
                 <div id="hinh">
                     <p>{!! $project_detail->hinh !!}</p>
                 </div>
                 <div id="contact">
                     <p>{!! $project_detail->contact !!}</p>

                 </div>

             </div>


         </div>
     </div>
 </section>


 
 @endsection