@extends('frontend.khokienthuc') 
@section('content')
    <!-- Product Detail-->
 <section class="product-detail">
     <div class="contanier">
         <div class="alter-contanier">
             <div class="pro-content">

                 <h3>{!! $newsCategoryDetail->title !!}</h3>

                <p>{!! $newsCategoryDetail->content !!}</p>
            </div>

             @include('frontend.pages.contactUs')
        </div>
    </section>

@endsection