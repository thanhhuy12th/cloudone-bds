@extends('frontend.lienhe')
@section('content')
<!-- Home icon -->
<section class="home-icon">
    <div class="container">
        <div class="home-icon-containner">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="icon-image"><i class="fas fa-home"></i></div>
                    <div class="icon-title">Văn phòng</div>

                    <div class="icon-content">{!! $address->content !!}</div>

                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="icon-image"><i class="fas fa-phone-volume"></i></div>
                    <div class="icon-title">Liên hệ</div>
                    <div class="icon-content">

                        {!! $phonename1->phonename !!}  {!! $hotline1->content !!}<br>
                       {!! $phonename2->phonename !!}  {!! $hotline2->content !!}</div>

                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="icon-image"><i class="fas fa-envelope-open"></i></div>
                    <div class="icon-title">Hỗ trợ</div>
                    <div class="icon-content">Email: {!! $email->content !!}</div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Thông tin liên hệ -->
<section class="contact">
    <div class="container contact-containner">

@include('frontend.pages.contactUs')
    </div>
</section>
@endsection