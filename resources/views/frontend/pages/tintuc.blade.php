@extends('frontend.news') 
@section('content')
    <!-- New -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">          
                <section class="product-detail">
                    <div class="container-fluid">
                        <div class="alter-contanier">
                            @foreach($news as $tt)
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="alter-grid" id="thumbnail">
                                            <img class="portrait" src="{{asset('public/uploads/postings/'.$tt->image)}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 text-justify" >
                                        <div class="grid-date">
                                            <i class="fas fa-calendar-alt"></i>
                                            {{$tt->created_at}}
                                        </div>
                                        <div class="grid-title-tintuc">
                                            <a href="{{route('news-detail',[$tt->alias])}}"><p>{{$tt->title}}</p></a>
                                        </div>
                                        <div class="grid-content-tintuc">
                                            <p>{!! $tt->intro !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-3 pt-5"style="padding-right: 50px">
                <section class="product-detail">
                    <div class="right-category">
                        <h5>Căn hộ mới</h5>
                        <ul class="list-group list-unstyled block-right-menu">
                             @foreach($canho as $item)
                                <li><a href="{{route('can-ho-detail',[$item->alias])}}">{{$item->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                     <div class="right-category" style="padding-top: 20px">
                        <h5>Tin mới trong ngày</h5>
                        <ul class="list-group list-unstyled block-right-menu">
                            @foreach($tinmoi as $item)
                                <li><a href="{{route('news-detail',[$item->alias])}}">{{ $item->name }}</a></li>  
                             @endforeach                      
                        </ul>
                    </div>
                </section>
            </div>
        </div>
         <div class="row" >
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <ul class="pagination" style="padding-top: 8px">
                     @if( $news->currentPage() != 1)
                        <li class="page-item">
                          <a class="page-link" href="{!! $news->url($news->currentPage() - 1) !!}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                          </a>
                        </li>
                     @endif
                     @for($i=1; $i <= $news->lastPage(); $i= $i+1)
                        <li class="page-item {!! ($news->currentPage() == $i )? 'active': '' !!}">
                            <a class="page-link" href="{!! $news->url($i) !!}">{!! $i !!}</a></li>      
                    @endfor
                    @if($news->currentPage() != $news->lastPage())
                        <li class="page-item">
                          <a class="page-link" href="{!! $news->url($news->currentPage() + 1) !!}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                          </a>
                        </li>
                     @endif
                </ul>
 
            </div>
            <div class="col-md-4"></div>
        </div>
        
@endsection