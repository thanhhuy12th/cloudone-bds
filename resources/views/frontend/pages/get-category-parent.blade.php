@extends('frontend.postings')
@section('content')
<!-- Product -->

<div class="container-fluid">
        <div class="row ">
            <div class="col-md-9">          
                <section class="product-detail">
                    <div class="container-fluid">
                        <?php
                        //pagi
                            $result=get_category_parent($alias);
                            $row_per_page=8 ;
                            $rows=count($result);
                            if ($rows>$row_per_page) $page=ceil($rows/$row_per_page);
                            else $page=1;
                            if(isset($_GET['start']) && (int)$_GET['start'])
                                $start=$_GET['start'];
                            else
                                $start=0;
                            $result=get_category_parent_limit($alias,$start,$row_per_page);
                                //End pagi
                       ?>
                <div class="alter-contanier">
                    @foreach($result as $item)
                        @if($item->duyetbai ==1)
                            @if($item->status ==1)
                                @if($item->position ==1)                               
                                    <div class="col-md-12"> 
                                        <div class="row pb-2">
                                            @php 
                                                    $splittedstring=explode("||",$item->images);   
                                            @endphp
                                                @for($i =0; $i<1; $i++)
                                            <div class=" col-md-4 mt-2" style="height: 188px">
                                                <span class="badge-notify-tindang"><img src="{{asset('frontend/img/vip.gif')}}" ></span>
                                                <div class="alter-grid"  id="thumbnail">                                          
                                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}"  >
                                                </div>
                                            </div>
                                             @endfor
                                            <div class="col-md-8 pb-1"  style="padding-right: 30px">
                                                <div class="grid">
                                                    <div class="grid-title-tindang">
                                                        <a href="{{route('product-detail',[$item->alias])}}" style="color: #ff0023">
                                                            {!! mb_strtolower($item->title) !!}
                                                        </a>
                                                    </div>
                                                    <div class="grid-date-tindang" >
                                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                                         <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>    
                                                    </div>   
                                                    <div class="grid-content-tindang">
                                                        <p>{!! $item->intro !!}</p>
                                                    </div>
                                                </div>

                                                 <div class="gia">
                                                    @if($item->typegia == 1)
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif          
                                                   
                                                </div>
                                                <div class=" dien_tich">
                                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                                </div>
                                                <div class="vi_tri">
                                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @elseif($item->position == 2)
                                    <div class="col-md-12"> 
                                        <div class="row pb-2">
                                            @php 
                                                    $splittedstring=explode("||",$item->images);   
                                            @endphp
                                                @for($i =0; $i<1; $i++)
                                            <div class=" col-md-4 mt-2" style="height: 188px">
                                                <span class="badge-notify-tindang-hot"><img src="{{asset('frontend/img/hot.gif')}}" ></span>
                                                <div class="alter-grid"  id="thumbnail">                                          
                                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}"  >
                                                </div>
                                            </div>
                                             @endfor
                                            <div class="col-md-8 pb-1"  style="padding-right: 30px">
                                                <div class="grid">
                                                    <div class="grid-title-tindang">
                                                        <a href="{{route('product-detail',[$item->alias])}}" style="color: #ff0023">
                                                            {!! mb_strtolower($item->title) !!}
                                                        </a>
                                                    </div>
                                                    <div class="grid-date-tindang" >
                                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                                         <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>     
                                                    </div>   
                                                    <div class="grid-content-tindang">
                                                        <p>{!! $item->intro !!}</p>
                                                    </div>
                                                </div>

                                                 <div class="gia">
                                                    @if($item->typegia == 1)
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif          
                                                   
                                                </div>
                                                <div class=" dien_tich">
                                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                                </div>
                                                <div class="vi_tri">
                                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @elseif($item->position == 3)
                                    <div class="col-md-12"> 
                                        <div class="row pb-2">
                                            @php 
                                                    $splittedstring=explode("||",$item->images);   
                                            @endphp
                                                @for($i =0; $i<1; $i++)
                                            <div class=" col-md-4 mt-2" style="height: 188px">                                       
                                                <div class="alter-grid"  id="thumbnail">                                          
                                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}"  >
                                                </div>
                                            </div>
                                             @endfor
                                            <div class="col-md-8 pb-1"  style="padding-right: 30px">
                                                <div class="grid">
                                                    <div class="grid-title-tindang">
                                                        <a href="{{route('product-detail',[$item->alias])}}" style="color: #ff0023">
                                                            {!! mb_strtolower($item->title) !!}
                                                        </a>
                                                    </div>
                                                    <div class="grid-date-tindang" >
                                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                                          <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>   
                                                    </div>   
                                                    <div class="grid-content-tindang">
                                                        <p>{!! $item->intro !!}</p>
                                                    </div>
                                                </div>

                                                 <div class="gia">
                                                    @if($item->typegia == 1)
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif          
                                                   
                                                </div>
                                                <div class=" dien_tich">
                                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                                </div>
                                                <div class="vi_tri">
                                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @elseif($item->position == 4)
                                    <div class="col-md-12"> 
                                        <div class="row pb-2">
                                            @php 
                                                    $splittedstring=explode("||",$item->images);   
                                            @endphp
                                                @for($i =0; $i<1; $i++)
                                            <div class=" col-md-4 mt-2" style="height: 188px">                                        
                                                <div class="alter-grid"  id="thumbnail">                                          
                                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}"  >
                                                </div>
                                            </div>
                                             @endfor
                                            <div class="col-md-8 pb-1"  style="padding-right: 30px">
                                                <div class="grid">
                                                    <div class="grid-title-tindang">
                                                        <a href="{{route('product-detail',[$item->alias])}}" style="color: #ff0023">
                                                            {!! mb_strtolower($item->title) !!}
                                                        </a>
                                                    </div>
                                                    <div class="grid-date-tindang" >
                                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                                         <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>    
                                                    </div>   
                                                    <div class="grid-content-tindang">
                                                        <p>{!! $item->intro !!}</p>
                                                    </div>
                                                </div>

                                                 <div class="gia">
                                                    @if($item->typegia == 1)
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif          
                                                   
                                                </div>
                                                <div class=" dien_tich">
                                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                                </div>
                                                <div class="vi_tri">
                                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @else
                                    <div class="col-md-12"> 
                                        <div class="row pb-2">
                                            @php 
                                                    $splittedstring=explode("||",$item->images);   
                                            @endphp
                                                @for($i =0; $i<1; $i++)
                                            <div class=" col-md-4 mt-2" style="height: 188px">
                                                
                                                <div class="alter-grid"  id="thumbnail">
                                                  
                                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}"  >
                                                </div>
                                            </div>
                                             @endfor
                                            <div class="col-md-8 pb-1"  style="padding-right: 30px">
                                                <div class="grid">
                                                    <div class="grid-title-tindang">
                                                        <a href="{{route('product-detail',[$item->alias])}}">
                                                            {!! mb_strtolower($item->title) !!}
                                                        </a>
                                                    </div>
                                                    <div class="grid-date-tindang" >
                                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                                         <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>    
                                                    </div>   
                                                    <div class="grid-content-tindang">
                                                        <p>{!! $item->intro !!}</p>
                                                    </div>
                                                </div>

                                                 <div class="gia">
                                                    @if($item->typegia == 1)
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif          
                                                   
                                                </div>
                                                <div class=" dien_tich">
                                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                                </div>
                                                <div class="vi_tri">
                                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @endif
                            @else
                                <div class="col-md-12"> 
                                    <div class="row pb-2">
                                        @php 
                                                $splittedstring=explode("||",$item->images);   
                                        @endphp
                                            @for($i =0; $i<1; $i++)
                                        <div class=" col-md-4 mt-2" style="height: 188px">
                                            
                                            <div class="alter-grid"  id="thumbnail">
                                              
                                                <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}"  >
                                            </div>
                                        </div>
                                         @endfor
                                        <div class="col-md-8 pb-1"  style="padding-right: 30px">
                                            <div class="grid">
                                                <div class="grid-title-tindang">
                                                    <a href="{{route('product-detail',[$item->alias])}}">
                                                        {!! mb_strtolower($item->title) !!}
                                                    </a>
                                                </div>
                                                <div class="grid-date-tindang" >
                                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>     
                                                </div>   
                                                <div class="grid-content-tindang">
                                                    <p>{!! $item->intro !!}</p>
                                                </div>
                                            </div>

                                             <div class="gia">
                                                @if($item->typegia == 1)
                                                     <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                @else
                                                     <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                @endif          
                                               
                                            </div>
                                            <div class=" dien_tich">
                                                <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                            </div>
                                            <div class="vi_tri">
                                                <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            @endif
                        @endif
                    @endforeach
                    </div>
                </div>
            </section>
            <div class="row" style="text-align: center; margin-top: 25px; margin-bottom: 20px">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        @php
                            $page_cr=($start/$row_per_page)+1;
                            for($i=1;$i<=$page;$i++)
                            {
                                if ($page_cr!=$i) echo "<div style='margin-left: 1px;' class='btn-group'>"."<a href='".$alias."?start=".$row_per_page*($i-1)."'><button class='btn btn-info' style='background-color: #007bff'>$i</button></a>"."</div>";
                                else echo "<div style='margin-left: 1px;' class='btn-group'><button style='background-color: #fb4908ba; border-color: #fb4908ba' class='btn btn-info active'>".$i." "."</button></div>";
                            }
                        @endphp
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>

            <div class="col-md-3 pt-5" style="padding-right: 50px">
                <div class="control-bar">
                    <section class="product-detail">                       
                         <div class="right-category">
                            <h5 style="text-align: center;">Tìm kiếm</h5>
                            <div class="search-product">
                            <div class="container">
                              <form id="search" action="{{route('tim-kiem')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Danh mục--</p>
                                            </div>
                                            <div class="select-content">
                                               <select name="danhmuc">
                                                   @for($i = 0; $i < count($danhmuc); $i++)
                                                    <option value="{!!$danhmuc[$i]['id']!!}" {{ (old('category_id') == $danhmuc[$i]['name'])? 'selected': ''}}>{!!$danhmuc[$i]['name']!!}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Tỉnh Thành--</p>
                                            </div>
                                            <div class="select-content">
                                                <select  id="province"  name="province">
                                                    <option value="-1">--Chọn Tỉnh Thành--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Quận/Huyện--</p>
                                            </div>
                                            <div class="select-content">
                                                <select  id="district"  name="district">
                                                    <option value="-1">--Chọn Quận Huyện--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Khoảng diện tích--</p>
                                            </div>
                                            <div class="select-content">
                                                <select name="dientichmatbang">
                                                    <option value="-1">--Chọn diện tích--</option>
                                                    <option value="less-15">< 15 <sup>m²</sup></option>
                                                    <option value="15-25">15 <sup>m²</sup> - 25 <sup>m²</sup></option>
                                                    <option value="25-50">25 <sup>m²</sup> - 50 <sup>m²</sup></option>
                                                    <option value="50-100">50 <sup>m²</sup> - 100 <sup>m²</sup></option>
                                                    <option value="more-100">> 100 <sup>m²</sup></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Khoảng giá--</p>
                                            </div>
                                            <div class="select-content">
                                                <select name="price">
                                                    <option value="-1">--Chọn giá--</option>
                                                    <option value="less-1m">< 1 triệu</option>
                                                    <option value="1m-3m"> 1 triệu - 3 triệu</option>
                                                    <option value="3m-5m"> 3 triệu - 5 triệu</option>
                                                    <option value="5m-10m"> 5 triệu - 10 triệu</option>
                                                    <option value="10m-100m"> 10 triệu - 100 triệu</option>
                                                    <option value="100m-1b"> 100 triệu - 1 tỷ</option>
                                                    <option value="1b-10b"> 1 tỷ - 10 tỷ</option>
                                                    <option value="more-10b"> > 10 tỷ</option>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="search_button_container" >
                                        <input type="submit" name="sumbit" value="Tìm kiếm">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        </div>
                    </section>
                </div>        
               
                <div class="control-bar mt-20">
                     <section class="product-detail">                       
                         <div class="right-category">
                            <h5 style="text-align: center;">Danh mục</h5>
                            <ul class="block-right-menu">
                                <?php   
                                $menu_lv_1 = DB::table('bds_dmtd')->where('parent_id', 0)->get();
                                ?>
                                @foreach($menu_lv_1 as $item_lv_1)                               
                                    <a href="{{url('product-category-parent',[$item_lv_1->alias])}}">{{$item_lv_1->name}}</a>
                                    <ul>
                                        <?php
                                        $menu_lv_2 = DB::table('bds_dmtd')->where('parent_id', $item_lv_1->id)->get();
                                        ?>
                                        @foreach($menu_lv_2 as $item_lv_2)
                                        <li class="left-sub">
                                            <a href="{{url('product-category',[$item_lv_2->alias,$item_lv_2->parent_id])}}">-- {{$item_lv_2->name}}</a>
                                        </li>
                                        @endforeach
                                    </ul>                              
                                @endforeach
                            </ul>
                        </div>
                    </section>               
                </div>
               <div class="control-bar mt-20">  
                    <section class="product-detail">                       
                         <div class="right-category">
                            <h5 style="text-align: center;">Nhà đất mới</h5>
                            <ul class="list-group list-unstyled block-right-menu" style="margin-bottom: 15px;">
                                @foreach($nhadat as $item)
                                    <li><a href="{{url('product-detail',[$item->alias])}}">{{ $item->title }}</a></li>  
                                 @endforeach                      
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
        </div>
</div>   


<script type="text/javascript">
    $(document).ready(function(){
        var url = "../api/province";
        $.getJSON(url,function(result){
            $.each(result, function(i, field){
                var id      = field.provinceid;
                var name    = field.name;
                var type    = field.type;
                $("#province").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });

        //Khi chọn tỉnh thành
        $("#province").change(function() {
            $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
            var idprovince = $("#province").val();
            var url = "../api/district/"+idprovince;
            $.getJSON(url,function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var id      = field.districtid;
                    var name    = field.name;
                    var type    = field.type;
                    $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
                });
            });
        });
    })
</script>
@endsection