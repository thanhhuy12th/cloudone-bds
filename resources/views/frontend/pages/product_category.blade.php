@extends('frontend.postings')
@section('content')
<!-- Product -->
<section class="product">
    <div class="contanier">
        <div class="row">
            <div class="col-md-9">
               
                <div class="row pb-4">
                    <div class="col-lg-12 col-md-12">
                    @foreach($product_category as $item)
                    @if($item->position ==1)
                        <div class="row">
                             @php 
                                    $splittedstring=explode("||",$item->images);   
                            @endphp
                                @for($i =0; $i<1; $i++)
                            <div class=" col-md-4 mb-2" style="height: 190px">
                                <span class="badge-notify-tindang"><img src="{{asset('frontend/img/vip.gif')}}" ></span>
                                <div class="alter-grid"  id="thumbnail">
                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                </div>
                            </div>
                            @endfor
                            <div class="col-md-8" >
                                <div class="grid">
                                    <div class="grid-title-tindang">
                                        <a href="{{route('product-detail',[$item->alias])}}">
                                            <p style="color: #ff0023">{{ mb_strtolower($item->title) }}</p>
                                        </a>
                                    </div>
                                    <div class="grid-date-tindang">
                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                         <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>    
                                    </div>   
                                    <div class="grid-content-tindang">
                                        <p>{!! $item->intro !!}</p>
                                    </div>
                                </div>
                         
                                 <div class="gia">
                                    @if($item->typegia == 1)
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                    @else
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                    @endif          
                                   
                                </div>
                                <div class=" dien_tich">
                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                </div>
                                <div class="vi_tri">
                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                    @elseif($item->position ==2)
                    <div class="row">
                             @php 
                                    $splittedstring=explode("||",$item->images);   
                            @endphp
                                @for($i =0; $i<1; $i++)
                            <div class=" col-md-4 mb-2" style="height: 190px">
                                <span class="badge-notify-tindang-hot"><img src="{{asset('frontend/img/hot.gif')}}" ></span>
                                <div class="alter-grid"  id="thumbnail">
                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                </div>
                            </div>
                            @endfor
                            <div class="col-md-8" >
                                <div class="grid">
                                    <div class="grid-title-tindang">
                                        <a href="{{route('product-detail',[$item->alias])}}">
                                            <p style="color: #ff0023">{{ mb_strtolower($item->title) }}</p>
                                        </a>
                                    </div>
                                    <div class="grid-date-tindang">
                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                        <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>   
                                    </div>   
                                    <div class="grid-content-tindang">
                                        <p>{!! $item->intro !!}</p>
                                    </div>
                                </div>
                         
                                 <div class="gia">
                                    @if($item->typegia == 1)
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                    @else
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                    @endif          
                                   
                                </div>
                                <div class=" dien_tich">
                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                </div>
                                <div class="vi_tri">
                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                    @elseif($item->position ==3)
                    <div class="row">
                             @php 
                                    $splittedstring=explode("||",$item->images);   
                            @endphp
                                @for($i =0; $i<1; $i++)
                            <div class=" col-md-4 mb-2" style="height: 190px">
                                <div class="alter-grid"  id="thumbnail">
                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                </div>
                            </div>
                            @endfor
                            <div class="col-md-8" >
                                <div class="grid">
                                    <div class="grid-title-tindang">
                                        <a href="{{route('product-detail',[$item->alias])}}">
                                            <p style="color: #ff0023">{{ mb_strtolower($item->title) }}</p>
                                        </a>
                                    </div>
                                    <div class="grid-date-tindang">
                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                        <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>    
                                    </div>   
                                    <div class="grid-content-tindang">
                                        <p>{!! $item->intro !!}</p>
                                    </div>
                                </div>
                         
                                 <div class="gia">
                                    @if($item->typegia == 1)
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                    @else
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                    @endif          
                                   
                                </div>
                                <div class=" dien_tich">
                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                </div>
                                <div class="vi_tri">
                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                    @elseif($item->position ==4)
                    <div class="row">
                             @php 
                                    $splittedstring=explode("||",$item->images);   
                            @endphp
                                @for($i =0; $i<1; $i++)
                            <div class=" col-md-4 mb-2" style="height: 190px">
                                <div class="alter-grid"  id="thumbnail">
                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                </div>
                            </div>
                            @endfor
                            <div class="col-md-8" >
                                <div class="grid">
                                    <div class="grid-title-tindang">
                                        <a href="{{route('product-detail',[$item->alias])}}">
                                            <p style="color: #ff0023">{{ mb_strtolower($item->title) }}</p>
                                        </a>
                                    </div>
                                    <div class="grid-date-tindang">
                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                         <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>    
                                    </div>   
                                    <div class="grid-content-tindang">
                                        <p>{!! $item->intro !!}</p>
                                    </div>
                                </div>
                         
                                 <div class="gia">
                                    @if($item->typegia == 1)
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                    @else
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                    @endif          
                                   
                                </div>
                                <div class=" dien_tich">
                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                </div>
                                <div class="vi_tri">
                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                    @else
                    <div class="row">
                             @php 
                                    $splittedstring=explode("||",$item->images);   
                            @endphp
                                @for($i =0; $i<1; $i++)
                            <div class=" col-md-4 mb-2" style="height: 190px">
                                <div class="alter-grid"  id="thumbnail">
                                    <img class="portrait-img" src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                </div>
                            </div>
                            @endfor
                            <div class="col-md-8" >
                                <div class="grid">
                                    <div class="grid-title-tindang">
                                        <a href="{{route('product-detail',[$item->alias])}}">
                                            <p>{{ mb_strtolower($item->title) }}</p>
                                        </a>
                                    </div>
                                    <div class="grid-date-tindang">
                                        <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                         <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>    
                                    </div>   
                                    <div class="grid-content-tindang">
                                        <p>{!! $item->intro !!}</p>
                                    </div>
                                </div>
                         
                                 <div class="gia">
                                    @if($item->typegia == 1)
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                    @else
                                         <span style="color: #056189">Giá bán:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                    @endif          
                                   
                                </div>
                                <div class=" dien_tich">
                                    <span style="color: #056189"> Diện tích mặt bằng:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichmatbang."m²" !!} </span> </span>
                                </div>
                                <div class="vi_tri">
                                    <span style="color: #056189">Khu vực: </span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                    @endif
                    @endforeach
                    </div>      
                </div>
                <div class="row" >
                    <div class="col-md-4"></div>
                    <div class="col-md-4 pt-5">
                         <ul class="pagination" >
                             @if( $product_category->currentPage() != 1)
                                <li class="page-item" >
                                  <a class="page-link" href="{!! $product_category->url($product_category->currentPage() - 1) !!}" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                </li>
                             @endif
                             @for($i=1; $i <= $product_category->lastPage(); $i= $i+1)
                                <li class="page-item {!! ($product_category->currentPage() == $i )? 'active': '' !!}">
                                    <a class="page-link" href="{!! $product_category->url($i) !!}">{!! $i !!}</a></li>      
                            @endfor
                            @if($product_category->currentPage() != $product_category->lastPage())
                                <li class="page-item">
                                  <a class="page-link" href="{!! $product_category->url($product_category->currentPage() + 1) !!}" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </li>
                             @endif
                          </ul>
                    </div>
                    <div class="col-md-4"></div>
                </div>  
            </div>

            <div class="col-md-3" >
                <div class="control-bar">
                    <p class="p-title">Tìm kiếm</p>
                    <div class="search-product">
                        <div class="container">
                              <form id="search" action="{{route('tim-kiem')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Danh mục--</p>
                                            </div>
                                            <div class="select-content">
                                               <select name="danhmuc">
                                                   @for($i = 0; $i < count($danhmuc); $i++)
                                                    <option value="{!!$danhmuc[$i]['id']!!}" {{ (old('category_id') == $danhmuc[$i]['name'])? 'selected': ''}}>{!!$danhmuc[$i]['name']!!}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Tỉnh Thành--</p>
                                            </div>
                                            <div class="select-content">
                                                <select  id="province"  name="province">
                                                    <option value="-1">--Chọn Tỉnh Thành--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Quận/Huyện--</p>
                                            </div>
                                            <div class="select-content">
                                                <select  id="district"  name="district">
                                                    <option value="-1">--Chọn Quận Huyện--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Khoảng diện tích--</p>
                                            </div>
                                            <div class="select-content">
                                                <select name="dientichmatbang">
                                                    <option value="-1">--Chọn diện tích--</option>
                                                    <option value="less-15">< 15 <sup>m²</sup></option>
                                                    <option value="15-25">15 <sup>m²</sup> - 25<sup>m²</sup></option>
                                                    <option value="25-50">25 <sup>m²</sup> - 50<sup>m²</sup></option>
                                                    <option value="50-100">50 <sup>m²</sup> - 100<sup>m²</sup></option>
                                                    <option value="more-100">> 100 <sup>m²</sup></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Khoảng giá--</p>
                                            </div>
                                            <div class="select-content">
                                                <select name="price">
                                                    <option value="-1">--Chọn giá--</option>
                                                    <option value="less-1m">< 1 triệu</option>
                                                    <option value="1m-3m"> 1 triệu - 3 triệu</option>
                                                    <option value="3m-5m"> 3 triệu - 5 triệu</option>
                                                    <option value="5m-10m"> 5 triệu - 10 triệu</option>
                                                    <option value="10m-100m"> 10 triệu - 100 triệu</option>
                                                    <option value="100m-1b"> 100 triệu - 1 tỷ</option>
                                                    <option value="1b-10b"> 1 tỷ - 10 tỷ</option>
                                                    <option value="more-10b"> > 10 tỷ</option>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="search_button_container" >
                                        <input type="submit" name="sumbit" value="Tìm kiếm">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="control-bar mt-5">
                    <section class="product-detail">                       
                         <div class="right-category">
                            <h5 style="text-align: center;">Danh mục</h5>
                            <ul class="block-right-menu">
                                <?php
                                $menu_lv_1 = DB::table('bds_dmtd')->where('parent_id', 0)->get();
                                ?>
                                @foreach($menu_lv_1 as $item_lv_1)
                                <li>
                                    <a href="{{url('product-category',[$item_lv_1->alias])}}">{{$item_lv_1->name}}</a>
                                    <ul>
                                        <?php
                                        $menu_lv_2 = DB::table('bds_dmtd')->where('parent_id', $item_lv_1->id)->get();
                                        ?>
                                        @foreach($menu_lv_2 as $item_lv_2)
                                        <li class="left-sub">
                                            <a href="{{url('product-category',[$item_lv_2->alias,$item_lv_2->parent_id])}}">-- {{$item_lv_2->name}}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </section>                    
                </div>           
                 
                <div class="control-bar mt-20 mb-20">                   
                   <section class="product-detail">                       
                         <div class="right-category">
                            <h5 style="text-align: center;">Nhà đất mới</h5>
                            <ul class="list-group list-unstyled block-right-menu" style="margin-bottom: -50px;">
                                @foreach($nhadat as $item)
                                    <li><a href="{{route('product-detail',[$item->alias])}}">{{$item->title}}</a></li>
                                @endforeach                
                            </ul>
                        </div>
                    </section>
                </div>

            </div>
        </div>
        
    </div>
    
</section>
<script type="text/javascript">
    $(document).ready(function(){
        var url = "../../api/province";
        $.getJSON(url,function(result){
            $.each(result, function(i, field){
                var id      = field.provinceid;
                var name    = field.name;
                var type    = field.type;
                $("#province").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });

        //Khi chọn tỉnh thành
        $("#province").change(function() {
            $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
            var idprovince = $("#province").val();
            var url = "../../api/district/"+idprovince;
            $.getJSON(url,function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var id      = field.districtid;
                    var name    = field.name;
                    var type    = field.type;
                    $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
                });
            });
        });
    })
</script>
@endsection