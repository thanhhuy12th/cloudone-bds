@extends('frontend.postings') 
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/slider/dist/css/slider-pro.min.css')}}" media="screen"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/slider/libs/fancybox/jquery.fancybox.css')}}" media="screen"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/slider/css/examples.css')}}" media="screen"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="{{asset('frontend/slider/libs/jquery-1.11.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/slider/dist/js/jquery.sliderPro.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/slider/libs/fancybox/jquery.fancybox.pack.js')}}"></script>
<script type="text/javascript">
    $( document ).ready(function( $ ) {
      $( '#example2' ).sliderPro({
        width: 300,
        height: 300,
        visibleSize: '100%',
        forceSize: 'Width',
        autoSlideSize: true
      }); 

      $( ".slider-pro" ).each(function(){
        var slider = $( this );
    
        slider.find( ".sp-image" ).parent( "a" ).on( "click", function( event ) {
          event.preventDefault();
    
          if ( slider.hasClass( "sp-swiping" ) === false ) {
            var sliderInstance = slider.data( "sliderPro" ),
            isAutoplay = sliderInstance.settings.autoplay;
    
            $.fancybox.open( slider.find( ".sp-image" ).parent( "a" ), {
              index: $( this ).parents( ".sp-slide" ).index(),
              afterShow: function() {
                if ( isAutoplay === true ) {
                  sliderInstance.settings.autoplay = false;
                  sliderInstance.stopAutoplay();
                }
              },
              afterClose: function() {
                if ( isAutoplay === true ) {
                  sliderInstance.settings.autoplay = true;
                  sliderInstance.startAutoplay();
                }
              }
    
            });
          }
        });
      });

    });
</script>
<style type="text/css">
    iframe {
        width: 100%
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">          
            <section class="product-detail">
                <div class="container-fluid">
                    <div class="alter-contanier">
                        <h4>{!! $canho_detail->title !!}</h4>
                        <div id="tongquan" >
                            <table class="table bds-detail">
                                <tbody>
                                    <tr>
                                        <td><span class="green">Khu: </span> {!! $canho_detail->khuvuc !!}</td>
                                        <td><span class="blue">Mã tòa: </span>{!! $canho_detail->maToa !!}</td>
                                    </tr>
                                    <tr>
                                        <td><span class="green">Mã căn: </span>{!! $canho_detail->maCan !!}</td>
                                        <td><span class="blue">DT (m2): </span>{!! $canho_detail->dientich !!}</td>
                                    </tr>
                                    <tr>
                                        <td><span class="green">Giá bán: </span> <span class="red">{!!  number_format($canho_detail->priceCDT,0,",",",") . " VND " !!} </span></td>
                                        <td><span class="blue">Chênh lệch: </span><span class="red">{!!  number_format($canho_detail->priceChenh,0,",",",") . " VND " !!}</span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="green">Hướng View: </span><span class="red">
                                                @if( $canho_detail->huong_view ==1 )
                                                    Nam
                                                @elseif($canho_detail->huong_view ==2)
                                                    Tây Nam
                                                @elseif($canho_detail->huong_view ==3)
                                                    Tây
                                                @elseif($canho_detail->huong_view ==4)
                                                    Tây Bắc
                                                @elseif($canho_detail->huong_view ==5)
                                                    Bắc
                                                @elseif($canho_detail->huong_view ==6)
                                                    Đông Bắc
                                                @elseif($canho_detail->huong_view ==7)
                                                    Đông
                                                @elseif($canho_detail->huong_view ==8)
                                                    Đông Nam
                                                @endif                                              
                                            </span>
                                        </td>
                                        <td>
                                            <span class="blue">Hướng ban công: </span><span class="red">
                                                 @if( $canho_detail->huong_view ==1 )
                                                    Ban công hướng Nam
                                                @elseif($canho_detail->huong_view ==2)
                                                    Ban công hướng Tây Nam
                                                @elseif($canho_detail->huong_view ==3)
                                                    Ban công hướng Tây
                                                @elseif($canho_detail->huong_view ==4)
                                                    Ban công hướng Tây Bắc
                                                @elseif($canho_detail->huong_view ==5)
                                                    Ban công hướng Bắc
                                                @elseif($canho_detail->huong_view ==6)
                                                   Ban công hướng Đông Bắc
                                                @elseif($canho_detail->huong_view ==7)
                                                   Ban công hướng Đông
                                                @elseif($canho_detail->huong_view ==8)
                                                    Ban công hướng Đông Nam
                                                @endif                                              
                                            </span>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td><span class="blue">Link dự án: </span> <a href="{!! ($canho_detail->link) !!}" target="_blank"> {!! ($canho_detail->link) !!}</a></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="chitiet">
                            <h4>Thông tin chi tiết</h4>
                            <div class="item-detail">
                                <div class="pm-desc" >
                                       <input type="hidden" id="tongquanbreak" value="{!! $canho_detail->content !!}">
                                        <p id="output"></p>                                    
                                </div>
                            </div>
                        </div>
                        <div id="contact">
                            <h4 class="mb-10">Liên hệ:</h4>
                             @php 
                                    $contact=explode("||",$canho_detail->contact); 
                                @endphp 
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <b>Họ tên:</b>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                   {!! $contact[0] !!}
                                </div>
                            </div>                            
                            <div class="row pt-10">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <b>Email:</b>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    {!! $contact[1] !!}
                                </div>
                            </div>
                            <div class="row pt-10">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <b>Điện thoại:</b>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                   {!! $contact[2] !!}
                                </div>
                            </div>
                            <p></p>
                            <p></p>
                            <p><i>Lưu ý: Quý vị đang xem nội dung tin rao ''{!! $canho_detail->title !!}''. Mọi thông tin liên quan tới tin rao này là do người đăng tin đăng tải và chịu trách nhiệm. Chúng tôi luôn cố gắng để có chất lượng thông tin tốt nhất, nhưng chúng tôi không đảm bảo và không chịu trách nhiệm về bất kỳ nội dung nào liên quan tới tin rao này. Nếu quý vị phát hiện có sai sót hay vấn đề gì xin hãy thông báo cho chúng tôi.</i></p>
                        </div>
                        <div id="hinh" class="pt-20">
                            <h4>Thư viện</h4>
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Xem ảnh</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Xem bản đồ</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <div id="example2" class="slider-pro">
                                        <div class="sp-slides">
                                             @php 
                                                    $splittedstring=explode("||",$canho_detail->images); 
                                                @endphp
                                                @for($i =0; $i<count($splittedstring)-1; $i++)
                                            <div class="sp-slide">
                                              <a href="{{asset('uploads/postings/'.$splittedstring[$i])}}">
                                                        <img class="sp-image" src="{{asset('public/uploads/postings/'.$splittedstring[$i])}}"  
                                                        data-src="{{asset('public/uploads/postings/'.$splittedstring[$i])}}" 
                                                        data-retina="{{asset('public/uploads/postings/'.$splittedstring[$i])}}" />
                                                </a>
                                            </div>
                                             @endfor
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" style="text-align: center">
                                   {!! $canho_detail->googlemap !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-3 pt-20" style="padding-right: 50px">
            <section class="product-detail">                    
                <div class="right-category">
                    <h5>Căn hộ nổi bật</h5>
                    <ul class="list-group list-unstyled block-right-menu">
                        @foreach($canho_hot as $hot)
                            <li><a href="{{route('can-ho-detail',[$hot->alias])}}">{{$hot->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
                  <div class="right-category" style="padding-top: 20px">
                    <h5>Nhà đất nổi bật</h5>
                    <ul class="list-group list-unstyled block-right-menu">
                        @foreach($nhadat as $item)
                            <li><a href="{{url('product-detail',[$item->alias])}}">{{ $item->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </section>            
        </div>
    </div>

    <hr>
    
   <section class="project">
        <div class="container">    
            <div class="m-title-canho" >
                <p >Căn hộ tương tự</p>
            </div>
            <div class="row">
                @foreach($canhonoibat as $item)
                    @if($item->position ==1 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                 @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                <span class="badge-notify"><img src="{{asset('frontend/img/vip.gif')}}" alt=""></span>
                                 @for($i =0; $i<1; $i++)
                                   <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                @endfor
                                </div>
                                <div class="grid-title">
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                 <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                  
                                </div>   
                                <div class="grid-content-canho">
                                        <table class="table bds-item">
                                            <tr>
                                                <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                                
                                                <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                               
                                            </tr>
                                            </tr>
                                            <tr>
                                                <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                               
                                                <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                                
                                            </tr>
                                            <tr>
                                                <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000)  !!} tr</span></td>
                                              
                                                <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>                                         
                                            </tr>                                        
                                        </table>
                                </div>
                            </div>
                        </div>
                    @elseif($item->position ==2 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                 @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                <span class="badge-notify-hot"><img src="{{asset('frontend/img/hot.gif')}}" alt=""></span>
                                 @for($i =0; $i<1; $i++)
                                   <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                @endfor
                                </div>
                                <div class="grid-title">
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                 <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                   
                                </div>   
                                <div class="grid-content-canho">
                                        <table class="table bds-item">
                                            <tr>
                                                <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                                
                                                <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                               
                                            </tr>
                                            </tr>
                                            <tr>
                                                <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                               
                                                <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                                
                                            </tr>
                                            <tr>
                                                <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000)  !!} tr</span></td>
                                              
                                                <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>                                         
                                            </tr>                                        
                                        </table>
                                </div>
                            </div>
                        </div>
                    @elseif($item->position ==3 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                 @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                 @for($i =0; $i<1; $i++)
                                   <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                @endfor
                                </div>
                                <div class="grid-title">
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                 <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                   
                                </div>   
                                <div class="grid-content-canho">
                                        <table class="table bds-item">
                                            <tr>
                                                <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                                
                                                <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                               
                                            </tr>
                                            </tr>
                                            <tr>
                                                <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                               
                                                <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                                
                                            </tr>
                                            <tr>
                                                <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000)  !!} tr</span></td>
                                              
                                                <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>                                         
                                            </tr>                                        
                                        </table>
                                </div>
                            </div>
                        </div>
                    @elseif($item->position ==4 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                 @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                 @for($i =0; $i<1; $i++)
                                   <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                @endfor
                                </div>
                                <div class="grid-title">
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                 <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                   
                                </div>   
                                <div class="grid-content-canho">
                                        <table class="table bds-item">
                                            <tr>
                                                <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                                
                                                <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                               
                                            </tr>
                                            </tr>
                                            <tr>
                                                <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                               
                                                <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                                
                                            </tr>
                                            <tr>
                                                <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000)  !!} tr</span></td>
                                              
                                                <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>                                         
                                            </tr>                                        
                                        </table>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                 @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                 @for($i =0; $i<1; $i++)
                                   <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                @endfor
                                </div>
                                <div class="grid-title">
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <p>{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                 <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                   
                                </div>   
                                <div class="grid-content-canho">
                                        <table class="table bds-item">
                                            <tr>
                                                <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                                
                                                <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                               
                                            </tr>
                                            </tr>
                                            <tr>
                                                <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                               
                                                <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                                
                                            </tr>
                                            <tr>
                                                <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000)  !!} tr</span></td>
                                              
                                                <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>                                         
                                            </tr>                                        
                                        </table>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach 
            </div>
        </div>
    </section>
  <hr>
    
   <section class="project">
        <div class="container">       
            <div class="m-title-canho">
                <p>Nhà đất nổi bật</p>
            </div>        
            <div class="row">
                @foreach($nhadatnoibat as $item)
                     @if($item->position ==1 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp
                                   <span class="badge-notify"><img src="{{asset('frontend/img/vip.gif')}}" alt=""></span>
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                  
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>                                        
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td>  
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>                                  
                                    </table>
                                </div>
                            </div>
                        </div>
                     @elseif($item->position ==2 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp
                                   <span class="badge-notify" style="width: 100px;"><img src="{{asset('frontend/img/hot.gif')}}" alt=""></span>
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                  
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>                                        
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td>  
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>                                  
                                    </table>
                                </div>
                            </div>
                        </div>
                     @elseif($item->position ==3 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                    
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>                                        
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td>  
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>                                  
                                    </table>
                                </div>
                            </div>
                        </div>
                     @elseif($item->position ==4 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                    
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>                                        
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td>  
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>                                  
                                    </table>
                                </div>
                            </div>
                        </div>
                     @else
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p>{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                     <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>                                   
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>                                        
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td>  
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>                                  
                                    </table>
                                </div>
                            </div>
                        </div>
                     @endif
                @endforeach
            </div>       
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds
 
      items : 4,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
 
  });
 
});

    let textareaText = $('#tongquanbreak').val();
    textareaText = textareaText.replace(/\r?\n/g, '<br />');
    $('#output').html(textareaText);
    
</script>
@endsection