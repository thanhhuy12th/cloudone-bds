 @extends('frontend.news')
 @section('content')
 <!-- Product Detail-->
 <div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
           
            <section class="product-detail">
                <div class="container-fluid">
                    <div class="alter-contanier">
                        <h4>{!! $chitiettin->title !!}</h4>
                            
                       	<p>{!! $chitiettin->content !!}</p>

                        <div class="col-md-9">
                            <div class="contact-company" id="lienhe">
                                @if(Session::has('success'))
                                <div class="alert alert-success" id="flash-message">
                                    {{ Session::get('success') }}
                                </div>
                                @endif
                                <div class="contact-company-title">
                                    <span>Liên hệ ngay với chúng tôi</span>
                                </div>
                                <form action="{{route('lien-he.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group fullname">
                                        <label>Name</label>
                                        <input type="text" class="form-control" name="name" id="contact-company-name" placeholder="Nhập Tên" value="{{old('name')}}" required="">
                                        <div class="alert alert-warning">
                                            <strong>Tên đăng ký của bạn đã vượt quá 50 ký tự</strong>
                                        </div>
                                    </div>
                                     <div class="form-group fullname">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" name="phone" id="contact-company-name" placeholder="Số điện thoại" value="{{old('phone')}}" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Địa chỉ email</label>
                                        <input type="email" class="form-control" name="email" id="contact-company-email" aria-describedby="emailHelp" placeholder="john@example.com" value="{{old('email')}}" required="">
                                    </div>

                                    <div class="form-group mess">
                                        <label for="message">Message</label>
                                        <textarea type="text" class="form-control luna-message" id="message" placeholder="Gửi nội dung" name="content" required="">{{old('content')}}</textarea>
                                        <div class="alert alert-warning">
                                            <strong>Nội dung của bạn đã vượt quá 200 ký tự</strong>
                                        </div>
                                        <div class="alert alert-info">
                                            <strong>Có ký tự đặc biệt</strong>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="g-recaptcha" data-sitekey="6LewA7oUAAAAANLI8MKhRUWB8kagUy4w8UC_lnNq"></div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Gửi thông tin</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-3 pt-5" style="margin-left: -40px">
            <section class="product-detail">
                <div class="right-category">
                    <h5>Căn hộ mới</h5>
                    <ul class="list-group list-unstyled block-right-menu">
                         @foreach($canho as $item)
                            <li><a href="{{route('can-ho-detail',[$item->alias])}}">{{$item->title}}</a></li>
                        @endforeach
                    </ul>
                </div>                
                <div class="right-category" style="padding-top: 20px">
                    <h5>Tin mới trong ngày</h5>
                    <ul class="list-group list-unstyled block-right-menu">
                         @foreach($tinmoi as $item)
                            <li><a href="{{route('news-detail',[$item->alias])}}">{{ $item->name }}</a></li>
                        @endforeach         
                    </ul>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection
