@extends('frontend.khokienthuc') 
@section('content')
     <section class="product">
        <div class="contanier">
            <div class="row">
                <div class="col-md-9 col-lg-8">
                    @foreach($news_category as $item)
                    <div class="row pb-4">
                        <div class="col-lg-4 col-md-4">
                            <div class="grid-image">
                                <img src="{{asset('uploads/postings/'.$item->image)}}">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="grid-date">
                                <i class="fas fa-calendar-alt"></i>
                                {{$item->created_at}}
                            </div>
                            <div class="grid-title">
                                <a href="{{route('news-detail',[$item->alias])}}"><p>{{$item->title}} </p></a>
                            </div>
                            <div class="grid-content">
                                <p>{{ $item->intro }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    


                    <ul class="pagination">
                        @if( $news_category->currentPage() != 1)
                        <li class="page-item"><a class="page-link" href="{!! $news_category->url($news_category->currentPage() - 1) !!}">Previous</a></li>
                        @endif
                        @for($i=1; $i <= $news_category->lastPage(); $i= $i+1)
                        <li class="page-item {!! ($news_category->currentPage() == $i )? 'active': '' !!}">
                            <a class="page-link" href="{!! $news_category->url($i) !!}">{!! $i !!}</a>
                        </li>
                       @endfor
                       @if($news_category->currentPage() != $news_category->lastPage())
                        <li class="page-item"><a class="page-link" href="{!! $news_category->url($news_category->currentPage() + 1) !!}">Next</a></li>
                        @endif
                      </ul>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="control-bar">
                        <p class="p-title">Tin Mới Nhất</p>
                        <ul>
                            @foreach($newsnews as $item)
                            <li class="pb-3 pr-2">
                                <a href="{{route('news-detail',[$item->alias])}}">{{$item->title}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="control-bar mt-20">
                    <p class="p-title">Bài viết liên quan</p>
                    <div class="h-title">
                        @foreach($product_category as $item)
                        <p><a href="{{route('product-detail',[$item->id,$item->alias])}}">{{$item->title}}</a>
                        </p>
                        @endforeach
                    </div>
                </div>
                </div>

            </div>
        </div>
    </section>

@endsection