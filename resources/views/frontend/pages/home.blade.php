@extends('frontend.index')
@section('content')

<!-- Dự án nổi bật -->
<section class="project">
    <div class="container">
        <div class="sec-title">
            <div class="m-title">
                <p>@yield('title','Căn hộ nổi bật')</p>
            </div>
        </div>
        <!-- FORM SEARCH -->
        <form method="POST" action="{{route('search-can-ho')}}">
            @csrf
            <div class="row search">

                <div class="col-md-2">
                    <?php
                        $dmcanho_all = DB::table('bds_dmcanho')->get();
                    ?>               
                    <select class="form-control" name="dmcanho">
                        <option value="0">-- Chọn dự án --</option>
                        @foreach($dmcanho_all as $dm_all)
                            <option value="{{$dm_all->id}}">{{$dm_all->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <input type="text" name="khuvuc" placeholder="Nhập khu vực" class="form-control">
                </div>
                <div class="col-md-2" onclick="showModalDienTich(this)" >
                    <input type="text" name="dientich" id="dientich"  placeholder="Click chọn diện tích" value="" class="form-control" >
                    <input type="hidden" name="dientichhidden" id="dientichhidden"  placeholder="Click chọn diện tích" value="" class="form-control">
                </div>
                <div class="col-md-3" onclick="showModalGia(this)">
                    <input type="text" name="priceCDT"  id="gia" placeholder="Click chọn giá" class="form-control" >
                    <input type="hidden" name="priceCDThidden"  id="giahidden" placeholder="Click chọn giá" class="form-control">
                </div>
                <div class="col-md-2">                 
                    <select class="form-control" name="huong_view">
                        <option value="0">-- Chọn hướng --</option>
                        <option value="1">Nam</option>
                        <option value="2">Tây Nam</option>
                        <option value="3">Tây</option>
                        <option value="4">Tây Bắc</option>
                        <option value="5">Bắc</option>
                        <option value="6">Đông Bắc</option>
                        <option value="7">Đông</option>
                        <option value="8">Đông Nam</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <button class="btn btn-primary" name="btnSearch" type="submit"><i class="fa fa-search"></i></button>
                </div>

            </div>
        </form>
        <!-- END FORM SEARCH -->

        <div class="row">
            @foreach($get_canho as $item)              
                @if($item->position ==1 )
                    <div class="col-lg-3 pd-lr-5 mt-10">
                        <div class="border-box border-news">                        
                            <div class="grid-image" >                                                  
                                @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                <span class="badge-notify"><img src="{{asset('frontend/img/vip.gif')}}" alt=""></span>
                                @for($i =0; $i<count($splittedstring)-1; $i++)
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                    </a>
                                    @endfor                               
                            </div>
                            <div class="grid-title-canho pt-20">
                                <a href="{{route('can-ho-detail',[$item->alias])}}">
                                    <p style="color: #ff0023">{!! $item->title !!}</p>
                                </a>
                            </div>
                            <div class="grid-date">
                                <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->updated_at))}} </i>

                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>     
                            </div>

                                
                            <div class="grid-content-canho">
                                <table class="table bds-item">
                                    <tr>
                                        <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                        
                                        <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                        
                                    </tr>
                                    </tr>
                                    <tr>
                                        <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                        
                                        <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                        
                                    </tr>
                                    <tr>
                                        <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000)  !!} tr</span></td>
                                        
                                        <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>
                                        
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                @elseif ($item->position == 2)
                    <div class="col-lg-3 pd-lr-5 mt-10">
                        <div class="border-box border-news">                        
                            <div class="grid-image">

                                @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                <span class="badge-notify-hot"><img src="{{asset('frontend/img/hot.gif')}}" alt=""></span>
                                @for($i =0; $i<count($splittedstring)-1; $i++)
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                    </a>
                                    @endfor                               
                            </div>
                            <div class="grid-title-canho pt-20">
                                <a href="{{route('can-ho-detail',[$item->alias])}}">
                                    <p style="color: #ff0023">{!! $item->title !!}</p>
                                </a>
                            </div>
                            <div class="grid-date">
                                <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->updated_at))}} </i>
                                <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>  
                            </div>
                            <div class="grid-content-canho">
                                <table class="table bds-item">
                                    <tr>
                                        <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                        
                                        <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                        
                                    </tr>
                                    </tr>
                                    <tr>
                                        <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                        
                                        <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                        
                                    </tr>
                                    <tr>
                                        <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000)  !!} tr</span></td>
                                        
                                        <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>
                                        
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                @elseif ($item->position == 3)
                    <div class="col-lg-3 pd-lr-5 mt-10">
                        <div class="border-box border-news">                        
                            <div class="grid-image">                                                  
                                @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                <span class="badge-notify"></span>
                                @for($i =0; $i<count($splittedstring)-1; $i++)
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                    </a>
                                    @endfor                               
                            </div>
                            <div class="grid-title-canho pt-20">
                                <a href="{{route('can-ho-detail',[$item->alias])}}">
                                    <p style="color: #ff0023">{!! $item->title !!}</p>
                                </a>
                            </div>
                            <div class="grid-date">
                                <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->updated_at))}} </i>
                                <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>  
                            </div>
                            <div class="grid-content-canho">
                                <table class="table bds-item">
                                    <tr>
                                        <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                        
                                        <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                        
                                    </tr>
                                    </tr>
                                    <tr>
                                        <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                        
                                        <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                        
                                    </tr>
                                    <tr>
                                        <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000) !!} tr</span></td>
                                        
                                        <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>
                                        
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                @elseif ($item->position == 4)
                    <div class="col-lg-3 pd-lr-5 mt-10">
                        <div class="border-box border-news">                        
                            <div class="grid-image">                                                  
                                @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                <span class="badge-notify"></span>
                                @for($i =0; $i<count($splittedstring)-1; $i++)
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                    </a>
                                    @endfor                               
                            </div>
                            <div class="grid-title-canho pt-20">
                                <a href="{{route('can-ho-detail',[$item->alias])}}">
                                    <p style="color: #ff0023">{!! $item->title !!}</p>
                                </a>
                            </div>
                            <div class="grid-date">
                                <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->updated_at))}} </i>
                                <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>

                            </div>
                            <div class="grid-content-canho">
                                <table class="table bds-item">
                                    <tr>
                                        <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                        
                                        <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                        
                                    </tr>
                                    </tr>
                                    <tr>
                                        <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                        
                                        <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                        
                                    </tr>
                                    <tr>
                                        <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000) !!} tr</span></td>
                                        
                                        <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>
                                        
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-lg-3 pd-lr-5 mt-10">
                        <div class="border-box border-news">                        
                            <div class="grid-image">                                                  
                                @php 
                                    $splittedstring=explode("||",$item->images);   
                                @endphp
                                <span class="badge-notify"></span>
                                @for($i =0; $i<count($splittedstring)-1; $i++)
                                    <a href="{{route('can-ho-detail',[$item->alias])}}">
                                        <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}" >
                                    </a>
                                    @endfor                               
                            </div>
                            <div class="grid-title-canho pt-20">
                                <a href="{{route('can-ho-detail',[$item->alias])}}">
                                    <p >{!! $item->title !!}</p>
                                </a>
                            </div>
                            <div class="grid-date">
                                <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->updated_at))}} </i>
                                <!-- <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>   -->
                            </div>
                            <div class="grid-content-canho">
                                <table class="table bds-item">
                                    <tr>
                                        <td><span class="green">Khu:</span> {!! $item->khuvuc !!}</td>
                                        
                                        <td><span class="blue">Mã tòa:</span>  {!! $item->maToa !!}</td>
                                        
                                    </tr>
                                    </tr>
                                    <tr>
                                        <td><span class="green">Mã căn: </span>{!! $item->maCan !!}</td>
                                        
                                        <td><span class="blue">DT (m2): </span> {!! $item->dientich !!}</td>
                                        
                                    </tr>
                                    <tr>
                                        <td><span class="green">Giá bán: </span> <span class="red">{!! str_replace ( ".", ",", $item->priceCDT/1000000) !!} tr</span></td>
                                        
                                        <td><span class="blue">Chênh lệch: </span><span class="red">{!! str_replace( ".", ",", $item->priceChenh/1000000) !!} tr</span></td>
                                        
                                    </tr>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                @endif                     
            @endforeach           
        </div>
        <div class="row">
            <div class="search_button_container">
                <a href="{{route('view-more-can-ho')}}">
                    <button type="button" class="btn btn-primary" style="border-radius: 30px">Xem thêm</button>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- Nhà đất nổi bật -->
 <section class="project-nhadat">
        <div class="container" style="margin-top: -40px">
        <div class="sec-title-nhadat">
            <div class="m-title">
                <p>@yield('title','Nhà đất nổi bật')</p>
            </div>
        </div>        
            <div class="row">
                @foreach($nhadatnoibat as $item)
                    @if($item->position ==1 )
                        <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp
                                    <span class="badge-notify"><img src="{{asset('frontend/img/vip.gif')}}" alt=""></span>
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>
                                        
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @elseif($item->position ==2)
                    <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp
                                   <span class="badge-notify-hot"><img src="{{asset('frontend/img/hot.gif')}}" alt=""></span>
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>
                                        
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @elseif($item->position ==3)
                    <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp                                    
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>
                                        
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @elseif($item->position ==4)
                    <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp                                   
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p style="color: #ff0023">{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>
                                        
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @else
                    <div class="col-lg-3 pd-lr-5 mt-10">
                            <div class="border-box border-news">
                                <div class="grid-image">
                                    @php 
                                        $splittedstring=explode("||",$item->images);   
                                    @endphp                                  
                                    @for($i =0; $i<1; $i++)
                                       <img src="{{ asset('public/uploads/postings/'.$splittedstring[0])}}">
                                    @endfor
                                </div>
                                <div class="grid-title pt-20">
                                    <a href="{{route('product-detail',[$item->alias])}}">
                                        <p>{!! mb_strtolower($item->title) !!}</p>
                                    </a>
                                </div>
                                <div class="grid-date">
                                    <i class="far fa-calendar-alt"> {{ date("d/m/Y",strtotime($item->created_at))}} </i>
                                    <i class="far fa-calendar-alt" style="padding-left: 80px;"> Ngày hết hạn: {{ date("d/m/Y",strtotime($item->exp_time))}}</i>
                                        
                                </div>     
                                <div class="grid-content-canho">
                                    <table class="table bds-item">
                                        <tr>
                                            <td><span class="green">Khu:</span> {!! $item->vitri !!}</td>
                                            
                                        </tr>
                                        <tr>
                                            <td><span class="green">Diện tích: </span>{!! $item->dientichmatbang . " m² " !!}</td> 
                                        </tr>
                                        <tr>
                                            <td>
                                                 @if($item->typegia == 1)
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " VND " !!}</span> </span>                                    
                                                    @else
                                                         <span class="green">Giá : </span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") . " USD " !!}</span> </span>                                            
                                                    @endif        
                                            </td>
                                        </tr>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="row">
                <div class="search_button_container">
                    <a href="{{route('view-more-nha-dat')}}">
                        <button type="button" class="btn btn-primary" style="border-radius: 30px">Xem thêm</button>
                    </a>
                </div>
            </div>       
        </div>
    </section>


<!-- MODAL FORM -->
<div id="m_dientich" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top: 10%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Lựa chọn diện tích</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>Từ: <span class="red"><span id="dt_from">0</span> m2</span> đến: <span class="red"><span id="dt_to">400</span> m2</span></p>
                <div id="slider-range"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div id="m_gia" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top: 10%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Lựa chọn giá</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>Từ: <span class="red"><span id="p_from">500 triệu</span> </span> đến: <span class="red"><span id="p_to">30 tỷ</span></span></p>
                <div id="slider-range-price"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL FORM -->

<!-- Script -->
<script type="text/javascript">
    $(document).ready(function() {
        var url = "./api/province";
        $.getJSON(url, function(result) {
            $.each(result, function(i, field) {
                var id = field.provinceid;
                var name = field.name;
                var type = field.type;
                $("#province").append('<option value="' + id + '">' + " " + name + '</option>');
            });
        });

        //Khi chọn tỉnh thành
        $("#province").change(function() {
            $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
            var idprovince = $("#province").val();
            var url = "./api/district/" + idprovince;
            $.getJSON(url, function(result) {
                console.log(result);
                $.each(result, function(i, field) {
                    var id = field.districtid;
                    var name = field.name;
                    var type = field.type;
                    $("#district").append('<option value="' + id + '">' + " " + name + '</option>');
                });
            });
        });
    })


</script>

@endsection