<div class="tab-pane fade show " id="nav-canho" role="tabpanel">
    <a href="{{route('quanlytk.CreateCanho')}}">
        <button type="button" class="btn btn-primary mt-2 mb-2" style="width:initial">Đăng bài căn hộ</button>
    </a>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tiêu đề</th>
                    <th scope="col">Ngày đăng</th>
                    <th scope="col">Chọn</th>
                </tr>
            </thead>
            <tbody>
                @foreach($canho as $item)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$item->title}}</td>
                    <td>{{Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans()}}</td>
                    <td>
                        <a  href="{{route('quanlycanho.EditCanHo',['quanlycanho' => $item->alias])}}">Sửa</a> &nbsp; &nbsp;

                        <a onclick="return acceptDelete() " href="{{ route('quanlytk.destroyCanho',['quanlytk' => $item ->id]) }}">Xóa</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
     <nav aria-label="Page navigation example">
          <ul class="pagination">
             @if( $canho->currentPage() != 1)
                <li class="page-item">
                  <a class="page-link" href="{!! $canho->url($canho->currentPage() - 1) !!}" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
             @endif
             @for($i=1; $i <= $canho->lastPage(); $i= $i+1)
                <li class="page-item {!! ($canho->currentPage() == $i )? 'active': '' !!}">
                    <a class="page-link" href="{!! $canho->url($i) !!}">{!! $i !!}</a></li>
           
            @endfor
            @if($canho->currentPage() != $canho->lastPage())
                <li class="page-item">
                  <a class="page-link" href="{!! $canho->url($canho->currentPage() + 1) !!}" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
             @endif
          </ul>
        </nav>
</div>
