<div class="tab-pane fade" id="nav-changepass" role="tabpanel">
     @if (session('error'))
        <div class="alert alert-danger" id="flash-message" style="left: auto;">
            {{ session('error') }}
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success" id="flash-message" style="left: auto;">
            {{ session('success') }}
        </div>
    @endif
    <form class="form-horizontal" id="doimatkhau" action="{{ route('changepass.update') }}" method="POST" />
       {{--  @method('PUT') --}}
        @csrf
        <div class="form-group {{ $errors->has('current-password') ? ' has-error' : '' }}" >
            <i class="fas fa-key"></i>
            <label>Mật khẩu cũ</label>
            <input type="password" class="form-control" name="current-password" required placeholder="Nhập mật khẩu cũ">
            @if ($errors->has('current-password'))
                <span class="help-block" style="color: red">
                    <strong>{{ $errors->first('current-password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('new-password') ? ' has-error' : '' }}" >
            <i class="fas fa-key"></i>
            <label>Mật khẩu mới</label>
            <input type="password" class="form-control" name="new-password" required placeholder="Nhập mật khẩu mới">
            @if ($errors->has('new-password'))
                <span class="help-block" style="color: red">
                    <strong>{{ $errors->first('new-password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group" >
            <i class="fas fa-key"></i>
            <label>Nhập lại mật khẩu mới</label>
            <input type="password" class="form-control" name="new-password_confirmation" required placeholder="Nhập lại mật khẩu mới">
        </div>
        <button type="submit" class="btn btn-primary mt-3">Thay đổi mật khẩu</button>
    </form>
</div>
