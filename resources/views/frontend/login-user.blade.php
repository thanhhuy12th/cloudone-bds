<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
</head>

<body>
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
    <div class="wrap-login ">
        <div class="container">

            <div class="login-backgroud" style="min-height: 500px;">
                <div class="login-form" style="padding-bottom: 87px">
                    <!-- dăng nhập -->
                @if ($errors->any())
                     
                      <div class="card" id="flash-message" style="margin-bottom: 15px">
                        <div class="card-header bg-danger" style="height: 30px">
                          
                            <a data-toggle="collapse" class="text-white" aria-expanded="true"></a>
                         
                        </div>
                        @foreach ($errors->all() as $error)
                        <div id="collapsible-styled-group1" class="collapse show" style=" padding-bottom: 4px">
                          
                           <span style="margin-left: 15px;">{{ $error }}</span>
                         
                        </div>
                          @endforeach
                    </div>
                  @endif   
                    <form id="dangnhap" action="{{route('dangnhap')}}" method="POST" >
                        @csrf
                        <div class="form-group">
                            <i class="fas fa-envelope-open"></i>
                            <label>Địa chỉ email</label>
                            <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Nhập địa chỉ email" required="" />
                            <small class="form-text text-muted">Địa chỉ email của bạn luôn được chúng tôi bảo mật</small>
                        </div>
                        <div class="form-group">
                            <i class="fas fa-key"></i>
                            <label>Mật khẩu</label>
                            <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" required="" />
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" name="remember" {{ old('remember') ? 'checked' : '' }} />
                            <label class="form-check-label">Nhớ tài khoản</label>
                        </div>
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6LewA7oUAAAAANLI8MKhRUWB8kagUy4w8UC_lnNq"></div>
                        </div>
                        <button type="submit" class="btn btn-primary">Đăng nhập</button>
                        <div class="form-group pt-5">
                            <label class="form-check-label"><a href="{{ route('password.request') }}">Quên mật khẩu?</a></label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Footer -->
    <footer class="footer">
        @include('frontend.blocks.footer')
    </footer>
 @include('frontend.blocks.lienhe')
</body>

</html>