<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body>
    @include('frontend.blocks.menu')

    @include('admin.blocks.alert')
  
	 <div class="container">
	 	<div class="content">
		    <!-- Form validation -->
		    <div class="card" style="top: 15px">
		        <div class="card-body">            
		            <form class="form-validate-jquery" action="{{route('quanlycanho.UpdateCanHo',['alias' => $canho->alias])}}" method="POST" enctype="multipart/form-data">
		                @csrf
		                <fieldset class="mb-3">
		                   <div class="page-header page-header-light mb-4">
		                        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
		                            <div class="d-flex">
		                                <div class="bread-crumb">
		                                    <span class="breadcrumb-item"><i class="fas fa-home mr-1"></i> <a href="{{url('/')}}">Trang Chủ</a></span>
		                                    <span class="breadcrumb-item"><a href="{{url('quan-ly-tai-khoan')}}">Quản lý căn hộ</a></span>
		                                    <span class="breadcrumb-item">Sửa Bài Viết</span>
		                                </div>
		                            </div>
		                        </div>
		                    </div>    
							<hr>
		                    <!-- Basic select -->
		                    <div class="form-group row">
		                        <label class="col-form-label col-lg-2">Chọn danh mục<span class="text-danger">*</span></label>
		                        <div class="col-lg-4">
		                           <select name="category_id" class="form-control">    
		                               @for($i = 0; $i < count($dm_cate); $i++)
		                                <option value="{!!$dm_cate[$i]['id']!!}" {{ (old('category_id', $canho->category_id) == $dm_cate[$i]['id'])? 'selected': ''}}>{!!$dm_cate[$i]['name']!!}</option>
		                               @endfor
		                           </select>
		                        </div>
		                    </div>
		                    <!-- /basic select -->	                  

			                  <!-- Basic title input -->
			                    <div class="form-group row">
			                        <label class="col-form-label col-lg-2">Tiêu đề<span class="text-danger">*</span></label>
			                        <div class="col-lg-10">
			                            <input type="text" name="title" class="form-control" value="{{old('title',$canho->title)}}" maxlength="250" required="" />
			                        </div>
			                    </div>
			                    <!-- /basic title input -->

								 <!-- Basic alias input -->
			                    <div class="form-group row" hidden="">
			                        <label class="col-form-label col-lg-2">{{trans('template.Alias Post')}}</label>
			                        <div class="col-lg-10">
			                            <input type="text" name="alias" class="form-control"  />
			                        </div>
			                    </div>
			                    <!-- /basic alias input -->                   

			                     <!-- Basic alias input -->
			                    <div class="form-group row">
			                        <label class="col-form-label col-lg-2">Khu vực</label>
			                        <div class="col-lg-10">
			                            <input type="text" name="khuvuc" class="form-control" value="{{old('khuvuc',$canho->khuvuc)}}" />
			                        </div>
			                    </div>
			                    <!-- /basic alias input -->

			                    
			                    <!-- Basic alias input -->
			                    <div class="form-group row">
			                        <label class="col-form-label col-lg-2">Link liên kết</label>
			                        <div class="col-lg-10">
			                            <input type="text" name="link" class="form-control" value="{{old('link',$canho->link)}}" />
			                        </div>
			                    </div>
			                    <!-- /basic alias input -->

			                  <!-- Basic text input -->
			                    <div class="form-group row" hidden="">
			                        <label class="col-form-label col-lg-2">Vị trí<span class="text-danger">*</span></label>
			                        <div class="col-md-10">
			                            <input class="form-control" type="number" name="position" placeholder="1-9999" />
			                        </div>
			                    </div>
			                    <!-- /basic text input -->

			                    <!-- Basic select -->
			                	<div class="form-group row">
			                        <label class="col-form-label col-lg-2">Hướng view<span class="text-danger">*</span></label>
			                        <div class="col-lg-4">                           
			                                <select name="huong_view" class="form-control"/>
			                                    <option value="1" {{ ($canho->huong_view ==1)? 'selected':'' }}>Nam</option>
			                                    <option value="2" {{ ($canho->huong_view ==2)? 'selected':'' }}>Tây Nam</option>
			                                    <option value="3" {{ ($canho->huong_view ==3)? 'selected':'' }}>Tây</option>
			                                    <option value="4" {{ ($canho->huong_view ==4)? 'selected':'' }}>Tây Bắc</option>
			                                    <option value="5" {{ ($canho->huong_view ==5)? 'selected':'' }}>Bắc</option>
			                                    <option value="6" {{ ($canho->huong_view ==6)? 'selected':'' }}>Đông Bắc</option>
			                                    <option value="7" {{ ($canho->huong_view ==7)? 'selected':'' }}>Đông</option>
			                                    <option value="8" {{ ($canho->huong_view ==8)? 'selected':'' }}>Đông Nam</option>
			                               </select>                          
			                        </div>
			                         <label class="col-form-label col-lg-2">Hướng ban công<span class="text-danger">*</span></label>
				                        <div class="col-lg-4">
				                             <select name="huong_bancong" class="form-control"/>
			                                    <option value="1" {{ ($canho->huong_bancong ==1)? 'selected':'' }}>Nam</option>
			                                    <option value="2" {{ ($canho->huong_bancong ==2)? 'selected':'' }}>Tây Nam</option>
			                                    <option value="3" {{ ($canho->huong_bancong ==3)? 'selected':'' }}>Tây</option>
			                                    <option value="4" {{ ($canho->huong_bancong ==4)? 'selected':'' }}>Tây Bắc</option>
			                                    <option value="5" {{ ($canho->huong_bancong ==5)? 'selected':'' }}>Bắc</option>
			                                    <option value="6" {{ ($canho->huong_bancong ==6)? 'selected':'' }}>Đông Bắc</option>
			                                    <option value="7" {{ ($canho->huong_bancong ==7)? 'selected':'' }}>Đông</option>
			                                    <option value="8" {{ ($canho->huong_bancong ==8)? 'selected':'' }}>Đông Nam</option>
			                               </select>
				                        </div>
			                    </div>
                    		<!-- /basic select -->
                    		<!-- Basic select -->
			                	<div class="form-group row">
			                       <label class="col-form-label col-lg-2">Ưu tiên<span class="text-danger">*</span></label>
			                        <div class="col-lg-4">
			                            <select name="hot" class="form-control"/>
			                            	<option value="0" {{ ($canho->hot ==0)? 'selected':'' }}>Không</option>
		                                   	<option value="1" {{ ($canho->hot ==1)? 'selected':'' }}>Có</option>                                			
		                               </select>
			                        </div>
			                    </div>
                    		<!-- /basic select -->
		                    <!-- Basic content input -->
		                    <div class="form-group row">
		                        <label class="col-form-label col-lg-12">{{trans('template.Content')}}</label>
		                    </div>
		                    
		                    <hr>
		                    <div class="card-body">
		                        <ul class="nav nav-tabs">
		                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.Intro Post')}}</a></li>
		                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.Acreage Content')}}</a></li>
		                            <li class="nav-item"><a href="#basic-tab3" class="nav-link" data-toggle="tab">{{trans('template.Price Content')}}</a></li>
		                            <li class="nav-item"><a href="#basic-tab4" class="nav-link" data-toggle="tab">Mã tòa căn hộ</a></li>
		                            <li class="nav-item"><a href="#basic-tab5" class="nav-link" data-toggle="tab">{{trans('template.Overview')}}</a></li>
		                            <li class="nav-item"><a href="#basic-tab6" class="nav-link" data-toggle="tab">{{trans('template.Contact')}}</a></li>
		                            <li class="nav-item"><a href="#basic-tab7" class="nav-link" data-toggle="tab">{{trans('template.Location Post')}}</a></li>
		                            <li class="nav-item"><a href="#basic-tab8" class="nav-link" data-toggle="tab">{{trans('template.Image Post')}}</a></li>
		                            
		                        </ul>

		                    <div class="tab-content">
		                        <div class="tab-pane fade active show" id="basic-tab1">
		                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                                <textarea rows="6" name="intro" class="form-control col-md-12 col-xs-12" >{{old('intro',$canho->intro)}}</textarea>
		                            </div>
		                        </div>

		                       <div class="tab-pane fade" id="basic-tab2">
		                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
		                             <div class="row">
		                                <div class="col-lg-6">                                   
		                                    <div class="input-group">
		                                        <input type="text" name="dientich" class="form-control" value="{{old('dientich',$canho->dientich)}}" placeholder="Diện tích" required="" maxlength="5" />
		                                        <span class="input-group-append">
		                                            <span class="input-group-text">m²</span>
		                                        </span>
		                                    </div>
		                                </div>                                 
		                            </div>
		                        </div>
		                         <div class="tab-pane fade" id="basic-tab3">
		                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
		                            <div class="row">
		                                <div class="col-lg-6">
		                                    <div class="input-group">
		                                        <span class="input-group-prepend">
		                                            <span class="input-group-text">$</span>
		                                        </span>
		                                        <input type="number" name="priceCDT" class="form-control"  maxlength="12" value="{{old('priceCDT',$canho->priceCDT)}}" placeholder="Giá bán"  />
		                                        <span class="input-group-append">
		                                            <span class="input-group-text">vnd</span>
		                                        </span>
		                                    </div>
		                                </div>
		                               
		                                <div class="col-lg-6">
		                                    <div class="input-group">
		                                        <span class="input-group-prepend">
		                                            <span class="input-group-text">$</span>
		                                        </span>
		                                        <input type="number" name="priceChenh" class="form-control" maxlength="12" value="{{old('priceChenh',$canho->priceChenh)}}" placeholder="Giá chênh lệch"  />
		                                        <span class="input-group-append">
		                                            <span class="input-group-text">vnd</span>
		                                        </span>
		                                    </div>
		                                </div>
		                               
		                            </div>
		                        </div>
		                        <div class="tab-pane fade" id="basic-tab4">
		                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
		                             <div class="row">
		                                <div class="col-lg-6">                                   
		                                    <div class="input-group">
		                                        <input type="text" name="maCan" class="form-control" value="{{old('maCan',$canho->maCan)}}" placeholder="Mã Căn" maxlength="8" />
		                                       
		                                    </div>
		                                </div>
		                                <div class="col-lg-6">                                   
		                                    <div class="input-group">
		                                        <input type="text" name="maToa" class="form-control" value="{{old('maToa',$canho->maToa)}}" placeholder="Mã Tòa" maxlength="6" />
		                                       
		                                    </div>
		                                </div>                                   
		                            </div>
		                        </div>
		                        <div class="tab-pane fade" id="basic-tab5">
		                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                                <textarea rows="10" id="tongquanbreak" name="content" class="form-control col-md-12 col-xs-12" required="">{{old('content',$canho->content)}}</textarea>
		                            </div>
		                        </div>

		                        <div class="tab-pane fade" id="basic-tab6">		                        	 
		                             <label class="control-label col-md-12 col-sm-12 col-xs-12 mt-1" for="last-name"></label>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                            	@php 
		                                    $splittedstring=explode("||",$canho->contact);   
		                                @endphp
		                               @for($i =0; $i<1; $i++)
		                                <div class="form-group row">
		                                    <label class="col-form-label col-lg-2">Tên liên hệ:</label>
		                                    <div class="col-lg-8">
		                                        <input type="text" name="name_contact" class="form-control"  value="{{old('contact',$splittedstring[0])}}" placeholder="Nhập tên liên hệ"  />
		                                    </div>
		                                </div>
		                                  <div class="form-group row">
		                                    <label class="col-form-label col-lg-2">Email:</label>
		                                    <div class="col-lg-8">
		                                        <input type="email" name="email_contact" class="form-control"  value="{{old('contact',$splittedstring[1])}}" id="email" placeholder="Email" />
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <label class="col-form-label col-lg-2">Số điện thoại:</label>
		                                    <div class="col-lg-8">
		                                        <input type="text" name="phone_contact" class="form-control"  value="{{old('contact',$splittedstring[2])}}" placeholder="Số điện thoại"/>
		                                    </div>
		                                </div>
		                                @endfor                                               
		                            </div>
		                        </div>

		                        <div class="tab-pane fade" id="basic-tab7">
		                            <div class="container mt-3">
		                                
		                                  <!-- Trigger the modal with a button -->
		                                  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="padding: 0px 20px; font-size: 14px; border-radius: 2rem; z-index: 9999">Hướng dẫn nhúng google map</button>
		                                  <!-- Modal -->
		                                  <div class="modal fade" id="myModal" role="dialog">
		                                    <div class="modal-dialog" style="top:110px">
		                                    
		                                      <!-- Modal content-->
		                                      <div class="modal-content">
		                                        <div class="modal-header">
		                                         
		                                          <h4 class="modal-title">Hướng dẫn nhúng iframe google map</h4>
		                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
		                                        </div>
		                                        <div class="modal-body">
		                                            <h6>Bước 1: Đăng nhập và truy cập vào website Google Maps tại địa chỉ: <a href="https://www.google.com/maps/preview" target="_blank" />https://www.google.com/maps/preview</a></h6>
		                                                <p> - Nhập địa chỉ của bạn tại ô tìm kiếm bản đồ.</p>
		                                            <h6>Bước 2: Click vào menu để lấy mã code</h6>
		                                                <p> - Click vào menu bản đồ Google Maps</p>
		                                                <p> - Click vào mục Chia sẻ & Nhúng bản đồ – Share & Embed map để lấy mã code</p>
		                                            <h6>Bước 3: Lấy mã code nhúng vào website</h6>
		                                                <p> - Click vào mục Nhúng bản đồ – Tab Embel maps</p>
		                                                <p> - Copy mã code Google Maps nhúng code vào website</p>
		                                        </div>
		                                        <div class="modal-footer">
		                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                                        </div>
		                                      </div>                                      
		                                    </div>
		                                  </div>                                  
		                                </div>
		                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                               <textarea rows="6" name="googlemap" class="form-control col-md-12 col-xs-12">{{old('googlemap',$canho->googlemap)}}</textarea>
		                            </div>
		                        </div>

		                         <div class="tab-pane fade" id="basic-tab8">
		                         	 @php 
	                                    $splittedstring=explode("||",$canho->images);   
	                                @endphp
	                                	@for($i =0; $i<count($splittedstring)-1; $i++)
                                    
		                                    <img src="{{ asset('public/uploads/postings/'.$splittedstring[$i])}}" style="width:100px; height:100px">
		                              	@endfor 
		                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
		                             <p style="color: red">Ghi chú: Chỉ cho phép định dạng jpg, png, jpeg, gif, svg và kích thước tối đa tệp tin là 2MB.</p>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                            	<input type="hidden" name="imageshidden" value="{{$canho->images}}" >
		                               <input type="file" name="images[]" class="form-input" >
		                            </div>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                               <input type="file" name="images[]" class="form-input" >
		                            </div>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                               <input type="file" name="images[]" class="form-input" >
		                            </div>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                               <input type="file" name="images[]" class="form-input" >
		                            </div>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                               <input type="file" name="images[]" class="form-input" >
		                            </div>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                               <input type="file" name="images[]" class="form-input" >
		                            </div>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                               <input type="file" name="images[]" class="form-input" >
		                            </div>
		                            <div class="col-md-12 col-sm-12 col-xs-12">
		                               <input type="file" name="images[]" class="form-input" >
		                            </div>                         
		                        </div>
		                    </div>
		                </div>
		                <!-- /basic content input -->
		                </fieldset>
		                <div class="d-flex justify-content-center align-items-center mt-3">
				            <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
				            <button type="submit"  class="btn btn-primary ml-3" style="width: initial">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>				            
				        </div>
		            </form>
		        </div>
		    </div>
		    <!-- /form validation -->
		</div>
	</div>
	

 <footer class="footer mt-5">
        @include('frontend.blocks.footer')
 </footer>
</body>
</html>