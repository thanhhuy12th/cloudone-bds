<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body>
    
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
    <!-- Intro -->
   @include('frontend.blocks.leftslide')
   
   @yield('content')
    <!-- Footer -->
 
       @include('frontend.blocks.footer')
 

    

    <!-- Slide Bar Thông tin khuyến mãi -->
    <div class="btn-slide-bar" id="mybtnslide">
        <span class="symbol-btn-slide-bar">
            <i class="fas fa-arrow-circle-right"></i>
        </span>
    </div>



    <!-- Back to top -->
    <div class="btn-back-to-top" id="myBtn" style="display:none;">
        <span class="symbol-btn-back-to-top">
            <i class="fa fa-angle-double-up" aria-hidden="true"></i>
        </span>
    </div>
</body>

</html>