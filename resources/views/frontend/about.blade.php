<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body>
    @include('frontend.blocks.leftslide')
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
  
    <!-- Intro -->
    <section class="intro">
       {{--  <div class="wrap-intro">
           
            <h2 style="color: #f7f7f7">CÔNG TY BẤT ĐỘNG SẢN NTK LAND</h2>
        </div> --}}
    </section>

    @yield('content')
    <!-- Footer -->
    <footer class="footer">
       @include('frontend.blocks.footer')
    </footer>

     @include('frontend.blocks.lienhe')

    <!-- Slide Bar Thông tin khuyến mãi -->
    <div class="btn-slide-bar" id="mybtnslide">
        <span class="symbol-btn-slide-bar">
            <i class="fas fa-arrow-circle-right"></i>
        </span>
    </div>

    <!-- Back to top -->
    <div class="btn-back-to-top" id="myBtn" style="display:none;">
        <span class="symbol-btn-back-to-top">
            <i class="fa fa-angle-double-up" aria-hidden="true"></i>
        </span>
    </div>
</body>

</html>