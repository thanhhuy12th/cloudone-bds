<a href="tel:{!! $hotline->content !!}" mypage="" class="call-now" rel="nofollow">
        <div class="bor-tel">
        </div>
        <div class="bg-tel">
            <i class="fas fa-phone-volume"></i>
        </div>
        <div class="hotline">
            <span class="before-hotline">Hotline:</span>
            <span class="hotline-number">{!! $hotline->content !!}</span>
        </div>
    </a>