<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="description" content="{!! $description->content !!} " />
<meta name="keywords" content="{!! $seo->content !!} " />
<title>@yield('title','MUANHAVN.NET - Thông tin giao dịch bất động sản hàng đầu Việt Nam')</title>
<link rel="shortcut icon" type="image/jpg" href="{{ asset('frontend/img/1044.jpg') }}"/>
<!-- bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- popup -->


<script src="{{asset('frontend/js/jquery.magnific-popup.js')}}"></script>
<link rel="stylesheet" href="{{asset('frontend/css/magnific-popup.css')}}">

<script src="{{asset('frontend/js/jquery.ui.touch-punch.js')}}"></script>
<!-- bxslider -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>

<!-- hieu ung -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/TweenMax.min.js"></script>
<!-- font -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900&amp;subset=vietnamese" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/Responsive/index-responsive.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/owl.transitions.css')}}">

<script src="{{asset('frontend/js/main.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('ckfinder/ckfinder.js')}}"></script>


<script src="{{asset('frontend/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/functions.js')}}" type="text/javascript"></script>


<script src="{{asset('backend/global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
<script src="{{asset('backend/global_assets/js/demo_pages/uploader_bootstrap.js')}}"></script>

<!-- /theme JS files -->

<script>
	$(document).ready(function() {
         $('#flash-message').delay(5000).slideUp(1000);
      });
	function acceptDelete () {
   		if (window.confirm('Bạn chắc chắn muốn xóa?')) {
       		return true;
   		}
     		return false;
  	}
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-153412277-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-153412277-1');
</script>

