
<section  class="slide">            
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php $i = 0; ?>
            @foreach($slider as $i=>$value)
            <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" @if($i==0) class="active" @endif></li>
            @endforeach
            <?php $i++; ?>
        </ol>
        <div  class="carousel-inner" role="listbox">
            <?php $i = 0; ?>
            @foreach($slider as $sl)           
                <div @if($i==0)  class="carousel-item  active" @else class="carousel-item " @endif>
                    <?php $i++; ?>
                    <img class="d-block w-100 "  src="{{asset('uploads/slider/'.$sl->image)}}"  alt="{{$sl->intro}}" >
                </div>            
            @endforeach
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="icon-prev" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="icon-next" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>