
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style>
.full {
    width: 100%;    
}

.footer {
  height: auto;
    padding-bottom: 0px;
    position: relative;
    width: 100%;
    color: #fff;
    border-bottom: 1px solid #CCCCCC;
    border-top: 1px solid #DDDDDD;
    background: #056189;
}


.footer h3 {
    border-bottom: 1px solid #BAC1C8;
    color:#fff;
    font-size: 18px;
    font-weight: 600;
    line-height: 27px;
    padding: 40px 0 10px;
    text-transform: uppercase;
}
.footer ul {
    font-size: 13px;
    list-style-type: none;
    margin-left: 0;
    padding-left: 0;
    margin-top: 15px;
    color: #7F8C8D;
}

.footer ul li a {
    padding: 0 0 5px 0;
    display: block;
}
.footer a {
    color: #78828D;
    color:#fff;
}
.supportLi h4 {
    font-size: 20px;
    font-weight: lighter;
    line-height: normal;
    margin-bottom: 0 !important;
    padding-bottom: 0;
}

.subscribe-btn .btn {
    border: medium none;
    border-radius: 4px;
    display: inline-block;
    height: 40px;
    padding: 0;
    width: 100%;
    color: #fff;
    margin-top:10px;
}
.subscribe-btn {
    overflow: hidden;
}

.social li {
    border: 2px solid #B5B5B5;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -o-border-radius: 50%;
    -ms-border-radius: 50%;
    border-radius: 50%;
    float: left;
    height: 36px;
    line-height: 36px;
    margin: 0 8px 0 0;
    padding: 0;
    text-align: center;
    width: 36px;
    transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -webkit-transition: all 0.5s ease 0s;
    -ms-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
}

.social li a {
    color: #EDEFF1;
}
.social li:hover {
    border: 2px solid #2c3e50;
    background: #2c3e50;
}
.social li a i {
    font-size: 16px;
    margin: 0 0 0 5px;
    color: #EDEFF1 !important;
}
.footer-bottom {

 
    color: #fff;
    background: #383838;
    font-size: 0.8em;
}
.footer-bottom p.pull-left {
    padding-top: 6px;
    margin-left: 420px;
}

</style>
    <div class="footer" >
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3> Tin Mới</h3>
                    <ul>
                        @foreach($hotnews as $news)
                            <li> <a href="{{route('news-detail',[$news->alias])}}"> {{$news->name}} </a> </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-2">
                    <h3> Liên kết nhanh </h3>
                    <ul>
                        <li> <a href="{{route('news')}}"> Tin Tức </a> </li>
                          <?php
                        $menu_lv_1 = DB::table('bds_dmtd')->where('parent_id', 0)->get();
                        ?>
                        @foreach($menu_lv_1 as $item_lv_1)
                            <li> <a href="{{url('product-category-parent',[$item_lv_1->id,$item_lv_1->alias])}}"> {{$item_lv_1->name}} </a> </li>
                        @endforeach
                        <li> <a href="{{route('home')}}"> Căn hộ </a> </li>
                       
                    </ul>
                </div>            
                <div class="col-md-4">
                    <h3> {!! $title->content !!}  </h3>
                    <ul>
                        <li> <a href="#">{!! $address->content !!} </a> </li>
                        <li> <a href="#">ĐIỆN THOẠI: {!! $hotline->content !!} </a> </li>
                        <li> <a href="#">Email: {!! $email->content !!}</a> </li>
                        <li> <a href="{!! $baseUrl->content !!}" target="_blank"> Website: {!! $baseUrl->content !!} </a> </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3> KẾT NỐI VỚI CHÚNG TÔI </h3>
                    <ul>
                        <li>
                            <div class="input-append subscribe-btn text-center">
                                <input type="text" class="full text-center" name="email" placeholder="Email ">
                                <button class="btn  btn-success" type="button"> Subscribe <i class="fa fa-save"> </i> </button>
                            </div>
                        </li>
                    </ul>
                    <ul class="social">
                        <li> <a href="https://www.facebook.com" title="facebook.com" target="_blank"> <i class=" fa fa-facebook"></i> </a></li>
                        <li> <a href="#"> <i class="fa fa-twitter">   </i> </a> </li>
                        <li> <a href="#"> <i class="fa fa-google-plus">   </i> </a> </li>
                        <li> <a href="#"> <i class="fa fa-pinterest">   </i> </a> </li>
                        <li> <a href="https://www.youtube.com" title="youtube.com" target="_blank"> <i class="fa fa-youtube">   </i> </a> </li>
                    </ul>
                </div>
            </div>
            <!--/.row--> 
        </div>
        <!--/.container--> 
        <div class="footer-bottom">
       <div class="copy-right">
            <p> &copy;2019 Design by <a href="https://cloudone.vn" target="_blank">CloudOne</a></p>
        </div>
    </div>
  
    </div>
    <!--/.footer-->    
    <!--/.footer-bottom--> 
 <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API = Tawk_API || {},
                Tawk_LoadStart = new Date();
            (function() {
                var s1 = document.createElement("script"),
                    s0 = document.getElementsByTagName("script")[0];
                s1.async = true;
                s1.src = 'https://embed.tawk.to/5cedeb012135900bac12f549/default';
                s1.charset = 'UTF-8';
                s1.setAttribute('crossorigin', '*');
                s0.parentNode.insertBefore(s1, s0);
            })();
        </script>
</div>

