<script src='https://www.google.com/recaptcha/api.js' async defer></script>
<div class="slide-bar none">
    <div class="slide-bar-container">
    </div>
    <div class="slide-bar-form">
        <div class="wrap-form">
            <div class="wrap-form-title">
                <p>Liên hệ với chúng tôi</p>
            </div>
            <div class="wrap-form-content">
                <form class="wrap-from-input" action="{{route('lien-he.store')}}" method="POST">
                    @csrf
                    <!-- Họ và tên -->
                    <div class="v-form">
                        <div class="v-title">
                            <div class="v-label">
                                <p>Họ và tên</p>
                            </div>
                            <span>*</span>
                        </div>
                        <input class="v-input" name="name" type="text" required="" value="{{old('name')}}" placeholder="Họ và tên">
                    </div>
                    <!-- Sdt -->
                    <div class="v-form">
                        <div class="v-title">
                            <div class="v-label">
                                <p>Số điện thoại</p>
                            </div>
                            <span>*</span>
                        </div>
                        <input class="v-input" name="phone" type="text" required="" value="{{old('phone')}}" placeholder="Số điện thoại">
                    </div>
                    <!-- Email -->
                    <div class="v-form">
                        <div class="v-title">
                            <div class="v-label">
                                <p>Email</p>
                            </div>
                            <span>*</span>
                        </div>
                        <input class="v-input" name="email" type="text" required="" value="{{old('email')}}" placeholder="Email">
                    </div>

                    <div class="v-form">
                        <div class="v-title">
                            <div class="v-label">
                                <p>Bạn quan tâm đến dự án nào?</p>
                            </div>
                        </div>
                        <textarea type="text" class="form-control luna-message" id="message" placeholder="Type your messages here" name="content">{{old('content')}}</textarea>
                    </div>
                    <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6LewA7oUAAAAANLI8MKhRUWB8kagUy4w8UC_lnNq"></div>
                        </div>  
                    <!-- Nút gửi -->
                    <div class="v-form pt-10">
                        <input class="v-submit" name="send-submit" type="submit" value="Gửi">
                    </div>
                </form>
            </div>
        </div>      
        <div class="close-form">
            <i id="mybtnclose" class="fas fa-window-close"></i>
        </div>
    </div>
</div>


