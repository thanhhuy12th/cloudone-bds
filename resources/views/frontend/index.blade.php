<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Google Tag Manager -->
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M3B43MV');
</script>
<!-- End Google Tag Manager -->
    @include('frontend.blocks.head')
</head>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M3B43MV"
height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<body>
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
    
    @include('frontend.blocks.leftslide')

    @include('frontend.blocks.slider')


    @yield('content')

    <!-- Footer -->
  
    @include('frontend.blocks.footer')
    

    <!-- Hotline -->
    @include('frontend.blocks.lienhe')
    
    <!-- Slide Bar Thông tin khuyến mãi -->
    <div class="btn-slide-bar" id="mybtnslide">
        <span class="symbol-btn-slide-bar">
            <i class="fas fa-arrow-circle-right"></i>
        </span>
    </div>



    <!-- Back to top -->
    <div class="btn-back-to-top" id="myBtn" style="display:none;">
        <span class="symbol-btn-back-to-top">
            <i class="fa fa-angle-double-up" aria-hidden="true"></i>
        </span>
    </div>

</body>

</html>